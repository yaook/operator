// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#messagequeue
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "DesignateDeployment"
	#plural:   "designatedeployments"
	#singular: "designatedeployment"
	#shortnames: ["designated", "designateds"]
	#releases: ["2024.1"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"database",
			"messageQueue",
			"memcached",
			"targetRelease",
			"designateConfig",
			"region",
			"issuerRef",
			"powerdns",
			"nsRecords",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			api:             crd.apiendpoint
			serviceMonitor:  crd.#servicemonitor
			designateConfig: crd.#anyconfig

			api: {
				description: "Designate API deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits for containers related to the Designate API."
					properties: {
						"designate-api":           crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"ssl-terminator-internal": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
						"service-reload-internal": crd.#containerresources
					}
				}
			}
			jobResources: {
				type:        "object"
				description: "Resource limits for Job Pod containers spawned by the Operator"
				properties: {
					"designate-db-sync-job":     crd.#containerresources
					"designate-pool-update-job": crd.#containerresources
				}
			}

			additionalNameservers: {
				type:        "array"
				description: "List of secondary nameservers that will be informed about zone changes"
				items: {
					type: "object"
					required: [
						"host",
						"port",
					]
					properties: {
						host: type: "string"
						port: type: "integer"
					}
				}
			}

			nsRecords: {
				type:        "array"
				description: "List of NS records for zones hosted within the default pool"
				items: {
					type: "object"
					required: [
						"hostname",
						"priority",
					]
					properties: {
						hostname: type: "string"
						priority: type: "integer"
					}
				}
			}

			powerdns: {
				type: "object"
				crd.replicated

				required: [
					"database",
					"subnetCidr",
				]

				properties: {
					database: crd.#databasesnip

					loadBalancerIP: {
						type:        "string"
						description: "IP address of the load balancer for the PowerDNS webserver"
					}

					subnetCidr: {
						type:        "string"
						description: "Comma separated IP ranges of the k8s pod network. Used to only allow requests from within the k8s network."
					}
				}
			}

			central: {
				type: "object"
				crd.replicated

				properties: {
					resources: {
						type:        "object"
						description: "Resource requests/limits for containers related to the Designate Central service"
						properties: {
							"central": crd.#containerresources
						}
					}
				}
			}

			minidns: {
				type: "object"
				crd.replicated

				properties: {
					resources: {
						type:        "object"
						description: "Resource requests/limits for containers related to the Designate MiniDNS server"
						properties: {
							"minidns": crd.#containerresources
						}
					}
				}
			}

			producer: {
				type: "object"
				crd.replicated

				properties: {
					resources: {
						type:        "object"
						description: "Resource requests/limits for containers related to the Designate Producer service"
						properties: {
							"producer": crd.#containerresources
						}
					}
				}
			}

			worker: {
				type: "object"
				crd.replicated

				properties: {
					resources: {
						type:        "object"
						description: "Resource requests/limits for containers related to the Designate Worker service"
						properties: {
							"worker": crd.#containerresources
						}
					}
				}
			}

			jobResources: {
				type:        "object"
				description: "Resource limits for Job Pod containers spawned by the Operator"
				properties: {
					"designate-db-sync-job":     crd.#containerresources
					"designate-pool-update-job": crd.#containerresources
				}
			}
		}
	}
}
