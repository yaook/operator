#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Kubernetes Authorization resources
##################################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: Role

.. autoclass:: RoleBinding

.. autoclass:: TemplatedRole

.. autoclass:: TemplatedRoleBinding

"""
import typing

import kubernetes_asyncio.client as kclient

from .k8s import BodyTemplateMixin, SingleObject
from .. import (
    api_utils,
    interfaces,
)
T = typing.TypeVar("T")


class Role(SingleObject[kclient.V1Role]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1Role]:
        return interfaces.role_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1Role,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["rules"], new["rules"]))


class ClusterRole(SingleObject[kclient.V1ClusterRole]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ClusterResourceInterface[
                kclient.V1ClusterRole]:
        return interfaces.clusterrole_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1ClusterRole,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["rules"], new["rules"]))


class RoleBinding(SingleObject[kclient.V1RoleBinding]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[
                kclient.V1RoleBinding]:
        return interfaces.role_binding_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1RoleBinding,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["roleRef"], new["roleRef"]) or
                api_utils.deep_has_changes(cyaml["subjects"], new["subjects"]))


class ClusterRoleBinding(SingleObject[kclient.V1ClusterRoleBinding]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ClusterResourceInterface[
                kclient.V1ClusterRoleBinding]:
        return interfaces.clusterrole_binding_interface(api_client)

    def _needs_update(self,
                      current: kclient.V1ClusterRoleBinding,
                      new: typing.Mapping) -> bool:
        cyaml = api_utils.k8s_obj_to_yaml_data(current)
        return (super()._needs_update(current, new) or
                api_utils.deep_has_changes(cyaml["roleRef"], new["roleRef"]) or
                api_utils.deep_has_changes(cyaml["subjects"], new["subjects"]))


class TemplatedRole(BodyTemplateMixin, Role):
    """
    Manage a jinja2-templated Role.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """


class TemplatedClusterRole(BodyTemplateMixin, ClusterRole):
    """
    Manage a jinja2-templated ClusterRole.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """


class TemplatedRoleBinding(BodyTemplateMixin, RoleBinding):
    """
    Manage a jinja2-templated RoleBinding.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """


class TemplatedClusterRoleBinding(BodyTemplateMixin, ClusterRoleBinding):
    """
    Manage a jinja2-templated ClusterRoleBinding.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """
