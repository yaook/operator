# Changelog

All notable changes to this project will be documented in this file.

## [release-20230914]
- OVN upgrade 22.12.0 to 23.3.1
  The upgrade is done automatically with a workflow added some releases ago, but you can also jump directly to this release.

## [release-20230504 - now]
### Changed
- We introduced network.yaook.cloud/neutron-bgp-agent as label to schedule neutron-ovn-agents separately from neutron network nodes(network.yaook.cloud/neutron-ovn-agent).
  **ovn-bgp-agents will be deleted if no action is taken:** The node label needs to be added, where you want to run bgp agents before updating.
