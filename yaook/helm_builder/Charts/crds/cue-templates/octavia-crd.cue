// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#memcached
crd.#messagequeue
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "OctaviaDeployment"
	#plural:   "octaviadeployments"
	#singular: "octaviadeployment"
	#shortnames: ["octaviad", "octaviads"]
	#releases: ["2024.1"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"worker",
			"housekeeping",
			"health_manager",
			"database",
			"memcached",
			"messageQueue",
			"targetRelease",
			"octaviaConfig",
			"region",
			"issuerRef",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			api:    crd.apiendpoint
			worker: crd.replicated
			worker: properties: replicas: default: 1
			worker:         crd.topologySpreadConstraints
			housekeeping:   crd.replicated
			housekeeping:   crd.topologySpreadConstraints
			health_manager: crd.replicated
			health_manager: crd.topologySpreadConstraints
			serviceMonitor: crd.#servicemonitor
			octaviaConfig:  crd.#anyconfig
			octaviaSecrets: crd.#configsecret

			api: properties: resources: {
				type: "object"
				properties: {
					"octavia-api":             crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"ssl-terminator-internal": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
					"service-reload-internal": crd.#containerresources
				}
			}
			api: properties: sslTerminator: {
				type: "object"
				// This default is required to populate nested default value(s) below.
				default: {}
				properties: {
					respondingReadTimeout: {
						type:        "integer"
						default:     7200
						description: "Set traefik parameter 'transport.respondingTimeouts.readTimeout' (time in seconds) to a large enough value for allowing the upload of larger images. "
					}
				}
			}
			worker: properties: resources: {
				type: "object"
				properties: "octavia-worker": crd.#containerresources
			}
			health_manager: properties: resources: {
				type: "object"
				properties: "octavia-health-manager": crd.#containerresources
			}
			housekeeping: properties: resources: {
				type: "object"
				properties: "octavia-housekeeping": crd.#containerresources
			}
			jobResources: {
				type: "object"
				properties: {
					"octavia-db-load-metadefs-job": crd.#containerresources
					"octavia-db-sync-job":          crd.#containerresources
					"octavia-db-upgrade-pre-job":   crd.#containerresources
					"octavia-db-upgrade-post-job":  crd.#containerresources
				}
			}
		}
	}
}
