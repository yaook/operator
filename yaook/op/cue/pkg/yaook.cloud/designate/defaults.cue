// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package designate

import (
	"strings"
	"yaook.cloud/designate_template"
)

designate_conf_spec: designate_template.designate_template_conf_spec
designate_conf_spec: {
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-designate"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		use_stderr:    *true | bool
		use_json:      *true | bool
		debug:         *true | bool
	}
	"service:api": {
		listen:        *"[::]:8080" | string
		auth_strategy: "keystone"
	}
	"service:mdns": {
		listen: *"[::]:5354" | string
	}
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
		service_token_roles: *["admin"] | [string, ...]
	}
	"storage:sqlalchemy": {
		#connection_host:        string
		#connection_port:        *3306 | int
		#connection_username:    string
		#connection_password:    string
		#connection_database:    string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string

		// connection_recycle_time should always be ~10% smaller
		// than the haproxy timeoutClient.
		connection_recycle_time: *280 | int
	}
	cache: {
		backend:          "oslo_cache.memcache_pool"
		enabled:          false
		backend_argument: *[
					"memcached_expire_time:660",
		] | [...string]
	}
	oslo_concurrency: {
		lock_path: "/var/lib/designate/tmp"
	}
	oslo_policy: {
		policy_file:          *"/etc/designate/policy.yaml" | "/etc/designate/policy.json"
		enforce_new_defaults: false
		enforce_scope:        false
	}
	oslo_messaging_rabbit: {
		ssl:                           true
		ssl_ca_file:                   "/etc/pki/tls/certs/ca-bundle.crt"
		amqp_durable_queues:           true
		rabbit_quorum_queue:           true
		rabbit_transient_quorum_queue: *true | bool
		use_queue_manager:             *true | bool
		rabbit_stream_fanout:          *true | bool
		rabbit_qos_prefetch_count:     *250 | int
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
}
