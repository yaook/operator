// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package octavia_housekeeping

import (
	"yaook.cloud/octavia_housekeeping_template"
)

octavia_housekeeping_conf_spec: octavia_housekeeping_template.octavia_housekeeping_template_conf_spec
octavia_housekeeping_conf_spec: {
	DEFAULT: {
		use_stderr: *true | bool
		use_json:   *true | bool
		debug:      *true | bool
	}
	database: {
		#connection_host:        *"mysql-octavia-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"octavia" | string
		#connection_password:    string
		#connection_database:    *"octavia" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string
		// connection_recycle_time should always be ~10% smaller
		// than the haproxy timeoutClient.
		connection_recycle_time: *280 | int
	}
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
		service_token_roles: *["admin"] | [string, ...]
	}
	service_auth: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
	}
	cache: {
		backend:          "oslo_cache.memcache_pool"
		enabled:          false
		backend_argument: *[
					"memcached_expire_time:660",
		] | [...string]
	}
	haproxy_amphora: {
		server_ca:   "/etc/octavia/octavia-certificates/server_ca.cert.pem"
		client_cert: "/etc/octavia/octavia-certificates/client.cert-and-key.pem"
	}
	certificates: {
		ca_private_key: "/etc/octavia/octavia-certificates/server_ca.key.pem"
		ca_certificate: "/etc/octavia/octavia-certificates/server_ca.cert.pem"
	}
	health_manager: {
		controller_ip_port_list: [...string]
	}
}
