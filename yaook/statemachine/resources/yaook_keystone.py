#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Yaook Keystone resources
########################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: KeystoneEndpoint

.. autoclass:: InstancedKeystoneUser

.. autoclass:: TemplatedKeystoneEndpoint

"""
import typing

import kubernetes_asyncio.client as kclient

from .. import context, interfaces
from .base import DependencyMap, ResourceBody
from .k8s import BodyTemplateMixin, KubernetesReference
from .yaook import YaookReadyResource


class KeystoneUser(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.custom_resource_interface(
            "yaook.cloud", "v1", "keystoneusers",
            api_client,
        )

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        return ((current.get("spec", {}).get("password_secret", None) !=
                new.get("spec", {}).get("password_secret", None)) or
                (current.get("spec", {}).get("keystoneRef", {}) !=
                new.get("spec", {}).get("keystoneRef", {})))


class StaticKeystoneUser(KeystoneUser):
    def __init__(self, *,
                 username: str,
                 keystone: KubernetesReference[typing.Mapping],
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.username = username
        self.keystone = keystone

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:
        keystone_ref = await self.keystone.get(ctx)
        return {
            "apiVersion": "yaook.cloud/v1",
            "kind": "KeystoneUser",
            "metadata": {
                "generateName": "{}-".format(self.username),
            },
            "spec": {
                "keystoneRef": {
                    "name": keystone_ref.name,
                    "kind": ctx.parent_spec["keystoneRef"]["kind"]
                },
            },
        }


class StaticKeystoneUserWithParent(KeystoneUser):
    def __init__(self, *,
                 username: str,
                 role: typing.Optional[str] = None,
                 keystone: KubernetesReference[typing.Mapping],
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.username = username
        self.role = role
        self._keystone = keystone

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> ResourceBody:
        keystone_ref = await self._keystone.get(ctx)
        username = f"{self.username}-{ctx.parent_name}"
        body = {
            "apiVersion": "yaook.cloud/v1",
            "kind": "KeystoneUser",
            "metadata": {
                "generateName": f"{username}-",
            },
            "spec": {
                "keystoneRef": {
                    "name": keystone_ref.name,
                    "kind": ctx.parent_spec["keystoneRef"]["kind"],
                },
            },
        }
        if self.role:
            body["spec"]["role"] = self.role  # type: ignore
        return body


class KeystoneEndpoint(YaookReadyResource):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.custom_resource_interface(
            "yaook.cloud", "v1", "keystoneendpoints",
            api_client,
        )

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        return (
            (current.get("spec", {}).get("endpoints", None) !=
             new.get("spec", {}).get("endpoints", None)) or
            (current.get("spec", {}).get("description", None) !=
             new.get("spec", {}).get("description", None)) or
            (current.get("spec", {}).get("keystoneRef", None) !=
             new.get("spec", {}).get("keystoneRef", None)) or
            (current.get("spec", {}).get("region", None) !=
             new.get("spec", {}).get("region", None))
        )


class InstancedKeystoneUser(KeystoneUser):
    def __init__(
            self, *,
            prefix: str,
            keystone: KubernetesReference[typing.Mapping],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._name_prefix = prefix
        self._keystone = keystone

    async def _make_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        keystone_ref = await self._keystone.get(ctx)
        return {
            "apiVersion": "yaook.cloud/v1",
            "kind": "KeystoneUser",
            "metadata": {
                "generateName": "{}-{}-".format(self._name_prefix,
                                                ctx.instance),
            },
            "spec": {
                "keystoneRef": {
                    "name": keystone_ref.name,
                    "kind": ctx.parent_spec["keystoneRef"]["kind"],
                },
            },
        }


class TemplatedKeystoneEndpoint(BodyTemplateMixin, KeystoneEndpoint):
    """
    Manage a jinja2-templated KeystoneEndpoint.

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.
    """
