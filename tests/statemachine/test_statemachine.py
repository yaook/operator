#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
import uuid
from unittest.mock import sentinel

import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.statemachine as sm
from yaook.statemachine.resources import ReadyInfo


class TestStateMachine(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.cr = unittest.mock.Mock([])
        self.ctx = unittest.mock.Mock(["logger"])
        self.ctx.logger = unittest.mock.Mock()
        self.sm = sm.StateMachine(self.cr)

    def _mock_state(self):
        s = unittest.mock.Mock(["component", "get_listeners"])
        s.reconcile = unittest.mock.AsyncMock()
        s.is_ready = unittest.mock.AsyncMock()
        s.component = str(uuid.uuid4())
        return s

    def test_get_listeners_returns_listeners_of_all_states(self):
        s1 = self._mock_state()
        s1.get_listeners.return_value = [
            unittest.mock.sentinel.s1_l1,
            unittest.mock.sentinel.s1_l2,
        ]

        s2 = self._mock_state()
        s2.get_listeners.return_value = [
            unittest.mock.sentinel.s2_l1,
            unittest.mock.sentinel.s2_l2,
        ]

        self.sm.add_state(s1)
        self.sm.add_state(s2)

        self.assertCountEqual(
            self.sm.get_listeners(),
            [
                unittest.mock.sentinel.s1_l1,
                unittest.mock.sentinel.s1_l2,
                unittest.mock.sentinel.s2_l1,
                unittest.mock.sentinel.s2_l2,
            ]
        )

    def test_get_listeners_is_empty_by_default(self):
        self.assertCountEqual(self.sm.get_listeners(), [])

    def test_states_iterates_all_states(self):
        s1 = self._mock_state()
        s2 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)

        self.assertCountEqual(
            self.sm.states,
            [s1, s2],
        )

    def test_add_state_requires_string_component(self):
        s = self._mock_state()
        s.component = unittest.mock.sentinel.foo

        with self.assertRaisesRegex(
                TypeError,
                "is not a string"):
            self.sm.add_state(s)

    def test_add_state_rejects_duplicate_component(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s2.component = s1.component

        self.sm.add_state(s1)

        with self.assertRaisesRegex(
                ValueError,
                "component .* already defined by"):
            self.sm.add_state(s2)

    async def test_ensure_runs_states(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)
        self.sm.add_state(s3)

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))

            await self.sm.ensure(self.ctx)

        self.assertCountEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
                unittest.mock.call(s3, self.ctx),
            ]
        )

    async def test_ensure_runs_states_in_dependency_order(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()

        self.sm.add_state(s3)
        self.sm.add_state(
            s1,
            dependencies=[s3],
        )
        self.sm.add_state(
            s2,
            dependencies=[s1, s3],
        )

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))

            await self.sm.ensure(self.ctx)

        self.assertSequenceEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s3, self.ctx),
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
            ]
        )

    async def test_ensure_does_not_run_states_depending_on_states_which_returned_False(self):  # NOQA
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()

        self.sm.add_state(s3)
        self.sm.add_state(
            s1,
            dependencies=[s3],
        )
        self.sm.add_state(
            s2,
            dependencies=[s1, s3],
        )

        def result_generator():
            yield ReadyInfo.cast(True)
            yield ReadyInfo.cast(False)

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))
            _ensure_state.side_effect = result_generator()

            await self.sm.ensure(self.ctx)

        self.assertSequenceEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s3, self.ctx),
                unittest.mock.call(s1, self.ctx),
            ]
        )

    async def test__ensure_state_injects_dependencies(self):
        s = self._mock_state()
        d1 = self._mock_state()
        d2 = self._mock_state()

        self.sm.add_state(d1)
        self.sm.add_state(d2)
        self.sm.add_state(s, dependencies=[d1, d2])

        await self.sm._ensure_state(
            s,
            self.ctx,
        )

        s.reconcile.assert_awaited_once_with(
            self.ctx,
            dependencies={
                d1.component: d1,
                d2.component: d2,
            },
        )

    async def test__ensure_state_returns_result_of_is_ready(self):
        s = self._mock_state()
        s.is_ready.return_value = unittest.mock.sentinel.result

        self.sm.add_state(s)

        result = await self.sm._ensure_state(
            s,
            self.ctx,
        )

        s.is_ready.assert_called_once_with(self.ctx)

        self.assertEqual(result, unittest.mock.sentinel.result)

    async def test__ensure_state_returns_cast_ContinueableError(self):
        exc = exceptions.ContinueableError(
            "foo",
            ready=unittest.mock.sentinel.result,
        )

        s = self._mock_state()
        s.reconcile.side_effect = exc

        self.sm.add_state(s)

        with contextlib.ExitStack() as stack:
            cast = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.base.ReadyInfo.cast",
            ))
            cast.return_value = sentinel.result

            result = await self.sm._ensure_state(
                s,
                self.ctx,
            )

        cast.assert_called_once_with(exc)

        self.assertEqual(result, sentinel.result)

    async def test__ensure_state_reraises_other_exceptions(self):  # NOQA
        class FooException(Exception):
            pass

        exc = FooException()

        s = self._mock_state()
        s.reconcile.side_effect = exc

        self.sm.add_state(s)

        with self.assertRaises(FooException) as exc:
            await self.sm._ensure_state(s, self.ctx)

    async def test_ensure_runs_all_possible_states(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()
        s4 = self._mock_state()
        s5 = self._mock_state()
        s6 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)
        self.sm.add_state(s3, dependencies=[s2])
        self.sm.add_state(s4)
        self.sm.add_state(s5, dependencies=[s1])
        self.sm.add_state(s6, dependencies=[s4, s5])

        def result_generator(state, _):
            return ReadyInfo.cast(state != s2)

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))
            _ensure_state.side_effect = result_generator

            await self.sm.ensure(self.ctx)

        self.assertCountEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
                unittest.mock.call(s4, self.ctx),
                unittest.mock.call(s5, self.ctx),
                unittest.mock.call(s6, self.ctx),
            ]
        )

    async def test_ensure_requires_all_dependencies_to_be_ready(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()
        s4 = self._mock_state()
        s5 = self._mock_state()
        s6 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)
        self.sm.add_state(s3, dependencies=[s2])
        self.sm.add_state(s4)
        self.sm.add_state(s5, dependencies=[s1])
        self.sm.add_state(s6, dependencies=[s4, s5])

        def result_generator(state, _):
            return ReadyInfo.cast(state != s2 and state != s5)

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))
            _ensure_state.side_effect = result_generator

            await self.sm.ensure(self.ctx)

        self.assertCountEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
                unittest.mock.call(s4, self.ctx),
                unittest.mock.call(s5, self.ctx),
            ]
        )

    async def test_ensure_returns_all_blocked_states(self):  # NOQA
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()
        s4 = self._mock_state()
        s5 = self._mock_state()
        s6 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)
        self.sm.add_state(s3, dependencies=[s2])
        self.sm.add_state(s4)
        self.sm.add_state(s5, dependencies=[s1])
        self.sm.add_state(s6, dependencies=[s4, s5])

        def result_generator(state, _):
            return ReadyInfo.cast(state != s5 and state != s3)

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))
            _ensure_state.side_effect = result_generator

            result = await self.sm.ensure(self.ctx)

        self.assertCountEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
                unittest.mock.call(s3, self.ctx),
                unittest.mock.call(s4, self.ctx),
                unittest.mock.call(s5, self.ctx),
            ]
        )

        self.assertCountEqual(result, [s5, s3])

    async def test_ensure_returns_last_state_if_stalled(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()
        s4 = self._mock_state()
        s5 = self._mock_state()
        s6 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)
        self.sm.add_state(s3, dependencies=[s2])
        self.sm.add_state(s4)
        self.sm.add_state(s5, dependencies=[s1])
        self.sm.add_state(s6, dependencies=[s4, s5])

        def result_generator(state, _):
            return ReadyInfo.cast(state != s6)

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))
            _ensure_state.side_effect = result_generator

            result = await self.sm.ensure(self.ctx)

        self.assertCountEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
                unittest.mock.call(s3, self.ctx),
                unittest.mock.call(s4, self.ctx),
                unittest.mock.call(s5, self.ctx),
                unittest.mock.call(s6, self.ctx),
            ]
        )

        self.assertCountEqual(result, [s6])

    async def test_ensure_returns_empty_if_all_states_succeded(self):
        s1 = self._mock_state()
        s2 = self._mock_state()
        s3 = self._mock_state()
        s4 = self._mock_state()
        s5 = self._mock_state()
        s6 = self._mock_state()

        self.sm.add_state(s1)
        self.sm.add_state(s2)
        self.sm.add_state(s3, dependencies=[s2])
        self.sm.add_state(s4)
        self.sm.add_state(s5, dependencies=[s1])
        self.sm.add_state(s6, dependencies=[s4, s5])

        def result_generator(state, _):
            return True

        with contextlib.ExitStack() as stack:
            _ensure_state = stack.enter_context(unittest.mock.patch.object(
                self.sm, "_ensure_state",
            ))
            _ensure_state.side_effect = result_generator

            result = await self.sm.ensure(self.ctx)

        self.assertCountEqual(
            _ensure_state.await_args_list,
            [
                unittest.mock.call(s1, self.ctx),
                unittest.mock.call(s2, self.ctx),
                unittest.mock.call(s3, self.ctx),
                unittest.mock.call(s4, self.ctx),
                unittest.mock.call(s5, self.ctx),
                unittest.mock.call(s6, self.ctx),
            ]
        )

        self.assertCountEqual(result, [])
