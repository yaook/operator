#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import typing

import kubernetes_asyncio.client as kclient
from opentelemetry import trace

import yaook.common.config
import yaook.op.common

import yaook.op.scheduling_keys
import yaook.statemachine as sm
from yaook.statemachine import api_utils, context, watcher
from yaook.statemachine.resources.instancing import (
    InstanceState, InstanceGroup, ResourceInfo
)
from yaook.statemachine.exceptions import ConfigurationInvalid
from yaook.statemachine.tracing import start_as_current_span_async


tracer = trace.get_tracer(__name__)

T = typing.TypeVar("T")


def _get_per_node_special_config(
        config_templates: typing.List[typing.Mapping],
        node_labels: typing.Mapping[str, str],
        section: str,
        logger: logging.Logger,
        ) -> typing.List[typing.Mapping]:
    extracted_configs = yaook.op.common.extract_labeled_configs(
        config_templates,
        node_labels,
    )
    logger.debug(
        "extracted configs %r from %r for labels %r",
        extracted_configs,
        config_templates,
        node_labels,
    )
    configs = yaook.op.common.transpose_config(
        extracted_configs,
        [section],
    )
    logger.debug(
        "transposed configs %r from %r",
        configs,
        extracted_configs,
    )
    return configs.pop(section)


async def _bgp_spec_configkeys(
        ctx: sm.Context,
        setupkey: str
) -> typing.Set[str]:
    return ctx.parent_spec["setup"][setupkey].get("bgp", {}).keys()


class NeutronL2Agents(sm.StatefulAgentResource):
    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", "neutronl2agents",
                self._handle_event,
                broadcast=True,
            ),
        ]


class NeutronOVNAgent(sm.SingleObject[typing.Mapping]):
    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_ovn_agent_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        relevant_keys = [
            "bridgeConfig",
            "caConfigMapName",
            "neutronMetadataAgentConfig",
            "keystoneRef",
            "ovnMonitorAll",
            "southboundInactivityProbe",
            "scrapeInterval",
            "northboundServers",
            "southboundServers",
            "imagePullSecrets",
            "region",
            "resources",
            "serviceMonitor",
            "issuerRef",
            "targetRelease",
            "imageRef",
        ]
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in relevant_keys
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in relevant_keys
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    def _collect_bridge_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        configs = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"]["controller"]["configTemplates"],
            node_labels,
            'bridgeConfig',
            ctx.logger,
        )
        if not configs:
            return []

        # TODO: do we really want to support only one bridge mapping section?
        # many mappings are ok, but they must be configured at same label...
        primary = configs[0]
        for cfg in configs[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting bridge configurations for "
                    f"{ctx.instance}: {primary} != {cfg}\n%s" % repr(configs)
                )

        return configs[0]

    def _collect_neutron_metadata_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            ) -> typing.Collection[typing.Mapping]:
        return _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"]["controller"]["configTemplates"],
            node_labels,
            'neutronMetadataAgentConfig',
            ctx.logger,
        )

    async def _collect_ovsdb_servers(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            service_name: str,
    ) -> typing.List:
        ovsdb_schema = \
            yaook.op.common.OVSDBSchema.schema_params_by_label(service_name)
        ovsdb = typing.cast(
            sm.OptionalKubernetesReference,
            dependencies.get(ovsdb_schema.access_service)
        )
        service_ref = await ovsdb.get_all(ctx)
        ovsdb_servers = await yaook.op.common.get_ovn_db_servers(
            ctx, service_ref, ovsdb_schema.port)
        return ovsdb_servers

    async def _deployed_on_compute_node(
            self,
            node_labels: typing.Mapping[str, str],
            ) -> bool:
        param = yaook.op.scheduling_keys.SchedulingKey. \
            COMPUTE_HYPERVISOR.value in node_labels
        return param

    @start_as_current_span_async(tracer, "get_used_resources")
    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "neutronovnagents",
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        result.append(sm.ResourceReference.config_map(
            ctx.namespace, instance["spec"]["caConfigMapName"]))
        return result


class TemplatedNeutronOVNAgent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronOVNAgent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await yaook.op.common.get_node_labels_from_instance(ctx)
        result["vars"]["bridge_config"] = \
            self._collect_bridge_config(ctx, node_labels)
        compute_node = await self._deployed_on_compute_node(node_labels)
        result["vars"]["deployed_on_compute_node"] = compute_node
        result["vars"]["neutron_metadata_agent_config"] = \
            self._collect_neutron_metadata_agent_config(ctx, node_labels)
        if not compute_node:
            result["vars"]["northbound_servers"] = \
                await self._collect_ovsdb_servers(
                    ctx, dependencies,
                    yaook.op.common.OVSDBSchema.northbound.name
                )
        result["vars"]["southbound_servers"] = \
            await self._collect_ovsdb_servers(
                ctx, dependencies, yaook.op.common.OVSDBSchema.southbound.name
            )
        return result


class NeutronOVNAgents(sm.StatefulAgentResource):

    def __init__(self, *args, **kwargs):
        super().__init__(
            ignore_grouping_labels=[yaook.op.scheduling_keys.SchedulingKey
                                    .COMPUTE_HYPERVISOR.value],
            *args, **kwargs)

    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", "neutronovnagents",
                self._handle_event,
                broadcast=True,
            ),
        ]


class NeutronOVNBGPAgent(sm.SingleObject[typing.Mapping]):
    RELEVANT_KEYS: typing.List[str] = [
        "addressScopes",
        "bgpNodeAnnotationSuffix",
        "bgpAgentConfig",
        "bridgeName",
        "driver",
        "debug",
        "hostname",
        "issuerRef",
        "localAS",
        "lockName",
        "peers",
        "syncInterval",
    ]

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> sm.ResourceInterfaceWithStatus[typing.Mapping]:
        return sm.neutron_ovn_bgp_agent_interface(api_client)

    def _needs_update(self, old: typing.Mapping, new: typing.Mapping) -> bool:
        old_spec = old["spec"]
        old_spec_pruned = {
            k: v for k, v in old_spec.items()
            if k in self.RELEVANT_KEYS
        }
        new_spec = new["spec"]
        new_spec_pruned = {
            k: v for k, v in new_spec.items()
            if k in self.RELEVANT_KEYS
        }
        return (super()._needs_update(old, new) or
                old_spec_pruned != new_spec_pruned)

    @start_as_current_span_async(tracer, "get_used_resources")
    async def get_used_resources(
            self,
            ctx: sm.Context,
            ) -> typing.Iterable[sm.ResourceReference]:
        result = list(await super().get_used_resources(ctx))
        try:
            instance = await self._get_current(ctx, True)
        except sm.ResourceNotPresent:
            return result

        pods = sm.pod_interface(ctx.api_client)
        label_selector = sm.LabelSelector(
            match_labels={
                sm.context.LABEL_PARENT_PLURAL: "neutronovnbgpagents",
                sm.context.LABEL_PARENT_GROUP: "network.yaook.cloud",
                sm.context.LABEL_PARENT_NAME: instance["metadata"]["name"],
            }
        )
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=label_selector.as_api_selector()):
            result.extend(api_utils.extract_pod_references(
                pod.spec, ctx.namespace,
            ))
        return result

    async def get_node_labels_from_instance(
            self,
            ctx: sm.Context) -> typing.Mapping[str, str]:
        core_client = kclient.CoreV1Api(ctx.api_client)
        data = typing.cast(typing.Mapping, ctx.instance_data)
        return (await core_client.read_node(data["node"])).metadata.labels

    def _collect_interface_mapping(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            config_key: str,
            ) -> typing.Mapping:
        interface_mappings = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'bgpInterfaceMapping',
            ctx.logger,
        )
        # We only support one interface config per bgp agent
        try:
            primary = interface_mappings[0]
        except IndexError:
            raise sm.ConfigurationInvalid(
                f"bgpInterfaceMapping for bgp agent '{str(config_key)}' is "
                "missing."
            )
        for cfg in interface_mappings[1:]:
            if cfg != primary:
                raise sm.ConfigurationInvalid(
                    "found multiple conflicting bgp interface configurations "
                    f"for {ctx.instance}: {primary} != {cfg}\n%s" % (
                        repr(interface_mappings))
                )
        return interface_mappings[0]

    def _collect_ovn_bgp_agent_config(
            self,
            ctx: sm.Context,
            node_labels: typing.Mapping[str, str],
            config_key: str,
            ) -> typing.Dict[str, typing.List[typing.Mapping]]:
        config_section = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'config',
            ctx.logger,
        )
        bgp_agent_config_section = _get_per_node_special_config(
            ctx.parent_spec["setup"]["ovn"].get("bgp", {})
            .get(config_key).get("configTemplates", []),
            node_labels,
            'bgpAgentConfig',
            ctx.logger,
        )
        return {
            'config': config_section,
            'bgpAgentConfig': bgp_agent_config_section
        }


class TemplatedNeutronOVNBGPAgent(
        yaook.statemachine.resources.BodyTemplateMixin,
        NeutronOVNBGPAgent):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> yaook.statemachine.resources.TemplateParameters:
        result = await super()._get_template_parameters(ctx, dependencies)
        node_labels = await self.get_node_labels_from_instance(ctx)
        config_key = typing.cast(
            typing.Mapping,
            ctx.instance_data,
        )["configkey"]

        configs = self._collect_ovn_bgp_agent_config(
            ctx,
            node_labels,
            config_key,
        )
        config = configs["config"].pop()
        if len(configs["bgpAgentConfig"]) > 0:
            bgpAgentConfig = configs["bgpAgentConfig"].pop()
        else:
            bgpAgentConfig = {}

        result["vars"]["address_scopes"] = config.get("addressScopes", [])
        result["vars"]["peers"] = config["peers"]
        result["vars"]["config_key"] = config_key
        result["vars"]["bgp_local_as"] = config["localAS"]
        result["vars"]["bridge_name"] = config["bridgeName"]
        result["vars"]["driver"] = config["driver"]
        result["vars"]["debug"] = config["debug"]
        result["vars"]["sync_interval"] = config["syncInterval"]
        result["vars"]["bgpAgentConfig"] = bgpAgentConfig
        return result


class NeutronBGPAgents(sm.L2AwareStatefulAgentResource):
    def __init__(
        self,
        *,
        scheduling_keys: typing.Collection[str],
        setup_key: str,
        **kwargs: typing.Any
    ):
        super().__init__(
            scheduling_keys=scheduling_keys,
            **kwargs)
        self.setup_key = setup_key
        self.listener_key = "neutronovnbgpagents"

    def _get_node_name(self, resource_info: ResourceInfo) -> str:
        return resource_info.body["spec"]["hostname"]

    def get_listener(self) -> typing.List[sm.Listener]:
        return [
            sm.KubernetesListener[typing.Mapping](
                "network.yaook.cloud", "v1", self.listener_key,
                self._handle_event,
                broadcast=True,
            ),
        ]

    @start_as_current_span_async(tracer, "get_target_instances")
    async def get_target_instances(
            self,
            ctx: sm.Context,
            ) -> typing.Mapping[str, typing.Any]:
        nodes = await self._get_target_nodes(ctx)
        configkeys = await _bgp_spec_configkeys(ctx, self.setup_key)
        lockNames_check = set()
        target_instances = {}
        for configkey in configkeys:
            # Kubernetes annotations are split into (optional) prefix/name.
            # The bgp annotation name used by the bgp-operator for the l2-lock
            # is "bgp-<configkey>". The maximum annotation name length is 63
            # characters (https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/#syntax-and-character-set).   # noqa E501
            # That means that the <configkey>-part of the name must have a
            # maximum length of 59 characters
            strippedConfigkey = configkey[:59].rstrip("-_.")
            lockName = "bgp-" + strippedConfigkey
            # here we check if we cut at an unfortunate position and a
            # non-alphanumeric character is at the last position (this is
            # invalid for kubernetes)
            if not api_utils.is_k8s_regex_valid(lockName):
                raise ConfigurationInvalid(
                    f"The lockName {lockName} does not match the k8s regex "
                    "validation"
                )
            # We add the lockName to a set and double check at the end
            # if the length of the configkeys and the check-set are equal.
            lockNames_check.add(lockName)
            for node in nodes:
                # We need to cut the target instances to 63 characters or the
                # generating of the bgpdr agent cr will fail at the lookup of
                # the ca certs with an instance because the instance is too
                # long.

                # The names for the target instances have the configkey and the
                # node name seperated by a '.' in it (for useability /
                # readability).
                # The maximum length for cr-names is 63 characters (by
                # kubernetes) that's why we also need to check if our
                # combinations of configkey + '.' + hostname are unique when
                # cut at the 63th character
                bgp_agent_name = (
                    strippedConfigkey + '.' + node.metadata.name
                )[:63].rstrip("-_.")
                # here we check if we cut at an unfortunate position and a
                # non-alphanumeric character is at the last position (this is
                # invalid for kubernetes)
                if not api_utils.is_k8s_regex_valid(bgp_agent_name):
                    raise ConfigurationInvalid(
                        "The bgp agent name shortened to 63 characters "
                        "does not match the k8s regex. Wrong name: "
                        f"{bgp_agent_name[:63]}"
                    )
                # If the bgp_agent_name is valid we add it to the
                # target_instances.
                target_instances[bgp_agent_name] = {
                    "node": node.metadata.name,
                    "configkey": configkey,
                    "lockName": lockName,
                }
        # Check if the number of configkeys is equal to the number of
        # lockNames
        if len(configkeys) != len(lockNames_check):
            raise ConfigurationInvalid(
                "The configkeys for the bgp agents are equal till the 59th "
                "character and cannot be created. Please use names that "
                "differ in the first 59 characters."
                # good config keys : firstbgp, secondbgp, thirdbgp
                # bad config keys : mybgpagent1, mybgpagent2, mybgpagent3
            )
        # We should get count_of_specified_bgp_agents (configkeys) multiplied
        # by count_of_nodes (nodes) target_instances. If not then something
        # went wrong and we should not procceed.
        if (len(nodes)*len(configkeys)) != len(target_instances):
            raise ConfigurationInvalid(
                "The configkeys and the node names do not differ when put "
                "together with a point in between. Use shorter keys so "
                "when combining the config keys with your node names they "
                "differ in the first 64 characters."
            )
        return target_instances

    def _does_node_require_maintenance(
            self,
            ctx: sm.Context,
            ) -> bool:
        if ctx.instance_data is None:
            raise AssertionError()
        nodes = sm.resources.openstack.nodes_in_maintenance.get()
        for node in nodes:
            if node == ctx.instance_data["node"]:
                return True

        return False

    @start_as_current_span_async(tracer, "_group_instances")
    async def _group_instances(
            self,
            ctx: context.Context,
            instances: typing.Mapping[str, InstanceState]
            ) -> typing.List[typing.Tuple[
                InstanceGroup, typing.Mapping[str, InstanceState]]]:
        group_instances: typing.List[typing.Tuple[
            InstanceGroup, typing.Mapping[str, InstanceState]]] = []
        config_keys = await _bgp_spec_configkeys(ctx, self.setup_key)
        for config_key in config_keys:
            configkey_instances = {}
            for instance, instance_state in instances.items():
                if instance.startswith(config_key + "."):
                    configkey_instances[instance] = instance_state

            # Note: this will create the same disruption budgets for each
            # config key
            group_instances_per_config = await super()._group_instances(
                ctx, configkey_instances)
            for group_instance in group_instances_per_config:
                group_instance[0].field_manager = "%s-%s-%s" % (
                    config_key, ctx.parent_kind, self.component)
            group_instances.extend(group_instances_per_config)

        return group_instances


class OVSDBAwareDeployment(sm.TemplatedDeployment):
    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.resources.DependencyMap
            ) -> sm.resources.TemplateParameters:
        param = await super()._get_template_parameters(ctx, dependencies)
        cluster_domain = api_utils.get_cluster_domain()
        for db_schema in yaook.op.common.OVSDBSchema:
            stanza = db_schema.value.stanza
            service_depencency = typing.cast(
                sm.Service,
                dependencies[f"ovsdb_{stanza}_service"],
            )
            service_ref = \
                await service_depencency.get_all(ctx)
            ovsdb_service_list = []
            for ovsdb_pod_service in service_ref.values():
                ovsdb_service_list.append(
                    # FIXME: Make protocol user configurable
                    f"ssl:{ovsdb_pod_service.name}."
                    f"{ovsdb_pod_service.namespace}.svc."
                    f"{cluster_domain}:{db_schema.value.port}"
                )
            param[f"ovsdb_{stanza}"] = \
                ",".join(sorted(ovsdb_service_list))
        return param


class OVNAgentVersionDependency(sm.ReadyForeignReference):

    def __init__(
            self,
            *,
            ovn_image: sm.ConfigurableVersionedDockerImage,
            **kwargs: typing.Any,
            ):
        super().__init__(
            resource_interface_factory=sm.neutron_ovn_agent_interface,
            **kwargs)
        self.ovn_image = ovn_image

    def get_resource_references(
            self,
            ctx: context.Context,
            ) -> typing.Collection[
                kclient.V1ObjectReference
            ]:
        return []

    def _handle_dependency_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[T],
            ) -> bool:
        status = event.raw_object.get('status', {})
        if 'ovnVersion' in status:
            if event.type_ == watcher.EventType.MODIFIED:
                old_status = event.old_raw_object.get('status', {})
                if 'ovnVersion' in old_status and \
                        status['ovnVersion'] == old_status['ovnVersion']:
                    return False
            return True
        return False

    @start_as_current_span_async(tracer, "is_ready")
    async def is_ready(self, ctx: context.Context) -> bool:
        ovn_url = await self.ovn_image.get_versioned_url(ctx)
        version_format = sm.OVNVersion()

        try:
            ovn_semver, _ = version_format.parse(ovn_url.split(':')[-1])
            ovn_version = version_format.serialize_software_build(ovn_semver)
        except ValueError:
            raise ConfigurationInvalid(
                f'No OVN version in image url: {ovn_url}, '
                'OVN upgrade path cannot be enforced!'
            )

        agent_interface = self.get_resource_interface(ctx)
        agents = await agent_interface.list_(ctx.namespace)

        out_of_date_agents = [
            agent for agent in agents
            if 'ovnVersion' not in agent.get('status', {})
            or agent['status']['ovnVersion'] != ovn_version]

        ctx.logger.debug("out-of-date OVN agents: %r", out_of_date_agents)
        return len(out_of_date_agents) == 0
