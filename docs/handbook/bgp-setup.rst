.. _bgp-setup:

BGP Setup with ovn-bgp-agent
==================================

OpenStack neutron with ovn supports the ovn-bgp-agent to announce customer
networks to an external network. Yaook is able to create and configure this
bgp agents.

Technical Setup
---------------

Yaook assumes, the bgp agent needs to speak to the external network, to
announce routes. Therefore the agent needs to be connected to the external
network.

To create this connection, Yaook creates a Pod running the ovn-bgp-agent,
with it's own pod network namespace. A ovs port is created, attached to the
ovs bridge representing the external network. The port is connected to the bgp
pod network namespace and gets an IP address, so bgp agent can use it to
communicate to the external network.

This way, we achieve connection the bgp agent with the external network and
still protect the nodes running the pods from being directly connected to the
external network.

Setup and configure a bgp agent
-------------------------------

To run a ovnbgpagent using Yaook, some options needs to be configured:

#. add bgp configuration at neutron deployment
#. set IP annotation to nodes running the bgp agents
#. set ID annotation to the nodes running the bgp agents
#. set bgp label to nodes running the agents

add bgp configuration
^^^^^^^^^^^^^^^^^^^^^

see also :ref:`examples.neutron`

Edit your neutron deployment to have at ``spec:setup:ovn`` the section `bgp`.
Next define a *config_key* with any name you want and define the
`configTemplates` as value. The name will be used for naming the agents.
As only one bgp agent can run one speaker (but a speaker can have many agents,
e.g. at each node agent), you need to configure many *config_keys* to run for
each speaker bgp agents.

One required config option is the `bridgeName`, where the bridge name of the
ovs bridge needs to be defined, the bgp agent should be connected to. It is
the same name like one of the bridges, defined in `ovn:bridge_mappings` at
`l2` section.

As this bridge is mostly setup at nodes with label
`network.yaook.cloud/neutron-ovn-agent: "true"`, you can also run the bgp agent
on these nodes. But it can also run on different nodes, make sure the
`bridge_mappings` exists there.

Additionaly define `addressScopes`, `driver`, `localAS` and `peers`.

IP annotation
^^^^^^^^^^^^^

Yaook needs to know, which IP to define at the interface connected to the
external network (via ovs bridge) used for bgp agent.

Therefore an annotation needs to be set, containing the *config_key*. Value is
the IP address with subnet.

Annotation prefix: `bgp-interface-ip.network.yaook.cloud/` (At the code the
variable `ANNOTATION_BGP_INTERFACE_IP` is used.)

Example: `bgp-interface-ip.network.yaook.cloud/mybgpspeaker: 10.2.4.42/24`

ID annotation
^^^^^^^^^^^^^

Yaook also needs to know the ID of the bgp speaker.

Therefore an annotation needs to be set, containing the *config_key*. Value is
the IP address without subnet or an unsigned integer.

Annotation prefix: `bgp-router-id.network.yaook.cloud/` (At the code the
variable `ANNOTATION_BGP_ROUTER_ID` is used.)

Example: `bgp-router-id.network.yaook.cloud/mybgpspeaker: 10.2.4.42`

bgp label at nodes
^^^^^^^^^^^^^^^^^^

see also :ref:`concepts.scheduling`

Each component started by Yaook will only be scheduled on nodes with specific labels.
This is used do define, which component should run on which nodes.

For bgp set the label `network.yaook.cloud/neutron-bgp-agent=true` (code use
the variable `NETWORK_NEUTRON_BGP_AGENT`)
