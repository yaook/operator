OpenStack Policies
==================

The default policies of each OpenStack service are added to the docker image for each version so additional and default policies can be combined by the PolicyValidator using logical ORs.

To create the default policy file for a specific OpenStack service and release you can run the following script with the use of a separate virtual Python environment or a docker container:

    .. code-block:: bash
        
        # set desired service and version
        OS_SERVICE=placement
        OS_RELEASE=2024.1

        # install oslo.policy and openstack service into environment
        pip install git+https://opendev.org/openstack/oslo.policy.git@stable/${OS_RELEASE} \
        || pip install git+https://opendev.org/openstack/oslo.policy.git@unmaintained/${OS_RELEASE} \
        || pip install git+https://opendev.org/openstack/oslo.policy.git@${OS_RELEASE}-eol

        pip install git+https://opendev.org/openstack/${OS_SERVICE}.git@stable/${OS_RELEASE} \
        || pip install git+https://opendev.org/openstack/${OS_SERVICE}.git@unmaintained/${OS_RELEASE} \
        || pip install git+https://opendev.org/openstack/${OS_SERVICE}.git@${OS_RELEASE}-eol

        # create file with default policies
        oslopolicy-policy-generator --namespace ${OS_SERVICE} --output-file policy_${OS_RELEASE}.yaml --exclude-deprecated
