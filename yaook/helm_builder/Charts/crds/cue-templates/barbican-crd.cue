// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#messagequeue
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "BarbicanDeployment"
	#plural:   "barbicandeployments"
	#singular: "barbicandeployment"
	#shortnames: ["barbicand", "barbicands"]
	#releases: ["zed", "2023.1", "2023.2", "2024.1"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"keystoneListener",
			"targetRelease",
			"barbicanConfig",
			"database",
			"messageQueue",
			"memcached",
			"region",
			"issuerRef",
		]
		properties: {
			keystoneRef:      crd.#keystoneref
			api:              crd.apiendpoint
			serviceMonitor:   crd.#servicemonitor
			keystoneListener: crd.replicated
			keystoneListener: crd.topologySpreadConstraints

			barbicanConfig: crd.#anyconfig
			barbicanConfig: #description: "Barbican OpenStack config. "
			barbicanSecrets: crd.#configsecret

			api: {
				description: "Configure the Barbican API deployment"
				properties: resources: {
					type: "object"
					properties: {
						"barbican-api":            crd.#containerresources
						"barbican-worker":         crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"ssl-terminator-internal": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
						"service-reload-internal": crd.#containerresources
					}
				}
			}
			keystoneListener: {
				description: "Configure the Barbican Keystone listener deployment"
				properties: resources: {
					type: "object"
					properties: "barbican-keystone-listener": crd.#containerresources
				}
			}
			jobResources: {
				type:        "object"
				description: "Resource limits for Job Pod containers spawned by the Operator"
				properties: "barbican-db-sync-job": crd.#containerresources
			}
		}
	}
}
