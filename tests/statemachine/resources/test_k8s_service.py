import unittest
from unittest.mock import sentinel
import uuid

import kubernetes_asyncio.client as kclient

import yaook.statemachine.context as context
import yaook.statemachine.resources.k8s_service as k8s_service
import yaook.statemachine.watcher as watcher

from .utils import ResourceTestMixin


class TestService(unittest.TestCase):
    class ServiceTest(ResourceTestMixin, k8s_service.Service):
        pass

    def test_needs_update_returns_true_if_port_added(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[],
            )
        )

        new = {
            "spec": {
                "ports": [
                    {
                        "name": "foo",
                        "port": 31416,
                        "protocol": "TCP",
                        "targetPort": 31416,
                    }
                ]
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_port_modified(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[
                    kclient.V1ServicePort(
                        name="foo",
                        port=31415,
                        protocol="TCP",
                        target_port=31416,
                    ),
                ],
            )
        )

        new = {
            "spec": {
                "ports": [
                    {
                        "name": "foo",
                        "port": 31416,
                        "protocol": "TCP",
                        "targetPort": 31416,
                    }
                ]
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_port_removed(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[
                    kclient.V1ServicePort(
                        name="foo",
                        port=31416,
                        protocol="TCP",
                        target_port=31416,
                    ),
                ],
            )
        )

        new = {
            "spec": {
                "ports": []
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_selector_changed(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                selector=sentinel.old_selector,
                ports=[],
            )
        )

        new = {
            "spec": {
                "selector": sentinel.new_selector,
                "ports": [],
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_missing_selector(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                selector=sentinel.old_selector,
                ports=[],
            )
        )

        new = {
            "spec": {
                "ports": [],
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_missing_port_and_selector(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
            )
        )

        new = {
            "spec": {
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_missing_default_type(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="ClusterIP"
            )
        )

        new = {
            "spec": {
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_different_type_unset(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="LoadBalancer"
            )
        )

        new = {
            "spec": {
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_different_type(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="LoadBalancer"
            )
        )

        new = {
            "spec": {
                "type": "ClusterIP",
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_same_type(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="LoadBalancer"
            )
        )

        new = {
            "spec": {
                "type": "LoadBalancer",
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_false_if_equal(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                selector=sentinel.common_selector,
                ports=[
                    kclient.V1ServicePort(
                        name="foo",
                        port=31416,
                        protocol="TCP",
                        target_port=31416,
                    ),
                ],
            )
        )

        new = {
            "spec": {
                "selector": sentinel.common_selector,
                "ports": [
                    {
                        "name": "foo",
                        "port": 31416,
                        "protocol": "TCP",
                        "targetPort": 31416,
                    }
                ],
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_on_label_change(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[],
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )

        new = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar",
                },
            },
        }

        self.assertTrue(
            s._needs_update(old, new)
        )


class TestIngress(unittest.TestCase):
    class IngressTest(ResourceTestMixin, k8s_service.Ingress):
        pass

    def setUp(self):
        self.component = str(uuid.uuid4())
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.i = self.IngressTest(component=self.component)

    def test_needs_update_returns_true_if_rule_added(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                rules=[],
            ),
        )

        new = {
            "spec": {
                "rules": [
                    {
                        "host": "foo",
                        "http": {
                            "path": "/",
                            "pathType": "Prefix",
                            "backend": {},
                        },
                    },
                ],
            },
        }

        self.assertTrue(
            self.i._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_rule_modified(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                rules=[
                    kclient.V1IngressRule(
                        host="foo",
                        http=kclient.V1HTTPIngressPath(
                            backend=kclient.V1IngressBackend(),
                            path="/",
                            path_type="Prefix",
                        ),
                    ),
                ],
            ),
        )

        new = {
            "spec": {
                "rules": [
                    {
                        "host": "foo",
                        "http": {
                            "path": "/foobar",
                            "pathType": "Prefix",
                            "backend": {},
                        },
                    },
                ],
            },
        }

        self.assertTrue(
            self.i._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_rule_removed(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                rules=[
                    kclient.V1IngressRule(
                        host="foo",
                        http=kclient.V1HTTPIngressPath(
                            backend=kclient.V1IngressBackend(),
                            path="/",
                            path_type="Prefix",
                        ),
                    ),
                ],
            ),
        )

        new = {
            "spec": {
                "rules": [],
            },
        }

        self.assertTrue(
            self.i._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_rule_host_changed(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                rules=[
                    kclient.V1IngressRule(
                        host="foo",
                        http=kclient.V1HTTPIngressPath(
                            backend=kclient.V1IngressBackend(),
                            path="/",
                            path_type="Prefix",
                        ),
                    ),
                ],
            ),
        )

        new = {
            "spec": {
                "rules": [
                    {
                        "host": "foobar",
                        "http": {
                            "path": "/",
                            "pathType": "Prefix",
                            "backend": {},
                        },
                    },
                ],
            },
        }

        self.assertTrue(
            self.i._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_ingess_class_changed(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                ingress_class_name="bar",
            ),
        )

        new = {
            "spec": {
                "ingressClassName": "foobar",
            },
        }

        self.assertTrue(
            self.i._needs_update(old, new)
        )

    def test_needs_update_returns_false_if_ingess_class_unchanged(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                ingress_class_name="bar",
            ),
        )

        new = {
            "spec": {
                "ingressClassName": "bar",
            },
        }

        self.assertFalse(
            self.i._needs_update(old, new)
        )

    def test_needs_update_returns_false_if_equal(self):
        old = kclient.V1Ingress(
            spec=kclient.V1IngressSpec(
                rules=[
                    kclient.V1IngressRule(
                        host="foo",
                        http=kclient.V1HTTPIngressPath(
                            backend=kclient.V1IngressBackend(),
                            path="/",
                            path_type="Prefix",
                        ),
                    ),
                ],
            ),
        )

        new = {
            "spec": {
                'rules': [
                    {
                        'host': 'foo',
                        'http': {
                            'path': '/',
                            "pathType": "Prefix",
                            "backend": {},
                        },
                    },
                ],
            },
        }

        self.assertFalse(
            self.i._needs_update(old, new)
        )

    def test_registers_listener_for_ingress(self):
        self.assertCountEqual(
            self.i.get_listeners(),
            [
                context.KubernetesListener(
                    'networking.k8s.io', 'v1', 'ingresses',
                    self.i._handle_ingress_event,
                    component=self.component,
                )
            ]
        )

    def test_handle_ingress_event_triggers_reconcile_if_deleted(
            self):
        ev = unittest.mock.Mock([])
        ev.type_ = watcher.EventType.DELETED

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    def test_handle_ingress_event_does_nothing_if_no_status(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = None

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    def test_handle_ingress_event_does_nothing_if_status_empty(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = {}

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    def test_handle_ingress_event_reconcile_if_no_old_object(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.old_object = None

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    def test_handle_ingress_event_reconcile_if_no_old_status(self):
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = None

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)

    def test_handle_ingress_event_does_nothing_if_status_unchanged(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.load_balancer = {"ingress": [{"ip": "10.42.24.13"}]}
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = unittest.mock.Mock([])
        ev.old_object.status.load_balancer = {"ingress": [{"ip": "10.42.24.13"}]}

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertFalse(result)

    def test_handle_ingress_event_reconciles_if_status_changed(self):  # noqa: E501
        ev = unittest.mock.Mock(["type_"])
        ev.object_ = unittest.mock.Mock([])
        ev.object_.status = unittest.mock.Mock([])
        ev.object_.status.load_balancer = {"ingress": [{"ip": "10.42.24.13"}]}
        ev.old_object = unittest.mock.Mock([])
        ev.old_object.status = unittest.mock.Mock([])
        ev.old_object.status.load_balancer = {"ingress": [{"ip": "10.42.24.14"}]}

        result = self.i._handle_ingress_event(
            self.ctx,
            ev,
        )

        self.assertTrue(result)
