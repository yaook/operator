#!/bin/bash

set -eu

script_path=$(realpath "$0")
script_dir=${script_path%/*}
script_name=${script_path##*/}

function clean_up() {
  rm -rf /tmp/certs
}

function error_clean_up {
  printf '\nError while executing "%s"' "$script_name" >&2
  clean_up
}

trap error_clean_up ERR
trap clean_up EXIT
trap clean_up SIGINT

# clear old directory
clean_up

# prepare target
mkdir /tmp/certs
chmod 700 /tmp/certs
cp "$script_dir/octavia_openssl.cnf" /tmp/certs/openssl.cnf
cd /tmp/certs
mkdir client_ca
mkdir server_ca

####################################################
# Server
####################################################
cd server_ca
mkdir certs crl newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial

openssl genpkey \
    -algorithm RSA \
    -out private/ca.key.pem \
    -pkeyopt rsa_keygen_bits:4096

chmod 400 private/ca.key.pem

openssl req \
    -batch \
    -config ../openssl.cnf \
    -key private/ca.key.pem \
    -new \
    -x509 \
    -days 7300 \
    -sha256 \
    -extensions v3_ca \
    -out certs/ca.cert.pem

####################################################
# Client
####################################################
cd ../client_ca
mkdir certs crl csr newcerts private
chmod 700 private
touch index.txt
echo 1000 > serial

openssl genpkey \
    -algorithm RSA \
    -out private/ca.key.pem \
    -pkeyopt rsa_keygen_bits:4096

chmod 400 private/ca.key.pem

openssl req \
    -batch \
    -config ../openssl.cnf \
    -key private/ca.key.pem \
    -new \
    -x509 \
    -days 7300 \
    -sha256 \
    -extensions v3_ca \
    -out certs/ca.cert.pem

openssl genpkey \
    -algorithm RSA \
    -out private/client.key.pem \
    -pkeyopt rsa_keygen_bits:2048

openssl req \
    -batch \
    -config ../openssl.cnf \
    -new \
    -sha256 \
    -key private/client.key.pem \
    -out csr/client.csr.pem

openssl ca \
    -batch \
    -config ../openssl.cnf \
    -extensions usr_cert \
    -days 7300 \
    -notext \
    -md sha256 \
    -in csr/client.csr.pem \
    -out certs/client.cert.pem

openssl rsa \
    -in private/client.key.pem \
    -out private/client.cert-and-key.pem

cat certs/client.cert.pem >> private/client.cert-and-key.pem
cd ..

# copy secrets to local point
cp server_ca/certs/ca.cert.pem ./server_ca.cert.pem
cp server_ca/private/ca.key.pem ./server_ca.key.pem
chmod 700 ./server_ca.key.pem
cp client_ca/certs/ca.cert.pem ./client_ca.cert.pem
cp client_ca/private/client.cert-and-key.pem ./client.cert-and-key.pem
chmod 700 ./client.cert-and-key.pem

# push secrets into kubernetes
kubectl create "$@" secret generic octavia-certificates \
    --from-file=server_ca.key.pem \
    --from-file=server_ca.cert.pem \
    --from-file=client_ca.cert.pem \
    --from-file=client.cert-and-key.pem

# removed directory from disc
clean_up
