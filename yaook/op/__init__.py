#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`yaook.op` -- Operator framework
#####################################

.. automodule:: yaook.op.common

.. automodule:: yaook.op.tasks

"""

"""
The following is a patch to backport
https://review.opendev.org/c/openstack/openstacksdk/+/890781
to the current operator, as the fix is not released upstream yet.

After the fix is released we can drop the following code again
"""
import openstack.connection  # noqa: E402
import atexit  # noqa: E402

orig_close = getattr(openstack.connection.Connection, "close")


def close(self):
    orig_close(self)
    atexit.unregister(self.close)


setattr(openstack.connection.Connection, "close", close)
del close
"""
End of Patch
"""
