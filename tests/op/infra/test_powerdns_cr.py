import base64
import unittest
import unittest.mock
import kubernetes_asyncio.client as kclient

from tests import testutils
import tests.op.common_tests as ct
import yaook.common.config as common_config
import yaook.op.common as common
import yaook.op.infra.powerdns_cr as powerdns_cr
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces


NAME = "powerdns-xyz"
NAMESPACE = "infra"
CONFIG_SECRET_NAME = "powerdns-api-key"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"
CONFIG_FILE_NAME = "powerdns.conf"
DB_CLIENT_CONFIG_FILE_NAME = "mariadb_client.conf"


class TestPowerDNSLayer(unittest.IsolatedAsyncioTestCase):
    async def test_get_layer_inserts_subnect_cidr(self):
        layer = powerdns_cr.PowerDNSLayer(target="powerdns")

        ctx = unittest.mock.Mock()

        subnet_cidr = "10.0.0.0/24"
        ctx.parent_spec = {"subnetCidr": subnet_cidr}

        res = await layer.get_layer(ctx)
        expected = {
            "powerdns": common_config.CUTTLEFISH.declare([{
                "#subnet_cidr": subnet_cidr,
            }
            ])}
        self.assertEqual(res, expected)


class TestPowerDNSService(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin):

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

        self._configure_cr(
            powerdns_cr.PowerDNSService,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "PowerDNSService",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "database": ct.get_default_database_config(),
                    "issuerRef": {
                        "name": "issuername"
                    },
                    "targetRelease": "4.9",
                    "replicas": 2,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "subnetCidr": "10.0.0.0/24",
                    "loadBalancerIP": "185.185.185.185",
                    "apiKeySecret": {
                        "secretName": CONFIG_SECRET_NAME,
                        "key": CONFIG_SECRET_KEY
                    },
                    "resources": testutils.generate_resources_dict(
                        "powerdns",
                        "ssl-terminator",
                        "service-reload",
                    ),
                }
            }
        )
        self.run_podspec_tests = False

    async def test_creates_database_and_user(self):
        await ct.test_creates_database_and_user_async(self, NAMESPACE)

    async def test_creates_certificate_and_halts(self):
        await ct.test_creates_certificate_and_halts_async(self, NAMESPACE)

    async def test_certificate_contains_service_name(self):
        await ct.test_certificate_contains_service_name_async(
            self, "pdns_api_service", NAMESPACE
        )

    async def test_certificate_contains_issuer_name(self):
        await ct.test_certificate_contains_issuer_name_async(self, NAMESPACE)

    async def test_creates_database_client_config_with_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_client_config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: common.MYSQL_DATABASE_SERVICE_COMPONENT,
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        mariadb_client_conf = config.data[DB_CLIENT_CONFIG_FILE_NAME]
        cfg = testutils._parse_config(mariadb_client_conf, decode=True)
        section = "client-server"

        db_host = f"{db_service.metadata.name}.{db_service.metadata.namespace}"

        self.assertEqual(cfg.get(section, "host"), db_host)
        self.assertEqual(cfg.get(section, "port"), "3306")
        self.assertEqual(cfg.get(section, "database"), db["spec"]["database"])
        self.assertEqual(cfg.get(section, "user"), db_user['spec']['user'])
        self.assertEqual(cfg.get(section, "password"), db_user_password)

    async def test_creates_config_with_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "pdns_config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: common.MYSQL_DATABASE_SERVICE_COMPONENT,
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        powerdns_conf = config.data[CONFIG_FILE_NAME]
        decoded_conf = base64.b64decode(
            powerdns_conf.encode("ascii")).decode("utf-8")

        # manually add encoded section with arbitrary name since the parser
        # does not support configurations without sections
        cfg = testutils._parse_config("[DEFAULT]\n" + decoded_conf)

        self.assertEqual(
            cfg.get("DEFAULT", "gmysql-host"),
            f"{db_service.metadata.name}.{db_service.metadata.namespace}"
        )
        self.assertEqual(cfg.get("DEFAULT", "gmysql-port"), "3306")
        self.assertEqual(cfg.get("DEFAULT", "gmysql-dbname"), db_name)
        self.assertEqual(
            cfg.get("DEFAULT", "gmysql-user"), db_user['spec']['user']
        )
        self.assertEqual(
            cfg.get("DEFAULT", "gmysql-password"), db_user_password
        )

    async def test_creates_api_deployment_with_replicas_when_all_jobs_succeed(
            self):
        await ct.test_creates_deployment_with_replicas_when_all_jobs_succeed(
            self, "pdns_deployment", NAMESPACE, 2
        )

    async def test_creates_pdns_api_service(self):
        await ct.test_creates_service_for_deployment_async(
            self, NAMESPACE, "pdns_deployment", "pdns_api_service"
        )

    async def test_creates_pdns_webserver_service(self):
        await ct.test_creates_service_for_deployment_async(
            self, NAMESPACE, "pdns_deployment", "pdns_webserver_service"
        )

    async def test_creates_containers_with_resource(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        powerdns_deployment = await self._get_pdns_deployment_async()

        self.assertEqual(
            testutils.container_resources(powerdns_deployment, 0),
            testutils.unique_resources("powerdns")
        )
        self.assertEqual(
            testutils.container_resources(powerdns_deployment, 1),
            testutils.unique_resources("ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(powerdns_deployment, 2),
            testutils.unique_resources("service-reload")
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        ct.test_applies_scheduling_key_to_deployment(
            self, NAMESPACE, "pdns_deployment", powerdns_cr.JOB_SCHEDULING_KEYS
        )

    async def _get_pdns_deployment_async(self) -> kclient.V1Deployment:
        deployments = sm.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "pdns_deployment",
            },
        )
        return deployment
