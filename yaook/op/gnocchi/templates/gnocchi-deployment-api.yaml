##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "gnocchi-api"
  labels:
    app: "gnocchi-api"
spec:
  replicas:  {{ crd_spec.api.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.api.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "gnocchi-api"
          image:  {{ versioned_dependencies['gnocchi_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["gnocchi-api"]
          volumeMounts:
            - name: gnocchi-config-vol
              mountPath: /etc/gnocchi
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
            {% if use_ceph %}
            - name: gnocchi-ceph-conf-volume
              mountPath: /etc/ceph/ceph.conf
              subPath: ceph.conf
            - name: gnocchi-ceph-keyfile-volume
              mountPath: /etc/ceph/keyfile
              subPath: keyfile
            {% endif %}
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          lifecycle:
            preStop:
              exec:
                command:
                - /bin/sleep
                - "5"
          livenessProbe:
            exec:
              command:
                - curl
                - --fail
                - localhost:8080
            timeoutSeconds: 5
          readinessProbe:
            exec:
              command:
                - curl
                - --fail
                - localhost:8080
            timeoutSeconds: 5
          resources: {{ crd_spec | resources('api.gnocchi-api') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: IfNotPresent
          env:
            - name: SERVICE_PORT
              value: "8041"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8041
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8041
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: IfNotPresent
          env:
            - name: SERVICE_PORT
              value: "8042"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8042
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8042
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "ssl-terminator-internal"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: IfNotPresent
          env:
            - name: SERVICE_PORT
              value: "8043"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8043
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8043
              scheme: HTTPS
          resources: {{ crd_spec | resources('api.ssl-terminator-internal') }}
{% endif %}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload-external') }}
{% if crd_spec.api.internal | default(False) %}
        - name: "service-reload-internal"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('api.service-reload-internal') }}
{% endif %}
      volumes:
        - name: gnocchi-config-vol
          projected:
            sources:
            - secret:
                name: {{ dependencies['config'].resource_name() }}
                items:
                  - key: gnocchi.conf
                    path: gnocchi.conf
            - configMap:
                name: {{ dependencies['gnocchi_policy'].resource_name() }}
                items:
                  - key: policy.json
                    path: policy.json
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['external_certificate_secret'].resource_name() }}
        {% if use_ceph %}
        - name: gnocchi-ceph-conf-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: ceph.conf
                path: ceph.conf
        - name: gnocchi-ceph-keyfile-volume
          secret:
            secretName: {{ crd_spec.backends.ceph.keyringReference }}
            items:
              - key: {{ crd_spec.backends.ceph.keyringUsername }}
                path: keyfile
        {% endif %}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.api.internal | default(False) %}
        - name: tls-secret-internal
          secret:
            secretName: {{ dependencies['internal_certificate_secret'].resource_name() }}
        - name: ssl-terminator-internal-config
          emptyDir: {}
{% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
