#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock

import yaook.statemachine.templating as templating

import ddt


class Testto_volume_templates(unittest.TestCase):
    def _join_spec(self, object_reference, extra_spec):
        vol_spec = dict(extra_spec)
        vol_spec.update(ref=object_reference)
        base_spec = {
            "fancy_volume": vol_spec
        }
        return base_spec

    def _make_ref(self, namespace, name):
        return getattr(unittest.mock.sentinel, f"{namespace}_{name}")

    def test_to_volume_templates_returns_joined_spec_for_each_node(self):
        instances = {
            "node-1": self._make_ref("testns", "obja"),
            "node-2": self._make_ref("testns", "objb"),
            "node-3": self._make_ref("testns", "objc"),
        }

        result = templating.to_volume_templates(
            self._join_spec,
            instances,
            {},
        )

        self.assertEqual(
            result,
            {
                "node-1": {
                    "template": {
                        "fancy_volume": {
                            "ref": unittest.mock.sentinel.testns_obja,
                        },
                    },
                },
                "node-2": {
                    "template": {
                        "fancy_volume": {
                            "ref": unittest.mock.sentinel.testns_objb,
                        },
                    },
                },
                "node-3": {
                    "template": {
                        "fancy_volume": {
                            "ref": unittest.mock.sentinel.testns_objc,
                        },
                    },
                },
            },
        )


class Testto_config_map_volumes(unittest.TestCase):
    def _make_ref(self, name):
        ref = unittest.mock.Mock([])
        ref.name = name
        return ref

    def test_to_config_map_volumes_returns_base_spec_for_all_nodes(self):
        instances = {
            "node-1": self._make_ref("obja"),
            "node-2": self._make_ref("objb"),
            "node-3": self._make_ref("objc"),
        }

        result = templating.to_config_map_volumes(
            instances,
            {},
        )

        self.assertEqual(
            result,
            {
                "node-1": {
                    "template": {
                        "configMap": {
                            "name": "obja",
                        },
                    },
                },
                "node-2": {
                    "template": {
                        "configMap": {
                            "name": "objb",
                        },
                    },
                },
                "node-3": {
                    "template": {
                        "configMap": {
                            "name": "objc",
                        },
                    },
                },
            },
        )

    def test_to_config_map_volumes_returns_joined_spec_for_all_nodes(self):
        instances = {
            "node-1": self._make_ref("obja"),
            "node-2": self._make_ref("objb"),
            "node-3": self._make_ref("objc"),
        }

        result = templating.to_config_map_volumes(
            instances,
            {
                "extra": "config",
            },
        )

        self.assertEqual(
            result,
            {
                "node-1": {
                    "template": {
                        "configMap": {
                            "name": "obja",
                            "extra": "config",
                        },
                    },
                },
                "node-2": {
                    "template": {
                        "configMap": {
                            "name": "objb",
                            "extra": "config",
                        },
                    },
                },
                "node-3": {
                    "template": {
                        "configMap": {
                            "name": "objc",
                            "extra": "config",
                        },
                    },
                },
            },
        )


class Testto_secret_volumes(unittest.TestCase):
    def _make_ref(self, name):
        ref = unittest.mock.Mock([])
        ref.name = name
        return ref

    def test_to_secret_volumes_returns_base_spec_for_all_nodes(self):
        instances = {
            "node-1": self._make_ref("obja"),
            "node-2": self._make_ref("objb"),
            "node-3": self._make_ref("objc"),
        }

        result = templating.to_secret_volumes(
            instances,
            {},
        )

        self.assertEqual(
            result,
            {
                "node-1": {
                    "template": {
                        "secret": {
                            "secretName": "obja",
                        },
                    },
                },
                "node-2": {
                    "template": {
                        "secret": {
                            "secretName": "objb",
                        },
                    },
                },
                "node-3": {
                    "template": {
                        "secret": {
                            "secretName": "objc",
                        },
                    },
                },
            },
        )

    def test_to_secret_volumes_returns_joined_spec_for_all_nodes(self):
        instances = {
            "node-1": self._make_ref("obja"),
            "node-2": self._make_ref("objb"),
            "node-3": self._make_ref("objc"),
        }

        result = templating.to_secret_volumes(
            instances,
            {
                "extra": "config",
            },
        )

        self.assertEqual(
            result,
            {
                "node-1": {
                    "template": {
                        "secret": {
                            "secretName": "obja",
                            "extra": "config",
                        },
                    },
                },
                "node-2": {
                    "template": {
                        "secret": {
                            "secretName": "objb",
                            "extra": "config",
                        },
                    },
                },
                "node-3": {
                    "template": {
                        "secret": {
                            "secretName": "objc",
                            "extra": "config",
                        },
                    },
                },
            },
        )


class Testflat_merge_dict(unittest.TestCase):
    def test_joins_two_simple_dictionaries(self):
        d1 = {
            "foo": "bar",
        }
        d2 = {
            "baz": "fnord",
        }

        self.assertEqual(
            templating.flat_merge_dict(d1, d2),
            {
                "foo": "bar",
                "baz": "fnord",
            }
        )

    def test_second_dictionary_wins(self):
        d1 = {
            "foo": "bar",
        }
        d2 = {
            "baz": "fnord",
            "foo": "notbar",
        }

        self.assertEqual(
            templating.flat_merge_dict(d1, d2),
            {
                "foo": "notbar",
                "baz": "fnord",
            }
        )

    def test_does_not_modify_either_dict(self):
        d1 = {
            "foo": "bar",
        }
        d2 = {
            "baz": "fnord",
        }

        templating.flat_merge_dict(d1, d2)

        self.assertEqual(d1, {"foo": "bar"})
        self.assertEqual(d2, {"baz": "fnord"})


class Testparse_chmod(unittest.TestCase):
    def test_parses_octal_number_with_0o_prefix(self):
        self.assertEqual(
            templating.parse_chmod("0o123"),
            0o123,
        )
        self.assertEqual(
            templating.parse_chmod("0o755"),
            0o755,
        )

    def test_raises_if_prefix_is_missing(self):
        with self.assertRaises(ValueError):
            templating.parse_chmod("755")

    def test_parses_fully_explicit_chmod_expression(self):
        self.assertEqual(
            templating.parse_chmod("u=rwx,g=rwx,o=rwx"),
            0o777,
        )
        self.assertEqual(
            templating.parse_chmod("u=r,g=w,o=x"),
            0o421,
        )
        self.assertEqual(
            templating.parse_chmod("u=x,g=r,o=w"),
            0o142,
        )
        self.assertEqual(
            templating.parse_chmod("g=rwx,o=rwx,u=rwx"),
            0o777,
        )
        self.assertEqual(
            templating.parse_chmod("u=rwx,go=rx"),
            0o755,
        )
        self.assertEqual(
            templating.parse_chmod("u=rw,go=r"),
            0o644,
        )
        self.assertEqual(
            templating.parse_chmod("u=r,go="),
            0o400,
        )

    def test_parses_aggregated_chmod_expression(self):
        self.assertEqual(
            templating.parse_chmod("ugo=rwx"),
            0o777,
        )
        self.assertEqual(
            templating.parse_chmod("u=rwx,go=rx"),
            0o755,
        )
        self.assertEqual(
            templating.parse_chmod("u=rw,go=r"),
            0o644,
        )

    def test_parses_a(self):
        self.assertEqual(
            templating.parse_chmod("a=rwx"),
            0o777,
        )

    def test_rejects_relative_expressions(self):
        with self.assertRaisesRegex(
                ValueError,
                "plus and minus are not supported in term"):
            templating.parse_chmod("u-rwx")

    def test_rejects_missing_equal_sign(self):
        with self.assertRaises(ValueError):
            templating.parse_chmod("ufoo")

    def test_rejects_unknown_bits(self):
        with self.assertRaisesRegex(
                ValueError,
                "unsupported permission bit 'y'"):
            templating.parse_chmod("u=y")

    def test_rejects_unknown_subjects(self):
        with self.assertRaisesRegex(
                ValueError,
                "unsupported subject 'x'"):
            templating.parse_chmod("x=x")

    def test_rejects_duplicate_terms(self):
        with self.assertRaisesRegex(
                ValueError,
                "bits for group specified multiple times"):
            templating.parse_chmod("ug=rwx,go=rw")
        with self.assertRaisesRegex(
                ValueError,
                "bits for user specified multiple times"):
            templating.parse_chmod("u=,u=rwx,go=rw")
        with self.assertRaisesRegex(
                ValueError,
                "bits for group specified multiple times"):
            templating.parse_chmod("a=rwx,go=rw")

    def test_rejects_missing_subject(self):
        # Note that chmod does, in such cases, strange things involving the
        # umask
        with self.assertRaisesRegex(
                ValueError,
                "missing subject in term"):
            templating.parse_chmod("=")
        with self.assertRaisesRegex(
                ValueError,
                "missing subject in term"):
            templating.parse_chmod("=rwx")


@ddt.ddt
class Testnormalize_cpu(unittest.TestCase):

    @ddt.data(
        ["100m", "100m"],
        ["1000m", "1"],
        ["1234m", "1234m"],
        ["2", "2"],
    )
    @ddt.unpack
    def test_normalize(self, value, expected):
        self.assertEqual(templating.normalize_cpu(value), expected)

    @ddt.data(
        "abcd",
        "100M",
    )
    def test_invalid(self, val):
        with self.assertRaisesRegex(ValueError, val):
            templating.normalize_cpu(val)


@ddt.ddt
class Testnormalize_memory(unittest.TestCase):

    @ddt.data(
        ["100k", "100k"],
        ["1000k", "1M"],
        ["1234k", "1234k"],
        ["2000000", "2M"],
        ["2000000k", "2G"],
        ["100Ki", "100Ki"],
        ["1000Ki", "1000Ki"],
        ["1024Ki", "1Mi"],
        # 1024-based is only assumed with a *i suffix.
        ["1024", "1024"],
        ["2097152Ki", "2Gi"],
    )
    @ddt.unpack
    def test_normalize(self, input, expected):
        self.assertEqual(templating.normalize_memory(input), expected)

    @ddt.data(
        [1000, ["", "k", "M", "G", "T", "P", "E"], 0, "9E"],
        # Starting at "Ki" because values without a suffix are mapped to
        # base 1000.
        [1024, [None, "Ki", "Mi", "Gi", "Ti", "Pi", "Ei"], 1, "7Ei"],
    )
    @ddt.unpack
    def test_overflow(self, base, suffixes, start, expected_quantity):
        # Test that quantities that represent values above 2^63 get capped
        # to the expected value. It caculates the 2^63 value corresponding
        # to the suffix.
        for exponent in range(start, 7):
            factor = base**exponent
            overflown = int((2**63) / factor)
            if base == 1000:
                # As the base is not a power of 2, it produces values below
                # the limit due to the dropped remainder of the division.
                # Adding one brings it over the limit.
                overflown += 1
            quantity = f"{overflown}{suffixes[exponent]}"
            self.assertEqual(
                templating.normalize_memory(quantity),
                expected_quantity)

    @ddt.data(
        [1000, ["", "k", "M", "G", "T", "P", "E"], 0],
        # Starting at "Ki" because values without a suffix are mapped to
        # base 1000.
        [1024, [None, "Ki", "Mi", "Gi", "Ti", "Pi", "Ei"], 1],
    )
    @ddt.unpack
    def test_limits(self, base, suffixes, start):
        # Test that quantities that represent a value of less than 2^63 do
        # not get capped. It caculates the 2^63 value corresponding
        # to the suffix and substracts one to stay in the limit.
        for exponent in range(start, 7):
            factor = base**exponent
            limit = int((2**63) / factor) - 1
            quantity = f"{limit}{suffixes[exponent]}"
            self.assertEqual(
                templating.normalize_memory(quantity),
                quantity)

    @ddt.data(
        "abcd",
        "100ki",
        "100X",
    )
    def test_invalid(self, val):
        with self.assertRaisesRegex(ValueError, val):
            templating.normalize_memory(val)


class Testcontainer_resources(unittest.TestCase):

    def test_no_top_level_resources(self):
        crd_spec = {}
        self.assertEqual(
            templating.container_resources(crd_spec, "foobar"),
            {})

    def test_no_resources(self):
        crd_spec = {'api': {}}
        self.assertEqual(
            templating.container_resources(crd_spec, "api.foobar"),
            {})

    def test_no_resources_content(self):
        crd_spec = {"api": {"resources": {}}}
        self.assertEqual(
            templating.container_resources(crd_spec, "api.foobar"),
            {})

    def test_no_matching_conainer(self):
        crd_spec = {
            "api": {
                "resources": {
                    "ssl-terminator": {
                        "requests": {
                            "cpu": "100m"
                        }
                    }
                }
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "api.foobar"),
            {})

    def test_matching_container(self):
        resources = {
            "requests": {
                "cpu": "100m"
            }
        }
        crd_spec = {
            "api": {
                "resources": {
                    "ssl-terminator": resources
                }
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "api.ssl-terminator"),
            resources)

    def test_matching_container_2(self):
        resources = {
            "requests": {
                "cpu": "100m"
            }
        }
        crd_spec = {
            "api": {
                "resources": {
                    "ssl-terminator": resources,
                    "ssl-terminator-external": {}
                }
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "api.ssl-terminator"),
            resources)

    def test_full_resources(self):
        resources = {
            "requests": {
                "cpu": "100m",
                "memory": "500M"
            },
            "limits": {
                "cpu": "500m",
                "memory": "2G"
            }
        }
        crd_spec = {
            "api": {
                "resources": {
                    "ssl-terminator": resources,
                }
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "api.ssl-terminator"),
            resources)

    def test_normalized(self):
        crd_spec = {
            "api": {
                "resources": {
                    "ssl-terminator": {
                        "requests": {
                            "cpu": "1000m",
                            "memory": "5000k"
                        },
                        "limits": {
                            "memory": "1234000"
                        }
                    },
                }
            }
        }
        expected = {
            "requests": {
                "cpu": "1",
                "memory": "5M"
            },
            "limits": {
                "memory": "1234k"
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "api.ssl-terminator"),
            expected)

    def test_multi_level(self):
        resources = {
            "requests": {
                "cpu": "100m"
            }
        }
        crd_spec = {
            "database": {
                "proxy": {
                    "resources": {
                        "haproxy": resources
                    }
                }
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "database.proxy.haproxy"),
            resources)

    def test_job_resources(self):
        resources = {
            "requests": {
                "cpu": "100m",
                "memory": "500M"
            },
            "limits": {
                "cpu": "500m",
                "memory": "2G"
            }
        }
        crd_spec = {
            "jobResources": {
                "db-sync-job": resources,
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "job.db-sync-job"),
            resources)

    def test_top_level_resources(self):
        # A top-level resources attribute is also supported for some
        # special or legacy use cases (horizon, temptest job).
        resources = {
            "requests": {
                "cpu": "100m",
                "memory": "500M"
            },
            "limits": {
                "cpu": "500m",
                "memory": "2G"
            }
        }
        crd_spec = {
            "resources": {
                "horizon": resources,
            }
        }
        self.assertEqual(
            templating.container_resources(crd_spec, "horizon"),
            resources)


class Testcontainer_resources_dict(unittest.TestCase):

    def test_no_containers(self):
        crd_spec = {}
        self.assertEqual(
            templating.container_resources_dict(
                crd_spec, "database", ["foobar"]),
            {})

    def test_one_container(self):
        resources = {
            "requests": {
                "cpu": "100m"
            }
        }
        crd_spec = {
            "database": {
                "resources": {
                    "foobar": resources
                }
            }
        }
        self.assertEqual(
            templating.container_resources_dict(
                crd_spec, "database", ["foobar"]),
            {"foobar": resources})

    def test_two_containers(self):
        resources = {
            "requests": {
                "cpu": "100m"
            }
        }
        crd_spec = {
            "database": {
                "resources": {
                    "foobar": resources,
                    "baz": resources,
                }
            }
        }
        self.assertEqual(
            templating.container_resources_dict(
                crd_spec, "database", ["foobar", "baz"]),
            {
                "foobar": resources,
                "baz": resources,
            })

    def test_missing_container(self):
        resources = {
            "requests": {
                "cpu": "100m"
            }
        }
        crd_spec = {
            "database": {
                "resources": {
                    "foobar": resources,
                }
            }
        }
        self.assertEqual(
            templating.container_resources_dict(
                crd_spec, "database", ["foobar", "baz"]),
            {"foobar": resources})
