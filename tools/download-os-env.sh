#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail
api_interface="${1:-internal}"

function retry {
  local max=3;
  local delay=5;
  for i in $(seq $max); do
    "$@" && return 0;
    if [[ $i -lt $max ]]; then
      echo "Command failed. Attempt $i/$max:";
      sleep $delay;
    fi
  done
  echo "The command has failed after $i attempts.";
  return 1;
}

shift
echo $(retry kubectl "$@" get configmap -l "state.yaook.cloud/parent-plural=keystonedeployments,state.yaook.cloud/component=${api_interface}_config" -o json) | jq -r '.items[0].data | to_entries[] | "export " + .key + "=\u0027" + .value + "\u0027"'
echo $(retry kubectl "$@" get secret -l "state.yaook.cloud/parent-plural=keystonedeployments,state.yaook.cloud/component=admin_credentials" -o json) | jq -r '.items[0].data | to_entries[] | "export " + .key + "=\"$(echo \u0027" + .value + "\u0027 | base64 -d)\""'
