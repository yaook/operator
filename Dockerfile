##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
ARG DESTDIR=/build
ARG ovs_version


FROM golang:1.23@sha256:574185e5c6b9d09873f455a7c205ea0514bfd99738c5dc7750196403a44ed4b7 AS cue-builder
WORKDIR /build
RUN GOPATH=/build go install cuelang.org/go/cmd/cue@v0.4.3

FROM debian:12.8-slim@sha256:1537a6a1cbc4b4fd401da800ee9480207e7dc1f23560c21259f681db56768f63 AS python
RUN \
	apt-get update && \
	apt-get install python3-minimal python3-pip wget --no-install-recommends -y && \
	apt-get clean -y

FROM python:3.11 AS cue-and-ovs-builder

ARG DESTDIR
ARG ovs_version

RUN \
	apt-get update && \
	apt-get install python3-dev gcc make --no-install-recommends -y
WORKDIR /build
COPY requirements-build.txt /build/
RUN pip3 install -r requirements-build.txt

RUN set -eux ; \
    git clone -b ${ovs_version} https://github.com/openvswitch/ovs.git /ovs --depth 1; \
    cd /ovs; \
    ./boot.sh; \
    ./configure \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc \
        # We need shared linking for the ovs._json python lib
        # With that lib OVS parses json in C instead of python
        --enable-shared; \
    gmake -j 8; \
    make install DESTDIR=${DESTDIR}; \
    cp -r /ovs/python ${DESTDIR}

# cuelang is needed for buildcue.py (make cue-templates)
COPY --from=cue-builder /build/bin/cue /bin/cue
COPY buildcue.py GNUmakefile /build/
RUN make cue-templates
# as this value is arbitrary set to 28 due to aarch64 / arm64 issues in qemu. These are no longer present using qemu 9.0.0 on arm64
# therefore we can globally use higher values.
# This leads to 28 being the maximumg number of pcie_ports present in a VM which leads to ~26 volumes to be attacheable
RUN sed -i '/num_pcie_ports/d' ./yaook/op/cue/pkg/yaook.cloud/nova_template/openstack_default_nova.cue

FROM python:3.11
ARG userid=2020
ARG DESTDIR

COPY --from=cue-and-ovs-builder ${DESTDIR}/ /
RUN \
	groupadd -g $userid yaook-operator && \
	useradd -u $userid -g $userid -d /tmp/yaook-operator -m yaook-operator
RUN \
	apt-get update && \
	apt-get install iputils-ping patch --no-install-recommends -y && \
	apt-get clean -y; \
    # A more recent version of the OVS python library is required
    # This is not intended to work nor tested to work with an Neutron OVS backend!
    cd /python/; \
    python3 setup.py build; \
    python3 setup.py install; \
    cd /; rm -rf /python;

COPY yaook /app/yaook
COPY setup.py MANIFEST.in /app/
ARG TARGETPLATFORM
RUN set -eux; cd /tmp ;\
    if [ "$TARGETPLATFORM" = "linux/amd64" ]; then \
        wget -q -O helm.tar.gz https://get.helm.sh/helm-v3.15.2-linux-amd64.tar.gz ;\
        echo '2694b91c3e501cff57caf650e639604a274645f61af2ea4d601677b746b44fe2 helm.tar.gz' > helm.tar.gz.sha256sum ;\
        sha256sum -c helm.tar.gz.sha256sum ;\
        tar -xf helm.tar.gz ;\
        mv linux-amd64/helm /bin/helm ;\
    elif [ "$TARGETPLATFORM" = "linux/arm64" ]; then \
        wget -q -O helm.tar.gz https://get.helm.sh/helm-v3.15.2-linux-arm64.tar.gz ;\
        echo 'adcf07b08484b52508e5cbc8b5f4b0b0db50342f7bc487ecd88b8948b680e6a7 helm.tar.gz' > helm.tar.gz.sha256sum ;\
        sha256sum -c helm.tar.gz.sha256sum ;\
        tar -xf helm.tar.gz ;\
        mv linux-arm64/helm /bin/helm ;\
    else \
        echo "no valid TARGETPLATFORM provided. Exiting..." ;\
        exit 1 ;\
    fi ;\
	rm -f helm.tar.gz helm.tar.gz.sha256sum
COPY --from=cue-builder /build/bin/cue /bin/cue
COPY --from=cue-and-ovs-builder /build/yaook/op /app/yaook/op
RUN cd /app && pip install . && rm -rf -- /root/.cache
COPY patches/apiclient.patch /apiclient.patch
RUN cd /usr/local/lib/python3.11/site-packages/kubernetes_asyncio/; patch -p 1 < /apiclient.patch

ENTRYPOINT ["python3", "-m", "yaook.op"]
