##
## Copyright (c) 2024 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "octavia-health-manager"
  labels:
    app: "octavia-health-manager"
spec:
  replicas:  {{ crd_spec.health_manager.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['health_manager_config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.health_manager.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "octavia-health-manager"
          image: {{ versioned_dependencies['octavia_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["octavia-health-manager"]
          volumeMounts:
            - name: octavia-health-manager-config-volume
              mountPath: /etc/octavia/octavia.conf
              subPath: octavia.conf
            - name: octavia-certificates
              mountPath: /etc/octavia/octavia-certificates
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          lifecycle:
            preStop:
              exec:
                command:
                - /bin/sleep
                - "5"
          resources: {{ crd_spec | resources('health_manager.octavia-health-manager') }}
      volumes:
        - name: octavia-health-manager-config-volume
          projected:
            sources:
            - secret:
                name: {{ dependencies['health_manager_config'].resource_name() }}
                items:
                  - key: octavia_health_manager.conf
                    path: octavia.conf
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: octavia-certificates
          secret:
            secretName: octavia-certificates
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
