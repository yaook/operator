// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neutron_ovn_bgp

import (
	"yaook.cloud/neutron_ovn_bgp_template"
)

neutron_ovn_bgp_conf_spec: neutron_ovn_bgp_template.neutron_ovn_bgp_template_conf_spec
neutron_ovn_bgp_conf_spec: {
	DEFAULT: {
		"ovsdb_connection":   *"unix:/run/openvswitch/db.sock" | string
		"ovn_sb_private_key": *"/etc/ssl/private/tls.key" | string
		"ovn_sb_certificate": *"/etc/ssl/private/tls.crt" | string
		"ovn_sb_ca_cert":     *"/etc/ssl/private/ca.crt" | string
		"use_stderr":         *true | bool
		"use_json":           *true | bool
	}
}
