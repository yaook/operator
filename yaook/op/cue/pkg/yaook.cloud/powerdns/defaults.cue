// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package powerdns

powerdns_conf_spec: {
	#subnet_cidr: string

	"setuid":                     *"pdns" | string
	"setgid":                     *"pdns" | string
	"allow-dnsupdate-from":       *"\( #subnet_cidr )" | string
	"allow-notify-from":          *"\( #subnet_cidr )" | string
	"allow-axfr-ips":             *"\( #subnet_cidr )" | string
	"api":                        "yes"
	"api-key":                    string
	"disable-axfr":               *"no" | bool
	"dnsupdate":                  "yes"
	"local-port":                 "5300"
	"log-dns-details":            *"yes" | bool
	"log-dns-queries":            *"yes" | bool
	"loglevel":                   *7 | int
	"primary":                    *"no" | string
	"secondary":                  *"yes" | string
	"trusted-notification-proxy": *"\( #subnet_cidr )" | string
	"xfr-cycle-interval":         *60 | int
	"webserver":                  "yes"
	"webserver-address":          "0.0.0.0"
	"webserver-allow-from":       *"127.0.0.1,::1,\( #subnet_cidr )" | string
	"launch":                     "gmysql"

	#connection_host:     string
	#connection_port:     *3306 | int
	#connection_username: string
	#connection_password: string
	#connection_database: string

	"gmysql-host":     *"\( #connection_host )" | string
	"gmysql-port":     *"\( #connection_port )" | string
	"gmysql-dbname":   *"\( #connection_database )" | string
	"gmysql-user":     *"\( #connection_username )" | string
	"gmysql-password": *"\( #connection_password )" | string
	"gmysql-dnssec":   "yes"
}
