#
# Copyright (c) 2024 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import time
import ddt
import unittest
import unittest.mock
import uuid

from unittest.mock import MagicMock, Mock

import yaook.op.neutron_ovn.eviction as eviction

NAMESPACE = f"namespace-{uuid.uuid4()}"
NAME = f"name-{uuid.uuid4()}"


@ddt.ddt
class TestEvictor(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.intf = unittest.mock.Mock(
            "yaook.statemachine.interfaces.ResourceInterfaceWithStatus",
        )
        self.connection_info = {
            "auth": unittest.mock.sentinel.auth,
            "interface": unittest.mock.sentinel.interface,
        }
        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = (unittest.mock.sentinel.nb_api,
                                          unittest.mock.sentinel.sb_api)
        with contextlib.ExitStack() as stack:
            stack.enter_context(
                unittest.mock.patch.object(
                    eviction.Evictor,
                    "_connect",
                    new=self.connect_mock,
                )
            )
            self.e = eviction.Evictor(
                nb_db=unittest.mock.sentinel.nb_db,
                sb_db=unittest.mock.sentinel.sb_db,
                interface=self.intf,
                namespace=NAMESPACE,
                node_name=NAME,
                connection_info=self.connection_info,
                reason="some-reason",
            )
        self.e.all_chassis = \
            ['gtw01', 'gtw02', 'gtw03', 'gtw04', 'gtw05', 'gtw06']
        self.e.active_chassis = \
            ['gtw01', 'gtw02', 'gtw03', 'gtw04', 'gtw05', 'gtw06']

        def mock_lrp(lrp_name, chassis_list):
            gateway_chassis = []
            for chassis_name, priority in chassis_list:
                gateway_chassis.append(Mock(
                    chassis_name=chassis_name, priority=priority))

            # as we need to mock a name attribut we can't initialize
            # the Mock object in the constructor
            lrp = Mock()
            lrp.configure_mock(
                name=lrp_name,
                gateway_chassis=gateway_chassis,
            )
            return lrp

        with unittest.mock.patch.object(self.e, 'nb_api') as nb_api_mock:
            tx = Mock()
            nb_api_mock.db_find_rows.return_value = tx
            tx.execute.return_value = [
                mock_lrp('lrp01', [('gtw01', 1), ('gtw02', 2), ('gtw03', 3),
                                   ('gtw04', 4), ('gtw05', 5)]),
                mock_lrp('lrp02', [('gtw01', 1), ('gtw02', 2), ('gtw03', 3),
                                   ('gtw04', 4), ('gtw05', 5)]),
                mock_lrp('lrp03', [('gtw01', 5), ('gtw02', 4)]),
                mock_lrp('lrp04', [('gtw02', 4)]),
                mock_lrp('lrp05', [('gtw01', 5)]),
            ]
            self.e._fill_lrp_distribution()

        self.__patches = []

    def tearDown(self):
        for p in self.__patches:
            p.stop()

    def test_setup(self):
        self.assertCountEqual(self.e.current_dist['gtw01'][1], ['lrp01', 'lrp02'])
        self.assertCountEqual(self.e.current_dist['gtw01'][5], ['lrp03', 'lrp05'])
        self.assertCountEqual(self.e.current_dist['gtw02'][2], ['lrp01', 'lrp02'])
        self.assertCountEqual(self.e.current_dist['gtw02'][4], ['lrp03', 'lrp04'])
        self.assertCountEqual(self.e.current_dist['gtw03'][3], ['lrp01', 'lrp02'])
        self.assertCountEqual(self.e.current_dist['gtw04'][4], ['lrp01', 'lrp02'])
        self.assertCountEqual(self.e.current_dist['gtw05'][5], ['lrp01', 'lrp02'])

        self.assertEqual(self.e.priorities, [1, 2, 3, 4, 5])

    def test_update_priorities_lrp_added_to_new_chassis(self):
        old_chassis = 'gtw01'
        new_chassis = 'gtw06'
        lrp = 'lrp01'
        priority = 1
        self.assertTrue(self.e.update_priorities(
            old_chassis, new_chassis, lrp, priority))
        self.assertEqual(self.e.updates, [('lrp01', 1, 'gtw01', 'gtw06')])

    def test_update_priorities_lrp_already_exist_on_chassis(self):
        old_chassis = 'gtw01'
        new_chassis = 'gtw02'
        lrp = 'lrp01'
        priority = 1
        self.assertFalse(self.e.update_priorities(
            old_chassis, new_chassis, lrp, priority))
        self.assertEqual(self.e.updates, [])

    def test__search_for_chassis(self):
        chassis = self.e.search_for_chassis(5)
        sorted_chassis = []
        for c in chassis:
            sorted_chassis.append(c)

        self.assertEqual(sorted_chassis,
                         ['gtw02',
                          'gtw03',
                          'gtw04',
                          'gtw06',
                          'gtw01',
                          'gtw05'])

    def test_evict(self):
        self.e._node_name = 'gtw01'
        self.e.evict()

        self.assertEqual(self.e.updates, [('lrp01', 1, 'gtw01', 'gtw06'),
                                          ('lrp02', 1, 'gtw01', 'gtw06'),
                                          ('lrp03', 5, 'gtw01', 'gtw03'),
                                          ('lrp05', 5, 'gtw01', 'gtw02')])

    def test__os_iterate(self):
        self.e._node_name = 'gtw01'
        self.e._fill_lrp_distribution = unittest.mock.Mock([])
        self.e._fill_lrp_distribution.return_value = None
        mock_transaction = MagicMock()
        self.e.nb_api = MagicMock()
        self.e.nb_api.transaction.return_value = MagicMock()
        mock_transaction.__enter__.return_value = MagicMock()
        mock_transaction.__exit__.return_value = MagicMock()

        self.e.nb_api.lrp_del_gateway_chassis = MagicMock()
        self.e.nb_api.lrp_set_gateway_chassis = MagicMock()
        time.sleep = unittest.mock.Mock([])
        time.sleep.return_value = None

        self.e._get_active_chassis = unittest.mock.Mock([])
        self.e._get_active_chassis.return_value = (['gtw01', 'gtw02', 'gtw03',
                                                   'gtw04', 'gtw05', 'gtw06'],
                                                   ['gtw01', 'gtw02', 'gtw03',
                                                   'gtw04', 'gtw05', 'gtw06'])
        eviction_status = self.e._os_iterate()

        self.assertEqual(eviction_status.migrated_lrps, 4)
        self.assertEqual(eviction_status.unmigratable_lrps, 0)
        self.assertEqual(eviction_status.unhandleable_gateway, False)
