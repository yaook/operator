##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: apps.yaook.cloud/v1
kind: ConfiguredDaemonSet
metadata:
  generateName: ceilometer-compute-
spec:
  volumeTemplates:
    - volumeName: ceilometer-config-volume
      nodeMap: {{ dependencies['compute_configs'].instances() | to_secret_volumes( {} ) }}
  updateStrategy:
    type: rollingUpdate
    rollingUpdate:
      maxUnavailable: 1
  template:
    metadata:
      labels: {{ labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                state.yaook.cloud/component: compute
            topologyKey: kubernetes.io/hostname
      containers:
        - name: "ceilometer-compute-agent"
          image:  {{ versioned_dependencies['ceilometer_docker_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["ceilometer-polling", "--polling-namespaces", "compute"]
          volumeMounts:
            - name: ceilometer-config-volume
              mountPath: /etc/ceilometer/ceilometer.conf
              subPath: ceilometer.conf
            - name: polling
              mountPath: /etc/ceilometer/polling.yaml
              subPath: polling.yaml
            - name: varrunlibvirt
              mountPath: /var/run/libvirt
            - name: ca-certs
              mountPath: /etc/ssl/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          livenessProbe:
            # This checks if ceilometer-polling has a open connection to the libvirt unix socket
            # Can be tested by killing the libvirt process mutliple times in a row
            exec:
              command:
              - sh
              - -c
              - ss -xapn | grep -F "$(ss -xap | grep ceilometer-polling | awk '{print $6}')" | grep -q /run/libvirt
            failureThreshold: 3
            initialDelaySeconds: 30
            timeoutSeconds: 5
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - ss -xapn | grep -F "$(ss -xap | grep ceilometer-polling | awk '{print $6}')" | grep -q /run/libvirt
            failureThreshold: 1
            timeoutSeconds: 5
          resources: {{ crd_spec | resources('ceilometerCompute.ceilometer-compute-agent') }}
      volumes:
        - name: varrunlibvirt
          hostPath:
            path: /var/run/libvirt
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: polling
          configMap:
            name: {{ dependencies['polling_config'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
