#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.version_utils`: Utilities for different releases
==========================================================================

.. currentmodule:: yaook.statemachine

.. automethod:: get_installed_release

.. automethod:: get_target_release

.. automethod:: is_upgrading

"""

import typing
import yaook.statemachine.context as context


KEY_SPEC_TARGET_RELEASE = "targetRelease"
"""
The key inside the resource spec that defines what the next release should be
"""

KEY_STATUS_INSTALLED_RELEASE = "installedRelease"
"""
The key inside the resource status that describes which release has been
successfully rolled out previously.
"""

KEY_STATUS_NEXT_RELEASE = "nextRelease"
"""
The key inside the resource status that describes which release will be rolled
out next.
"""


def get_installed_release(ctx: context.Context) -> typing.Optional[str]:
    """
    Represents the latest release that has been rolled out successfully.
    Returns `None` if no release has been rolled out yet
    """
    return ctx.parent.get("status", {}).get(KEY_STATUS_INSTALLED_RELEASE)


def get_target_release(ctx: context.Context) -> str:
    """
    Represents the target release as specified in the spec of the resource.
    Raises a KeyError if no target release is specified.
    """
    return ctx.parent_spec[KEY_SPEC_TARGET_RELEASE]


def get_next_release(ctx: context.Context) -> typing.Optional[str]:
    """
    Represents the next release as specified in the status of the resource.
    Returns `None` if no release has been defined as a next release.
    """
    return ctx.parent.get("status", {}).get(KEY_STATUS_NEXT_RELEASE)


def is_upgrading(ctx: context.Context) -> bool:
    """
    Returns `True` if we are currently upgrading the resource. This means
    that a `installed_release` is set and is not equal to the
    `target_release`.

    Note that this will return `True` on any difference, so also if the
    user would request a downgrade.
    """
    installed_release = get_installed_release(ctx)
    if installed_release:
        return get_target_release(ctx) != installed_release
    return False
