##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = {} %}
{% set _ = pod_labels.update(labels) %}

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['neutron_ovn_metadata_service'].resource_name() }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
    # no pdb needed -> metadata agent can be recreated during runtime
    test.yaook.cloud/skip-pdb: "true"
spec:
  serviceName: {{ dependencies['neutron_ovn_metadata_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronOVNAgent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ labels['state.yaook.cloud/parent-name'] }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                state.yaook.cloud/component: ovn_controller
            topologyKey: kubernetes.io/hostname
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      containers:
        - name: "neutron-ovn-metadata-agent"
{% if 'imageRef' in crd_spec and 'neutron-ovn-agent' in crd_spec.imageRef %}
          image:  {{ crd_spec.imageRef['neutron-ovn-agent'] }}
{% else %}
          image:  {{ versioned_dependencies['neutron_ovn_agent_docker_image'] }}
{% endif %}
          imagePullPolicy: IfNotPresent
          args: ["/neutron-ovn-metadata-agent-runner.sh"]
          livenessProbe:
            exec:
              command:
              - bash
              - -c
              - 'curl -H "X-Ovn-Network-Id: I-am-the-liveness-probe" --unix-socket /run/neutron/metadata_proxy_socket http://localhost'
            initialDelaySeconds: 60
            timeoutSeconds: 30
          startupProbe:
            exec:
              command:
              - bash
              - -c
              - test -S /run/neutron/metadata_proxy_socket
            failureThreshold: 30
            periodSeconds: 10
            timeoutSeconds: 5
          resources: {{ crd_spec | resources('neutron-ovn-metadata-agent') }}
          securityContext:
            privileged: true
          volumeMounts:
            - name: run-openvswitch
              mountPath: /run/openvswitch
            - name: run-netns
              mountPropagation: Bidirectional
              mountPath: /run/netns
            - mountPath: /run/xtables.lock
              name: iptables-lockfile
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
            - name: tls-secret
              mountPath: /etc/ssl/private
            - name: neutron-config-vol
              mountPath: /etc/neutron
      terminationGracePeriodSeconds: 240
      volumes:
        - name: run-openvswitch
          hostPath:
            path: /run/openvswitch
        - name: run-netns
          hostPath:
            path: /run/netns
          name: run-netns
        - name: iptables-lockfile
          hostPath:
            path: /run/xtables.lock
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
        - name: neutron-config-vol
          secret:
            secretName: {{ dependencies['ovn_metadata_config'].resource_name() }}
            items:
            - key: neutron_ovn_metadata_agent.conf
              path: neutron.conf
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
