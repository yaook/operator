#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import pprint

import ddt

import yaook.op.common as op_common
import yaook.op.scheduling_keys as scheduling_keys
import yaook.op.octavia as octavia
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from tests import testutils


NAMESPACE = "test-namespace"
NAME = "octavia"
CONFIG_FILE_NAME = "octavia.conf"
CONFIG_PATH = "/etc/octavia"
POLICY_FILE_NAME = "policy.yaml"
POLICY_RULE_KEY = "volume:create_snapshot"
POLICY_RULE_VALUE = "is_admin:True"
OCTAVIA_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}
CONFIG_SECRET_NAME = "config-secret"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"


def _get_octavia_deployment_yaml(keystone_name):
    return {
        "metadata": {
            "name": NAME,
            "namespace": NAMESPACE,
        },
        "spec": {
            "keystoneRef": {
                "name": keystone_name,
                "kind": "KeystoneDeployment",
            },
            "region": {
                "name": "regionname",
                "parent": "parentregionname",
            },
            "issuerRef": {
                "name": "issuername"
            },
            "api": {
                "ingress": {
                    "fqdn": "octavia-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 2,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "api.octavia-api",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.service-reload",
                    "api.service-reload-external",
                ),
            },
            "worker": {
                "replicas": 1,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "worker.octavia-worker",
                ),
            },
            "housekeeping": {
                "replicas": 1,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "housekeeping.octavia-housekeeping",
                ),
            },
            "health_manager": {
                "replicas": 1,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "health_manager.octavia-health_manager",
                ),
            },
            "database": {
                "timeoutClient": 300,
                "replicas": 2,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "storageSize": "8Gi",
                "storageClassName": "foo-class",
                "proxy": {
                    "replicas": 1,
                    "resources": testutils.generate_db_proxy_resources(),
                },
                "backup": {
                    "schedule": "0 * * * *"
                },
                "resources": testutils.generate_db_resources(),
            },
            "messageQueue": {
                "replicas": 1,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "storageSize": "2Gi",
                "storageClassName": "bar-class",
                "resources": testutils.generate_amqp_resources(),
            },
            "memcached": {
                "replicas": 2,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "memory": "512",
                "connections": "2000",
                "resources": testutils.generate_memcached_resources(),
            },
            "octaviaConfig": {
                "controller_worker": {
                    "amp_image_owner_id": "98a3c44385de48ddba045d8c19340451",
                    "amp_ssh_key_name": "octavia_test_rsa",
                    "amp_secgroup_list": [
                        "f4f4c4f7-657f-40e0-8107-54b86e788795",
                    ],
                    "amp_boot_network_list": [
                        "b9fe8c69-4510-4e38-a5f6-3b73117be928",
                    ],
                    "amp_flavor_id": "57b90104-2af4-4208-b717-b999fb9f3d54",
                },
                "health_manager": {
                    "controller_ip_port_list": [
                        "192.168.144.101:5555",
                    ],
                },
            },
            "targetRelease": "2024.1",
            "jobResources": testutils.generate_resources_dict(
                "job.octavia-db-sync-job",
            ),
        },
    }


@ddt.ddt
class TestOctaviaDeployments(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin,
        testutils.MemcachedTestMixin):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            octavia.Octavia,
            _get_octavia_deployment_yaml(self._keystone_name),
        )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_keystone_endpoint_is_created(self):
        deployment_yaml = _get_octavia_deployment_yaml(self._keystone_name)
        self._configure_cr(octavia.Octavia, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 1)

    async def test_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_octavia_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(octavia.Octavia, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(len(endpoints), 0)

    async def test_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_created_config_matches_keystone_user(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        user_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: user["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        user_credentials = sm.api_utils.decode_secret_data(
            user_credentials_secret.data
        )
        cfg = testutils._parse_config(config.data["octavia.conf"], decode=True)

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         user_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         user_credentials["OS_PASSWORD"])

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                         "certificate")

    async def test_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "octavia-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def test_creates_db_sync_job_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 2)
        _, db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 2)
        _, db_sync, = all_jobs

        self.assertEqual(db_sync.metadata.labels[context.LABEL_COMPONENT],
                         "db_sync")

    async def test_creates_api_deployment_with_replicas_when_all_jobs_succeed(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        self.assertEqual(deployment.spec.replicas, 2)

    async def test_jobs_use_config_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        jobs = interfaces.job_interface(self.api_client)
        for job in await jobs.list_(NAMESPACE):
            if job._metadata.generate_name == "octavia-policy-validator-":
                continue
            self.assertEqual(
                job.spec.template.spec.volumes[0].secret.secret_name,
                config_secret.metadata.name,
                str(job.metadata),
            )

    async def test_creates_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(NAMESPACE)
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["timeoutClient"], 300)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "foo-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user_password",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "frontend_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_api_user",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db",
            },
        )
        db_name = db["spec"]["database"]
        octavia_conf = config.data["octavia.conf"]
        cfg = testutils._parse_config(octavia_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "octaviadeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_deployment",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "octavia-api",
        )

        octavia_conf = config.data["octavia.conf"]
        cfg = testutils._parse_config(octavia_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_creates_message_queue_and_user(self):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        api_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(mq["spec"]["replicas"], 1)
        self.assertEqual(mq["spec"]["storageSize"], "2Gi")
        self.assertEqual(mq["spec"]["storageClassName"], "bar-class")

    async def test_amqp_server_frontendIssuer_name(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        amqpserver = interfaces.amqpserver_interface(self.api_client)

        amqp, = await amqpserver.list_(
            NAMESPACE,
        )

        self.assertEqual(
            "issuername",
            amqp["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_transport_url(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "mq_api_user_password",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "frontend_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        octavia_conf = config.data["octavia.conf"]
        cfg = testutils._parse_config(octavia_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://api:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )
        self.assertTrue(
            cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/pki/tls/certs/ca-bundle.crt",
        )

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "octavia-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "octavia-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://octavia-ingress:8080",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "octavia")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "octavia-ingress",
        )

    async def test_disable_ingress_creation(self):
        deployment_yaml = _get_octavia_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = False
        self._configure_cr(octavia.Octavia, deployment_yaml)
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingress_int = interfaces.ingress_interface(self.api_client)
        ingresses = await ingress_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_ingress"},
        )
        self.assertEqual(len(ingresses), 0)

    async def test_enable_ingress_creation(self):
        deployment_yaml = _get_octavia_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = True
        self._configure_cr(octavia.Octavia, deployment_yaml)
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingress_int = interfaces.ingress_interface(self.api_client)
        ingresses = await ingress_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_ingress"},
        )
        self.assertEqual(len(ingresses), 1)

    async def test_ingress_force_ssl_annotation(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "octavia")

        self.assertEqual(
            ingress
            .metadata
            .annotations["nginx.ingress.kubernetes.io/force-ssl-redirect"],
            "true",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "octavia")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "octavia-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_service, _ = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_PARENT_PLURAL: "octaviadeployments"}
        )
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        pod_labels = deployment.spec.template.metadata.labels
        api_service_labels = api_service.spec.selector
        self.assertTrue(
            sm.matches_labels(pod_labels, api_service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(api_service_labels)}\n",
        )

    async def test_applies_scheduling_key_to_api_deployment(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )

        self.assertEqual(
            deployment.spec.template.spec.affinity.node_affinity.
            required_during_scheduling_ignored_during_execution.to_dict(),
            {
                "node_selector_terms": [
                    {
                        "match_expressions": [
                            {
                                "key": scheduling_keys.SchedulingKey.
                                OCTAVIA_API.value,
                                "operator": "Exists",
                                "values": None,
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": scheduling_keys.SchedulingKey.
                                OCTAVIA_ANY_SERVICE.value,
                                "operator": "Exists",
                                "values": None,
                            },
                        ],
                        "match_fields": None,
                    },
                    {
                        "match_expressions": [
                            {
                                "key": scheduling_keys.SchedulingKey.
                                ANY_API.value,
                                "operator": "Exists",
                                "values": None,
                            },
                        ],
                        "match_fields": None,
                    },
                ],
            },
        )

        self.assertCountEqual(
            deployment.spec.template.spec.to_dict()["tolerations"],
            [
                {
                    "key": scheduling_keys.SchedulingKey.
                    OCTAVIA_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": scheduling_keys.SchedulingKey.
                    OCTAVIA_ANY_SERVICE.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
                {
                    "key": scheduling_keys.SchedulingKey.ANY_API.value,
                    "operator": "Exists",
                    "effect": None,
                    "toleration_seconds": None,
                    "value": None,
                },
            ],
        )

    async def test_applies_scheduling_key_to_jobs(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        assert jobs, "No jobs were found"
        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        scheduling_keys.SchedulingKey.
                                        OPERATOR_OCTAVIA.value,
                                    "operator": "Exists",
                                    "values": None,
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        scheduling_keys.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": None,
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": scheduling_keys.SchedulingKey.
                        OPERATOR_OCTAVIA.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": scheduling_keys.SchedulingKey.
                        OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    @ddt.data({}, OCTAVIA_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        octavia_deployment_yaml = _get_octavia_deployment_yaml(
            self._keystone_name,
        )
        octavia_deployment_yaml["spec"].update(policy)
        self._configure_cr(octavia.Octavia, octavia_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        octavia_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILE_NAME],
            decode=True
        )

        observed_policy_file = \
            octavia_conf_content.get("oslo_policy", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    @ddt.data({}, OCTAVIA_POLICY)
    async def test_creates_api_deployment_with_cert_volume(self, policy):
        octavia_deployment_yaml = _get_octavia_deployment_yaml(
            self._keystone_name,
        )
        octavia_deployment_yaml["spec"].update(policy)
        self._configure_cr(octavia.Octavia, octavia_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "octaviadeployments"
            },
        )

        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 6)

        expected_volume_name = "ca-certs"

        self.assertEqual(
            len(api_deployment.spec.template.spec.containers[0].volume_mounts),
            3
        )
        cert_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[1]
        self.assertEqual(cert_volume_mount.name, expected_volume_name)
        self.assertEqual(cert_volume_mount.mount_path, "/etc/pki/tls/certs")

        cert_volume = api_deployment.spec.template.spec.volumes[1]
        self.assertEqual(cert_volume.name, expected_volume_name)
        self.assertEqual(cert_volume.config_map.name, ca_certs.metadata.name)

    @ddt.data({}, OCTAVIA_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        octavia_deployment_yaml = _get_octavia_deployment_yaml(
            self._keystone_name,
        )
        octavia_deployment_yaml["spec"].update(policy)
        self._configure_cr(octavia.Octavia, octavia_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 6)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            os.path.join(CONFIG_PATH, CONFIG_FILE_NAME),
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    async def test_creates_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.octavia-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.octavia-db-sync-job")
        )

    @ddt.data({}, OCTAVIA_POLICY)
    async def test_creates_policy_configmap(self, policy):
        octavia_deployment_yaml = _get_octavia_deployment_yaml(
            self._keystone_name,
        )
        octavia_deployment_yaml["spec"].update(policy)
        self._configure_cr(octavia.Octavia, octavia_deployment_yaml)

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        octavia_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(octavia_policy.kind, "ConfigMap")
        self.assertTrue(
            octavia_policy.metadata.name.startswith("octavia-policy"))

        self.assertEqual(octavia_policy.data, {})
        self.assertEqual(
            octavia_policy.metadata.annotations[
                'state.yaook.cloud/policies'
            ],
            str(policy.get("policy", {}))
        )

    async def test_created_config_correct_region(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configs = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "backup_configs"},
        )

        for config in configs:
            cfg = testutils._parse_config(config.data["octavia.conf"],
                                          decode=True)

            self.assertEqual(cfg.get("keystone_authtoken", "region_name"),
                             "regionname")
            self.assertEqual(cfg.get("keystone_authtoken", "os_region_name"),
                             "regionname")


@ddt.ddt
class TestOctaviaDeploymentsWithInternalIngress(
        testutils.ReleaseAwareCustomResourceTestCase,
        testutils.DatabaseTestMixin,
        testutils.MessageQueueTestMixin):

    def _get_octavia_deployment_yaml(self, keystone_name):
        return {
            "metadata": {
                "name": NAME,
                "namespace": NAMESPACE,
            },
            "spec": {
                "keystoneRef": {
                    "name": keystone_name,
                    "kind": "KeystoneDeployment",
                },
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "issuerRef": {
                    "name": "issuername"
                },
                "api": {
                    "internal": {
                        "ingress": {
                            "fqdn": "internal-octavia-ingress",
                            "port": 8081,
                            "ingressClassName": "nginx",
                        },
                    },
                    "ingress": {
                        "fqdn": "octavia-ingress",
                        "port": 8080,
                        "ingressClassName": "nginx",
                    },
                    "replicas": 2,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "resources": testutils.generate_resources_dict(
                        "api.octavia-api",
                        "api.ssl-terminator",
                        "api.ssl-terminator-external",
                        "api.ssl-terminator-internal",
                        "api.service-reload",
                        "api.service-reload-external",
                        "api.service-reload-internal",
                    ),
                },
                "worker": {
                    "replicas": 1,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "resources": testutils.generate_resources_dict(
                        "worker.octavia-worker",
                    ),
                },
                "housekeeping": {
                    "replicas": 1,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "resources": testutils.generate_resources_dict(
                        "housekeeping.octavia-housekeeping",
                    ),
                },
                "health_manager": {
                    "replicas": 1,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "resources": testutils.generate_resources_dict(
                        "health_manager.octavia-health_manager",
                    ),
                },
                "database": {
                    "timeoutClient": 300,
                    "replicas": 2,
                    "storageSize": "8Gi",
                    "storageClassName": "foo-class",
                    "proxy": {
                        "replicas": 1,
                        "resources": testutils.generate_db_proxy_resources(),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(),
                },
                "memcached": {
                    "replicas": 2,
                    "memory": "512",
                    "connections": "2000",
                    "resources": testutils.generate_memcached_resources(),
                },
                "messageQueue": {
                    "replicas": 1,
                    "storageSize": "2Gi",
                    "storageClassName": "bar-class",
                    "resources": testutils.generate_amqp_resources(),
                },
                "octaviaConfig": {
                    "controller_worker": {
                        "amp_image_owner_id": "98a3c44385de48ddba045d8c19340451",
                        "amp_ssh_key_name": "octavia_test_rsa",
                        "amp_secgroup_list": [
                            "f4f4c4f7-657f-40e0-8107-54b86e788795",
                        ],
                        "amp_boot_network_list": [
                            "b9fe8c69-4510-4e38-a5f6-3b73117be928",
                        ],
                        "amp_flavor_id": "57b90104-2af4-4208-b717-b999fb9f3d54",
                    },
                    "health_manager": {
                        "controller_ip_port_list": [
                            "192.168.144.101:5555",
                        ],
                    },
                },
                "targetRelease": "2024.1",
            },
        }

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            octavia.Octavia,
            self._get_octavia_deployment_yaml(self._keystone_name),
        )
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )

    async def test_certificate_contains_internal_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "certificate",
            },
        )

        self.assertIn(
            "internal-octavia-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_creates_endpoint_with_internal_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "octavia-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            "https://octavia-api.test-namespace.svc:9876",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "octavia-internal")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "internal-octavia-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "octavia")
        internal_ingress = await ingresses.read(NAMESPACE, "octavia-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "octavia-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )
