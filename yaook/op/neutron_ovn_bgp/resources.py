#
# Copyright (c) 2022 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import jsonpatch

import yaook.statemachine as sm
import yaook.statemachine.context as context

import kubernetes_asyncio.client as kclient

OVN_BGP_LOCK_NAME = context.ANNOTATION_L2_MIGRATION_LOCK + "{lock}"


class OVNBGPResource(sm.ExternalResource):
    async def update(
            self,
            ctx: context.Context,
            dependencies: sm.DependencyMap) -> None:
        pass

    async def delete(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap,
            ) -> None:
        await self.update(ctx, dependencies)
        v1 = kclient.CoreV1Api(ctx.api_client)
        annotation = OVN_BGP_LOCK_NAME.format(lock=ctx.parent_spec['lockName'])
        await v1.patch_node(
            ctx.parent_spec["hostname"],
            [{
                "op": "remove",
                "path": jsonpatch.JsonPointer.from_parts([
                    "metadata", "annotations", annotation
                ]).path,
                "value": "",
            }],
        )


class OVNBGPLock(sm.L2Lock):
    def _get_annotation_name(self, ctx: sm.Context) -> str:
        return OVN_BGP_LOCK_NAME.format(lock=ctx.parent_spec['lockName'])

    def _get_patch_node_name(self, ctx: sm.Context) -> str:
        return ctx.parent_spec["hostname"]
