##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ "%s-conductor" | format(labels['state.yaook.cloud/parent-name']) }}
spec:
  replicas: {{ crd_spec.conductor.replicas }}
  serviceName: {{ dependencies['conductor_service'].resource_name() }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.conductor.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      hostAliases:
{% if crd_spec.ingressAddress | default(False) %}
      - ip: {{ crd_spec.ingressAddress }}
{% else %}
      - ip: {{ ingressAddress }}
{% endif %}
        hostnames:
        - {{ crd_spec.imageServer.ingress.fqdn }}
      containers:
      - name: conductor
        image: {{ versioned_dependencies["ironic_image"] }}
        command:
        - ironic-conductor
        volumeMounts:
        - name: ironic
          mountPath: /etc/ironic/
        - name: http-data
          mountPath: /httpboot
        - name: ca-certs
          mountPath: /etc/pki/tls/certs
        env:
        - name: REQUESTS_CA_BUNDLE
          value: /etc/pki/tls/certs/ca-bundle.crt
        ports:
        - containerPort: 8089
          protocol: TCP
          name: json-rpc
        resources: {{ crd_spec | resources('conductor.conductor') }}
      volumes:
      - name: ironic
        projected:
          sources:
          - configMap:
              name: {{ dependencies['bootscripts'].resource_name() }}
              items:
              - key: boot.ipxe
                path: boot.ipxe
          - secret:
              name: {{ dependencies['config'].resource_name() }}
              items:
              - key: ironic.conf
                path: ironic.conf
      - name: http-data
        persistentVolumeClaim:
          claimName: {{ dependencies['data_volume'].resource_name() }}
      - name: ca-certs
        configMap:
          name: {{ dependencies['api_ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
