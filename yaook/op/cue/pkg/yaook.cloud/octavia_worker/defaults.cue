// Copyright (c) 2024 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package octavia_worker

import (
	"strings"
	"yaook.cloud/octavia_worker_template"
)

octavia_worker_conf_spec: octavia_worker_template.octavia_worker_template_conf_spec
octavia_worker_conf_spec: {
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-octavia"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		use_stderr:    *true | bool
		use_json:      *true | bool
		debug:         *true | bool
	}
	database: {
		#connection_host:        *"mysql-octavia-mariadb" | string
		#connection_port:        *3306 | int
		#connection_username:    *"octavia" | string
		#connection_password:    string
		#connection_database:    *"octavia" | string
		#connection_ssl_ca_path: *"/etc/pki/tls/certs/ca-bundle.crt" | string
		connection:              *"mysql+pymysql://\( #connection_username ):\( #connection_password )@\( #connection_host ):\( #connection_port )/\( #connection_database )?charset=utf8&ssl_ca=\( #connection_ssl_ca_path )" | string
		// connection_recycle_time should always be ~10% smaller
		// than the haproxy timeoutClient.
		connection_recycle_time: *280 | int
	}
	keystone_authtoken: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
		service_token_roles: *["admin"] | [string, ...]
	}
	service_auth: {
		auth_url:            *"https://keystone:5000/v3" | string
		cafile:              "/etc/pki/tls/certs/ca-bundle.crt"
		auth_type:           "password"
		project_domain_name: string
		user_domain_name:    string
		project_name:        string
		username:            string
		password:            string
	}
	cache: {
		backend:          "oslo_cache.memcache_pool"
		enabled:          false
		backend_argument: *[
					"memcached_expire_time:660",
		] | [...string]
	}
	oslo_policy: {
		policy_file:          *"/etc/octavia/policy.yaml" | "/etc/octavia/policy.json"
		enforce_new_defaults: false
		enforce_scope:        false
	}
	oslo_messaging: {
		topic: "octavia_prov"
	}
	oslo_messaging_rabbit: {
		ssl:                 true
		ssl_ca_file:         "/etc/pki/tls/certs/ca-bundle.crt"
		amqp_durable_queues: true
		rabbit_quorum_queue: true
	}
	oslo_middleware: {
		enable_proxy_headers_parsing: *true | bool
	}
	haproxy_amphora: {
		server_ca:   "/etc/octavia/octavia-certificates/server_ca.cert.pem"
		client_cert: "/etc/octavia/octavia-certificates/client.cert-and-key.pem"
	}
	certificates: {
		ca_private_key: "/etc/octavia/octavia-certificates/server_ca.key.pem"
		ca_certificate: "/etc/octavia/octavia-certificates/server_ca.cert.pem"
	}
	controller_worker: {
		amp_image_owner_id: string
		amp_image_tag:      "amphora"
		amp_ssh_key_name:   string
		amp_secgroup_list: [...string]
		amp_boot_network_list: [...string]
		amp_flavor_id:  string
		network_driver: "allowed_address_pairs_driver"
		compute_driver: "compute_nova_driver"
		amphora_driver: "amphora_haproxy_rest_driver"
		client_ca:      "/etc/octavia/octavia-certificates/client_ca.cert.pem"
	}
	glance: {
		service_name:         *"glance" | string
		endpoint_type:        *"internal" | string
		ca_certificates_file: keystone_authtoken.cafile
	}
	nova: {
		service_name:         *"nova" | string
		endpoint_type:        *"internal" | string
		ca_certificates_file: keystone_authtoken.cafile
	}
	health_manager: {
		controller_ip_port_list: [...string]
	}
}
