// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#messagequeue
crd.#memcached
crd.#issuer
{
	#kind:     "CeilometerDeployment"
	#plural:   "ceilometerdeployments"
	#singular: "ceilometerdeployment"
	#shortnames: ["ceilometerd", "ceilometerds"]
	#releases: ["zed", "2023.1", "2023.2", "2024.1"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"central",
			"notification",
			"ceilometerConfig",
			"ceilometerCompute",
			"messageQueue",
			"memcached",
			"targetRelease",
			"issuerRef",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			central: crd.replicated
			central: properties: replicas: default: 1
			central:      crd.topologySpreadConstraints
			notification: crd.replicated
			notification: properties: replicas: default: 1
			notification: crd.topologySpreadConstraints

			ceilometerConfig:  crd.#anyconfig
			ceilometerSecrets: crd.#configsecret

			ceilometerCompute: {
				crd.pernodeconfig
				properties:
					enabled: {
						type:        "boolean"
						default:     *true | bool
						description: "Deploy ceilometer-agent-compute"
					}
				properties: configTemplates: items: properties: ceilometerComputeConfig: crd.#anyconfig
			}

			additionalHosts: {
				type: "array"
				items: {
					type: "object"
					required: ["hostnames", "ip"]
					properties: {
						hostnames: {
							type: "array"
							items: type: "string"
						}
						ip: type: "string"
					}
				}
			}
			ceilometerEventDefinitions: {
				type: "array"
				items: {
					type: "object"
					required: ["event_type", "traits"]
					properties: {
						event_type: {
							type: "array"
							items: type: "string"
						}
						traits: {
							type: "object"
							additionalProperties: {
								type:                                   "object"
								"x-kubernetes-preserve-unknown-fields": true
							}
						}
					}
				}
			}
			ceilometerPolling: {
				type: "array"
				items: {
					type: "object"
					required: ["name", "interval", "meters"]
					properties: {
						name: type:     "string"
						interval: type: "integer"
						meters: {
							type: "array"
							items: type: "string"
						}
						resources: {
							type: "array"
							items: type: "string"
						}
						discovery: {
							type: "array"
							items: type: "string"
						}
					}
				}
			}
			ceilometerEventPipeline: crd.#anyobj
			ceilometerPipeline:      crd.#anyobj
			pankoConfig:             crd.#configsecret

			central: properties: resources: {
				type: "object"
				properties: "ceilometer-agent-central": crd.#containerresources
			}
			notification: properties: resources: {
				type: "object"
				properties: "ceilometer-agent-notification": crd.#containerresources
			}
			ceilometerCompute: properties: resources: {
				type: "object"
				properties: "ceilometer-compute-agent": crd.#containerresources
			}
			jobResources: {
				type: "object"
				properties: "ceilometer-upgrade-job": crd.#containerresources
			}
			skip_gnocchi_bootstrap: {
				type:    "boolean"
				default: false
			}
		}
	}
}
