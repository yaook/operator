#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations

import collections.abc
import dataclasses
import io
import json
import math
import os.path
import pathlib
import re
import subprocess  # nosemgrep
import tempfile
import typing
import yaml

from json.decoder import JSONDecodeError

BASEDIR = os.path.dirname(__file__)
CUEDIR = pathlib.Path(__file__).parent.parent / "op" / "cue"


# This is a low-key fix for operator#300, to allow for sequences which are
# serialized to multiple keys instead of a comma-separated value of a single
# key.
#
# The correct, long-term fix would be to be able to import the schema from the
# actual service image being deployed, but barring that, we do this nasty thing
# here.
#
# With operator#371 this has been extended to use a custom key which
# can be used in the serializer to implement different cases.
#
# This dict is cuelang_module ↦ (section ↦ (key ↦ (custom_key ↦ flag)))
#
# With keep_default_at_list we add user defined values to the list, but keep
# the default at beginning.

# get_multivalued_injected_secrets() in sm/resources/base.py could be merged
# with get_injected_secrets() by using the multivalued knowledge from here.
OPENSTACK_TYPE_EXTRAS = {
    "ceilometer": {
        "notification": {
            "messaging_urls": {
                "multivalued": True,
                "keep_default_at_list": True,
            }
        },
    },
    "nova": {
        "filter_scheduler": {
            "available_filters": {
                "multivalued": True,
            }
        },
        "pci": {
            "alias": {
                "multivalued": True,
            },
            "passthrough_whitelist": {
                "multivalued": True,
            }
        },
    },
    "novacompute": {
        "pci": {
            "alias": {
                "multivalued": True
            },
            "passthrough_whitelist": {
                "multivalued": True
            },
        },
    },
    "glance": {
        "DEFAULT": {
            "enabled_import_methods": {
                "list_brackets": True
            },
        },
        "image_import_opts": {
            "image_import_plugins": {
                "list_brackets": True
            },
        },
    },
}


class FromJsonToCue(typing.Protocol):
    def __call__(
            self,
            data: ConfigInputMappingItem
            ) -> ConfigInputMappingItem: ...


class CueJsonToBinaryOutput(typing.Protocol):
    def __call__(self,
                 data: typing.Mapping[str, typing.Any],
                 modulename: str,
                 *,
                 encoding: str = "utf-8"
                 ) -> bytes: ...


class CueJsonToStringOutput(typing.Protocol):
    def __call__(self,
                 data: typing.Mapping[str, typing.Any],
                 modulename: str,
                 ) -> str: ...


@dataclasses.dataclass(frozen=True)
class CueConfigReference:
    """
    Class to use as a configuration reference to some other part of cue.
    """

    target: str


ConfigInputMappingItem = typing.Union[
    typing.Mapping[str, typing.Any],
    typing.Iterable[typing.Mapping[str, typing.Any]]
]
ConfigInputItem = typing.Union[ConfigInputMappingItem, CueConfigReference]
ConfigInput = typing.Iterable[ConfigInputItem]


@dataclasses.dataclass(frozen=True)
class ConfigFlavor:
    name: str
    from_json: FromJsonToCue
    serialize: typing.Union[CueJsonToBinaryOutput, CueJsonToStringOutput]
    filename_format: str

    def declare(self, contents: ConfigInput) -> ConfigDeclaration:
        return ConfigDeclaration(
            flavor=self,
            contents=list(contents),
        )


@dataclasses.dataclass(frozen=True)
class ConfigDeclaration:
    flavor: ConfigFlavor
    contents: typing.Collection[
        typing.Union[ConfigInputItem]
    ]


SupportedCueValue = typing.Any
ConfigSpec = typing.Mapping[str, ConfigDeclaration]
MutableConfigSpec = typing.MutableMapping[str, ConfigDeclaration]


class ConfigValidationError(Exception):
    """
    Exception being thrown if the config validation fails.

    :param message: The message for the user (e.g. probably the cuelang output)
    :param configinput: The configuration input for the compilation. This may
        contain sensitive data

    This exception signifies an error in the content of the configuration.
    Retrying will not help.
    """

    def __init__(self, message: str, configinput: typing.Optional[str] = None):
        self.message = message
        self.configinput = configinput

    def __str__(self):
        if self.configinput:
            return (f"{self.message}\n"
                    f"Template content: {self.configinput}")
        return self.message


class CuelangValidationError(ConfigValidationError):
    """
    Exception being thrown when the cuelang validation fails.
    """

    def __str__(self):
        return "cuelang output: " + super().__str__()


def _load_static_config(staticdir: str) -> typing.Mapping[str, str]:
    out = {}
    for filename in os.listdir(staticdir):
        with open(os.path.join(staticdir, filename)) as f:
            out[filename] = f.read()
    return out


def _serialize_openstack_primitive_value(v: typing.Any) -> str:
    if isinstance(v, str):
        if "\n" in v:
            # it can, in fact, be done. OpenStack supports continuation lines,
            # but AFAICT only if the newline is not immediately followed by
            # non-newline whitespace. that’s too crazy to do on a whim, even
            # for me.
            raise NotImplementedError(
                "support for serialising a string with newlines to OpenStack "
                "is not implemented yet"
            )

        # as oslo_config simply strips one leading and one trailing quote,
        # without any escaping, we could go ahead and wrap all values in
        # quotes.
        # however, we have to consider cases which are "stringly typed", such
        # as IP addresses.
        # until we support passing type hints to this function (it’d be really
        # great if we could export cuelang attributes), we’ll have to be
        # conservative about what we quote and what we pass through
        #
        # specifically, we’ll only quote things which start or end on a space
        # or a quote to ensure that those are handled correctly.
        if v and (v[0] in " '\"" or v[-1] in " '\""):
            v = f"'{v}'"

        # oslo_config treats `$` as a reference to another variable. In the
        # most cases this is not what the user wants so we will escape it for
        # them. To still support cases where a reference is needed the user
        # will need to escape the `$` with a backslash.
        def dollar_replace_func(match: re.Match) -> str:
            if match.group(0).startswith("\\"):
                return "$"
            return "$$"
        return re.sub(r"\\?\$", dollar_replace_func, v)

    if isinstance(v, bool):
        return ["false", "true"][v]
    if isinstance(v, (int, float)):
        if not math.isfinite(v):
            raise ValueError(
                "unsupported floating-point value for serialisation to "
                f"OpenStack config: {v!r}"
            )
        return str(v)

    raise TypeError(
        f"unsupported type for serialisation to OpenStack config: {type(v)!r}"
    )


def to_openstack_conf(
        data: typing.Mapping[str, typing.Mapping[str, typing.Any]],
        modulename: str,
        ) -> str:
    # FIXME: in the future, we'd rather have the type extras handed down from
    # the caller of build_config, because we'd want them to come from the
    # actual service image.
    type_extras = OPENSTACK_TYPE_EXTRAS.get(modulename, {})
    lines = []
    for section, values in sorted(
            data.items(),
            key=lambda x: x[0] if x[0] != "DEFAULT" else ""):
        lines.append(f"[{section}]")
        for key, value in values.items():
            is_proper_sequence = (
                isinstance(value, typing.Sequence)
                and not isinstance(value, str)
            )
            is_multivalued = (
                type_extras.get(section, {})
                .get(key, {})
                .get("multivalued", False)
            )
            use_list_brackets = (
                type_extras.get(section, {})
                .get(key, {})
                .get("list_brackets", False)
            )
            if is_multivalued:
                if not is_proper_sequence:
                    raise TypeError(
                        f"multi-valued key {key} in section {section} must be "
                        f"a non-string sequence, got {type(value)}: {value!r}"
                    )
                lines.extend(
                    f"{key} = {_serialize_openstack_primitive_value(item)}"
                    for item in value
                )
            else:
                if is_proper_sequence:
                    value_s = ",".join(map(
                        _serialize_openstack_primitive_value,
                        value,
                    ))
                    if use_list_brackets:
                        value_s = f"[{value_s}]"
                else:
                    value_s = _serialize_openstack_primitive_value(value)
                lines.append(f"{key} = {value_s}")
        lines.append("")
    return "\n".join(lines)


def to_yaml(
        data: typing.Mapping[str, typing.Mapping[str, typing.Any]],
        modulename: str,
        ) -> str:
    if data:
        return yaml.dump(data)
    return ''


def escape_str_using(to_escape: re.Pattern,
                     simple_escapes: typing.Mapping[str, str],
                     value: str) -> str:
    parts = []
    previous_end = 0
    for match in to_escape.finditer(value):
        start, end = match.span()
        parts.append(value[previous_end:start])
        matched_substr = value[start:end]
        sequence = simple_escapes.get(
            matched_substr,
            "x{:02x}".format(ord(matched_substr)),
        )
        parts.append("\\")
        parts.append(sequence)
        previous_end = end
    parts.append(value[previous_end:])
    return "".join(parts)


_TO_ESCAPE_MYSQL_RX = re.compile(
    "|".join(map(re.escape, "\"' \n\r\t\b"))
)


def _serialize_wsrep_provider_options(
        vs: typing.Mapping[str, typing.Any]) -> str:
    parts = []
    for key, value in vs.items():
        if not isinstance(value, (str, int, float)):
            raise TypeError(
                "unsupported value type for wsrep_provider_options: "
                f"{type(value)!r}"
            )
        parts.append(f"{key}={value}")
    return ";".join(parts)


def _serialize_optimizer_switch(
        vs: typing.Mapping[str, typing.Any]) -> str:
    parts = []
    for key, value in vs.items():
        parts.append(f"{key}={'on' if value else 'off'}")
    return ",".join(parts)


def _serialize_mysql_primitive_value(v: typing.Any) -> str:
    if isinstance(v, str):
        if _TO_ESCAPE_MYSQL_RX.search(v):
            escaped_v = escape_str_using(_TO_ESCAPE_MYSQL_RX,
                                         _SIMPLE_ESCAPES_S, v)
            return f'"{escaped_v}"'
        else:
            return v
    if isinstance(v, bool):
        return ["0", "1"][v]
    if isinstance(v, (float, int)):
        if not math.isfinite(v):
            raise ValueError(
                "unsupported floating-point value for serialisation to "
                f"MySQL config: {v!r}"
            )
        return repr(v)

    raise TypeError(
        f"unsupported type for serialisation to MySQL config: {type(v)!r}"
    )


def to_mysql_ini(
        data: typing.Mapping[str, typing.Mapping[str, typing.Any]],
        modulename: str,
        ) -> str:
    lines = []
    for section, values in sorted(
            data.items(),
            key=lambda x: x[0] if x[0] != "DEFAULT" else ""):
        lines.append(f"[{section}]")
        for key, value in values.items():
            if value is None:
                lines.append(key)
                continue
            # XXX: ugly special case
            if key == "wsrep_provider_options":
                value = _serialize_wsrep_provider_options(value)
            elif key == "optimizer_switch":
                value = _serialize_optimizer_switch(value)

            value_s = _serialize_mysql_primitive_value(value)
            lines.append(f"{key}={value_s}")
        lines.append("")
    return "\n".join(lines)


def _flatten_cuttlefish(
        data: typing.Iterable[typing.Tuple[str, typing.Any]],
        ) -> typing.Iterable[typing.Tuple[str, typing.Any]]:
    for key, value in data:
        if isinstance(value, dict):
            for subkey, subvalue in _flatten_cuttlefish(value.items()):
                yield f"{key}.{subkey}", subvalue
            continue
        if isinstance(value, list):
            for i, subvalue in enumerate(value, 1):
                yield f"{key}.{i}", subvalue
            continue
        yield key, value


def _serialize_cuttlefish_primitive_value(value: typing.Any) -> str:
    if isinstance(value, bool):
        return ["false", "true"][value]
    if isinstance(value, str):
        if "\n" in value or value.startswith(" ") or value.endswith(" "):
            raise ValueError(f"string {value!r} cannot be serialized to "
                             "Cuttlefish config")
        return value
    if isinstance(value, float):
        if not math.isfinite(value):
            raise ValueError(
                "unsupported floating-point value for serialization to "
                f"Cuttlefish config: {value!r}"
            )
        return str(value)
    if isinstance(value, int):
        return str(value)

    raise TypeError(
        "unsupported type for serialization to Cuttlefish config: "
        f"{type(value)!r}"
    )


def to_cuttlefish(
        data: typing.Mapping[str, typing.Any],
        modulename: str,
        ) -> str:
    lines = []
    for key, value in _flatten_cuttlefish(data.items()):
        value_s = _serialize_cuttlefish_primitive_value(value)
        lines.append(f"{key}={value_s}")
    return "\n".join(lines)


_TO_ESCAPE_S_RX = re.compile(
    "|".join(map(re.escape, map(chr, range(0x00, 0x20)))) + "|\"|\\\\"
)
_TO_ESCAPE_B_RX = re.compile(
    b"|".join(
        map(re.escape, (bytes([x]) for x in range(0x00, 0x20)))
    ) + b"|'|\\\\|" + b"|".join(
        map(re.escape, (bytes([x]) for x in range(0x80, 0x100)))
    )
)
_SIMPLE_ESCAPES_S = {
    "\n": "n",
    "\t": "t",
    "'": "'",
    '"': '"',
    "\\": "\\",
    " ": "s",
}
_SIMPLE_ESCAPES_B = {
    b"\n": "n",
    b"\t": "t",
    b"'": "'",
    b'"': '"',
    b"\\": "\\",
}
_IDENTIFIER_RE = re.compile(
    r"^(#|_#)?([^\W\d]|\$)[\w$]*$",
)


def is_cue_identifier(s: str) -> bool:
    if s == "_" or s == "$":
        return False
    return _IDENTIFIER_RE.match(s) is not None


def _format_str(value: str) -> str:
    return f'"{escape_str_using(_TO_ESCAPE_S_RX, _SIMPLE_ESCAPES_S, value)}"'


def _format_bytes(value: bytes) -> str:
    parts = ["'"]
    previous_end = 0
    for match in _TO_ESCAPE_B_RX.finditer(value):
        start, end = match.span()
        parts.append(value[previous_end:start].decode("ascii"))
        matched_substr = value[start:end]
        sequence = _SIMPLE_ESCAPES_B.get(
            matched_substr,
            "x{:02x}".format(ord(matched_substr)),
        )
        parts.append("\\")
        parts.append(sequence)
        previous_end = end
    parts.append(value[previous_end:].decode("ascii"))
    parts.append("'")
    return "".join(parts)


def _serialize_struct(
        st: typing.Mapping[str, typing.Any],
        modulename: str = "",
        section: str = "",
        ) -> str:
    type_extras = OPENSTACK_TYPE_EXTRAS.get(modulename, {})
    parts = ["{"]
    for k, v in st.items():
        # In case we want to keep the default value at the list, we need to
        # add `_` as first item, so cuelang will set the default there.
        keep_default_at_list = (
            type_extras.get(section, {})
            .get(k, {})
            .get("keep_default_at_list", False)
        )
        if keep_default_at_list and isinstance(v, collections.abc.Sequence):
            parts.append(f"{_format_struct_label(k)}: "
                         f"{_serialize_list_with_default(v)}")
        else:
            parts.append(f"{_format_struct_label(k)}: {_serialize_value(v)}")
    parts.append("}")
    return "\n".join(parts)


def _serialize_list(lst: typing.Iterable[typing.Any]) -> str:
    return f"[{', '.join(map(_serialize_value, lst))}]"


def _serialize_list_with_default(lst: typing.Iterable[typing.Any]) -> str:
    return f"[_, {', '.join(map(_serialize_value, lst))}]"


def _serialize_value(
        value: typing.Any,
        modulename: str = "",
        section: str = "",
        ) -> str:
    if value is None:
        return "null"
    if isinstance(value, bytes):
        return _format_bytes(value)
    if isinstance(value, str):
        return _format_str(value)
    if isinstance(value, bool):
        return ["false", "true"][value]
    if isinstance(value, (int, float)):
        if not math.isfinite(value):
            raise ValueError(
                "unsupported floating-point value for serialisation to Cue: "
                f"{value!r}"
            )
        return str(value)
    if isinstance(value, collections.abc.Mapping):
        return _serialize_struct(value, modulename, section)
    if isinstance(value, collections.abc.Sequence):
        return _serialize_list(value)
    if isinstance(value, CueConfigReference):
        return value.target
    raise TypeError(
        f"unsupported type for serialisation to Cue: {type(value)!r}"
    )


def _format_struct_label(key: str) -> str:
    if is_cue_identifier(key):
        return key
    return _format_str(key)


def _serialize_config_input(
        modulename: str,
        configinput: ConfigInput,
        out: typing.BinaryIO,
        ) -> None:
    textout = io.TextIOWrapper(
        out,
        encoding="utf-8",
        line_buffering=False,
    )

    def _print_config_spec(input: ConfigInput) -> None:
        for snippet in input:
            if isinstance(snippet, CueConfigReference):
                print(f"{snippet.target}", file=textout)
            elif isinstance(snippet, list) or isinstance(snippet, set):
                print("[{", file=textout)
                for i in snippet:
                    for k, v in i.items():
                        _print_config_spec([({k: v})])
                print("}]", file=textout)
            elif isinstance(snippet, typing.Mapping):
                for k, v in snippet.items():
                    print(f"{_format_struct_label(k)}: "
                          f"{_serialize_value(v, modulename, k)}",
                          file=textout)

    print(f"""
import (
    "yaook.cloud/{modulename}"
)

conf: {{
    {modulename}.{modulename}_conf_spec""", file=textout)
    _print_config_spec(configinput)
    print("}", file=textout)
    textout.flush()
    textout.close()


def render_cue_template(
        template: pathlib.Path,
        include_path: pathlib.Path,
        ) -> typing.Mapping[str, str]:
    with tempfile.TemporaryDirectory() as scratchspace:
        scratchspace_path = pathlib.Path(scratchspace)
        inputfile = scratchspace_path / "input.cue"
        inputfile.symlink_to(template)

        pkg_path = scratchspace_path / "pkg"
        pkg_path.symlink_to(include_path)

        cue_output = subprocess.run(  # nosemgrep
            # inputfile.name is completely controlled by us.
            # the content of the input file contains escaped inputs from
            # untrusted sources.
            # cue is not a absolute path, which should not be an issue as we
            # completly control the container image.
            ["cue", "export", inputfile.name],
            cwd=str(scratchspace_path),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        try:
            cue_output.check_returncode()
            cuejson = json.loads(cue_output.stdout)
            return cuejson
        except (JSONDecodeError, subprocess.CalledProcessError):
            filecontent = ""
            try:  # nosemgrep as this is just for convenience
                with inputfile.open("r") as fin:
                    filecontent = fin.read()
            except Exception:
                pass
            msg = "Cue: "
            if cue_output and cue_output.stderr:
                msg += f"stderr: {cue_output.stderr.decode('ascii')}. "
            if cue_output and cue_output.stdout:
                msg += f"stdout: {cue_output.stdout.decode('ascii')}. "
            raise CuelangValidationError(msg, filecontent)


def _genconfig(
        modulename: str,
        configinput: ConfigInput,
        ) -> typing.Mapping:
    with tempfile.TemporaryDirectory() as scratchspace:
        scratchspace_path = pathlib.Path(scratchspace)
        inputfile = scratchspace_path / "input.cue"
        with inputfile.open("xb") as fout:
            _serialize_config_input(
                modulename,
                configinput,
                fout,
            )

        return render_cue_template(
            inputfile,
            CUEDIR / "pkg",
        )


def build_config(
        modules: ConfigSpec,
        ) -> typing.Dict[str, typing.Union[str, bytes]]:
    config = {}
    for modulename, configdecl in modules.items():
        configinput: ConfigInput = map(
            lambda e: e if isinstance(e, CueConfigReference)
            else configdecl.flavor.from_json(e),
            configdecl.contents,
        )
        dynconfig = _genconfig(modulename, configinput)
        filename = configdecl.flavor.filename_format.format(
            modulename=modulename,
        )
        config[filename] = configdecl.flavor.serialize(
            dynconfig["conf"],
            modulename,
        )
    return config


OSLO_CONFIG = ConfigFlavor(
    name="oslo_config",
    from_json=lambda x: x,
    serialize=to_openstack_conf,
    filename_format="{modulename}.conf",
)


def ceph_normalize_underscores(
        data: ConfigInputMappingItem,
        ) -> ConfigInputMappingItem:
    if not isinstance(data, typing.Mapping):
        raise TypeError(
            "unsupported value type for ceph_normalize_underscores: "
            f"{type(data)!r}"
        )

    result: typing.Dict[str, typing.Dict[str, typing.Any]] = {}
    for in_section, in_pairs_ in data.items():
        in_pairs = typing.cast(typing.Mapping[str, typing.Any], in_pairs_)
        out_section = result.setdefault(in_section, {})
        for in_key, in_value in in_pairs.items():
            out_section[in_key.replace(" ", "_")] = in_value
    return result


CEPH_CONFIG = ConfigFlavor(
    name="ceph",
    from_json=ceph_normalize_underscores,
    # FIXME: that’s not the correct serializer for ceph
    serialize=to_openstack_conf,
    filename_format="{modulename}.conf",
)


def mysql_normalize_underscores(
        data: ConfigInputMappingItem,
        ) -> ConfigInputMappingItem:
    if not isinstance(data, typing.Mapping):
        raise TypeError(
            "unsupported value type for ceph_normalize_underscores: "
            f"{type(data)!r}"
        )

    result: typing.Dict[str, typing.Dict[str, typing.Any]] = {}
    for in_section, in_pairs_ in data.items():
        in_pairs = typing.cast(typing.Mapping[str, typing.Any], in_pairs_)
        out_section = result.setdefault(in_section, {})
        for in_key, in_value in in_pairs.items():
            out_section[in_key.replace("-", "_")] = in_value
    return result


MYSQL_INI = ConfigFlavor(
    name="my.cnf",
    from_json=mysql_normalize_underscores,
    serialize=to_mysql_ini,
    filename_format="my.cnf",
)

CUTTLEFISH = ConfigFlavor(
    name="cuttlefish",
    from_json=lambda x: x,
    serialize=to_cuttlefish,
    filename_format="{modulename}.conf",
)

YAML_CONFIG = ConfigFlavor(
    name="yaml",
    from_json=lambda x: x,
    serialize=to_yaml,
    filename_format="{modulename}.yaml",
)
