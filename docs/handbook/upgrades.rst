Upgrades
========

To get new features, fix bugs and have a newer OpenStack version, we need to
upgrade.

Most of this is copied from :doc:`../concepts/updates`. For more information
have a look there.

OpenStack upgrades
------------------

To support multiple versions of OpenStack all OpenStack services have a
``.spec.targetRelease`` field. In this field you can specify the release of
this OpenStack service you would like to use (e.g. ``zed``).

In order to upgrade to a new OpenStack release ensure that your service has
rolled out successfully (so that it is in the ``Updated`` state). Also you
need to rollout an operator release supporting the new OpenStack versions.
Afterwards you can change the ``.spec.targetRelease`` of your service. If the
service is capable of non-disruptive upgrades this method will be used.
Otherwise the upgrade process might cause disruptions.

.. note::

    You can only upgrade a single version at a time. Jumping over
    releases is not supported.

The upgrade is finished once the service is again in the ``Updated`` state.

If at any point you want to view the OpenStack version currently rolled out at
the service you can check ``.status.installedRelease``. This field will always
point to the last release that has completed a full rollout. So during normal
operation it will be the same as ``.spec.targetRelease``. During upgrades it
will show the version you are upgrading from.

For documentation on how the upgrade procedure is implemented please view
:ref:`implementation_details.openstack_upgrades`.
