#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.instancing` – Manage groups of resources
##################################################################

This module contains a bunch of classes to manage sets of resources in
different ways. The key difference is whether the resources are considered
stateful (like as in StatefulSet: resources need active lifecycle management
to not break) or stateless (think ConfigMaps, Secrets, ...).

The following clasess implement node selector based instancing of resources
using those two paradigms:

.. currentmodule:: yaook.statemachine

.. autosummary::

    PerNode
    StatefulPerNode

The main logic is implemented in:

.. currentmodule:: yaook.statemachine.instancing

.. autosummary::

    StatelessInstancedResource
    StatefulInstancedResource

To achieve per-node semantics, the following mixin is used:

.. autosummary::

    PerNodeMixin

The lowest-level building block is:


.. autosummary::

    InstancedResource

.. currentmodule:: yaook.statemachine

High level classes
==================

.. autoclass:: PerNode

.. autoclass:: StatefulPerNode

.. currentmodule:: yaook.statemachine.instancing

Building blocks
===============

Business logic base classes
---------------------------

.. autoclass:: StatelessInstancedResource

.. autoclass:: StatefulInstancedResource


Mixins
------

.. autoclass:: PerNodeMixin

Base class
----------

.. autoclass:: InstancedResource

"""  # noqa
import abc
import asyncio
import dataclasses
from datetime import datetime
import enum
import jsonpatch
import math
import typing

import kubernetes_asyncio.client as kclient
from opentelemetry import trace

from .. import (
    api_utils,
    context,
    exceptions,
    interfaces,
    watcher,
    customresource,
)
from . import base
from .base import (
    DependencyMap,
    ResourceBody,
    # TODO(resource-refactor): make it non-private
    _scheduling_keys_to_selectors,
    inject_namespace_affinity,
)
from .k8s import KubernetesResource, SingleObject, _encode_body
from .k8s_workload import StatefulSet
from yaook.statemachine.tracing import start_as_current_span_async


tracer = trace.get_tracer(__name__)


T = typing.TypeVar("T")


class ResourceRunState(enum.Enum):
    """
    .. attribute:: STARTING

        If the resource is currently in the process of becoming ready. May also
        be used if the resource is currently updating.

    .. attribute:: READY

        If the resource is ready to be used.

    .. attribute:: UNKNOWN

        If the resource has a state where we do not have further
        information about. We assume that it will recover itself
        from this state.

    .. attribute:: SHUTTING_DOWN

        If the resource is getting cleaned up and will eventually disappear.

    .. attribute:: DELETION_REQUIRED

        If the resource is in a dangerous state and needs to be deleted
        immediately, even if it may violate the max_unavailable guarantees.

    """
    STARTING = 'starting'
    READY = 'ready'
    UNKNOWN = 'unknown'
    SHUTTING_DOWN = 'shutting-down'
    DELETION_REQUIRED = 'immediate-deletion-required'


class ResourceSpecState(enum.Enum):
    UP_TO_DATE = 'up-to-date'
    STALE = 'stale'


@dataclasses.dataclass(frozen=True)
class InstanceStatistics:
    nconfigured: int
    nexisting_instances: int
    nexisting_resources: int
    nupdated: int
    nready: int
    navailable: int
    ncreated: int
    ndeleted: int
    spare_nodes: typing.List[str]

    def no_resource_exists(self) -> bool:
        return self.nconfigured == 0 and \
            self.nexisting_instances == 0 and \
            self.nexisting_resources == 0 and \
            self.nupdated == 0 and \
            self.nready == 0 and \
            self.navailable == 0


@dataclasses.dataclass(frozen=True)
class ResourceInfo(typing.Generic[T]):
    metadata: ResourceBody
    body: T


class WinnerInfo(typing.NamedTuple):
    ref: kclient.V1ObjectReference
    run_state: ResourceRunState
    spec_state: ResourceSpecState
    creation_timestamp: typing.Optional[str] = ""
    recreation_reason_count: typing.Optional[int] = 0

    def __repr__(self):
        return "(<Reference namespace={!r} name={!r}>, {!r}, {!r})".format(
            self.ref.namespace,
            self.ref.name,
            self.run_state,
            self.spec_state,
        )


@dataclasses.dataclass
class InstanceGroup:
    ref: typing.Optional[kclient.V1ObjectReference]
    max_unavailable: str
    prevent_deletion: bool
    stats: typing.Optional[InstanceStatistics]
    field_manager: str
    disruptive_maintenance: bool
    spare_nodes: int

    @property
    def name(self):
        if self.ref:
            return self.ref.name
        return "DEFAULT"

    def __init__(self, ref, max_unavailable, stats,
                 field_manager=None,
                 prevent_deletion=False,
                 disruptive_maintenance=False,
                 spare_nodes=0,
                 spare_aggregate=None,
                 ):
        self.prevent_deletion = prevent_deletion
        self.ref = ref
        self.max_unavailable = max_unavailable
        self.stats = stats
        self.field_manager = field_manager
        self.disruptive_maintenance = disruptive_maintenance
        self.spare_nodes = spare_nodes

    def __repr__(self):
        return "InstanceGroup({!r}, {!r}, {!r}, {!r}, {!r}, {!r})".format(
            self.name,
            self.max_unavailable,
            self.stats,
            self.field_manager,
            self.prevent_deletion,
            self.spare_nodes,
        )

    def get_max_unavailable(self, configured: int) -> int:
        """
        Calculates the number of max unavailable instances.

        Potentially converts percentages to the actual numbers of unavailable
        instances.
        """
        if self.max_unavailable == "0" or self.max_unavailable == "0%":
            return 0
        try:
            return int(self.max_unavailable)
        except ValueError:
            percentage = int(self.max_unavailable[:-1])
            max_unavailable = configured * percentage/100
            return max(1, math.floor(max_unavailable))

    def get_prevent_deletion(self) -> bool:
        return self.prevent_deletion

    def get_disruptive_maintenance(self) -> bool:
        return self.disruptive_maintenance


@dataclasses.dataclass
class InstanceState:
    winner: typing.Optional[WinnerInfo]
    intended_body: ResourceBody
    instance_data: typing.Any  # the same as the value of get_target_instances
    has_any_resources: bool
    may_create_resources: bool

    @property
    def is_up_to_date(self):
        return (
            self.winner is not None and
            self.winner.spec_state == ResourceSpecState.UP_TO_DATE
        )

    @property
    def is_ok(self):
        return (
            self.winner is not None and
            (
                self.winner.run_state == ResourceRunState.STARTING or
                self.winner.run_state == ResourceRunState.READY
            )
        )

    @property
    def is_available(self):
        return (
            self.winner is not None and
            self.winner.run_state == ResourceRunState.READY
        )

    @property
    def can_create_new_resource(self):
        return not self.has_any_resources and self.may_create_resources


class InstancedResource(KubernetesResource[T]):
    def __init__(
            self,
            *,
            wrapped_state: SingleObject[T],
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self._declare_dependencies(*wrapped_state._dependencies)
        self._wrapped_state = wrapped_state

    def __set_name__(self, owner: typing.Type, name: str) -> None:
        super().__set_name__(owner, name)
        self._wrapped_state.__set_name__(owner, name)

    def get_listeners(self) -> typing.List[context.Listener]:
        return (
            super().get_listeners() +
            self._wrapped_state.get_listeners()
        )

    def labels(self, ctx: context.Context) -> typing.MutableMapping[str, str]:
        return self._wrapped_state.labels(ctx)

    async def get_with_instanced(
            self,
            ctx: context.Context,
            ) -> typing.Tuple[kclient.V1ObjectReference, bool]:
        if ctx.instance is None:
            raise exceptions.AmbiguousRequest(
                self.component,
                ctx,
            )
        return (await self._wrapped_state.get(ctx), True)

    async def get_all(
            self,
            ctx: context.Context,
            show_deleted: bool = False
            ) -> typing.Mapping[
                typing.Optional[str],
                kclient.V1ObjectReference,
            ]:
        return await self._wrapped_state.get_all(ctx, show_deleted)

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient,
            ) -> interfaces.ResourceInterface[T]:
        return self._wrapped_state._autocreate_resource_interface(api_client)

    @start_as_current_span_async(tracer, "get_used_resources")
    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        instances = (await self.get_all(ctx, True)).keys()
        gathered_results = await asyncio.gather(*(
            self._wrapped_state.get_used_resources(ctx.with_instance(instance))
            for instance in instances
            if instance is not None
        ))
        result = set()
        for used_resources in gathered_results:
            result.update(used_resources)
        return result

    @abc.abstractmethod
    async def get_target_instances(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, typing.Any]:
        """
        Return a mapping of strings to arbitrary data.
        The keys of the mapping identifiy the instances to maintain.
        The value can be used to store arbitrary data attached to the instance.

        Resources which do not match any of those instances will eventually be
        deleted. If no resource exists for an instance, a new one will be
        created.

        The exact mode of management depends on the subclass.
        """

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
    ) -> None:
        intf = self._wrapped_state.get_resource_interface(ctx)
        labels = self.labels(ctx)
        instances = await intf.list_(ctx.namespace, label_selector=labels)
        for instance in instances:
            metadata = api_utils.extract_metadata(instance)
            await self._wrapped_state._orphan(ctx,
                                              metadata["namespace"],
                                              metadata["name"])


if typing.TYPE_CHECKING:
    _FakeBase = base.Resource
else:
    _FakeBase = object


class PerNodeMixin(_FakeBase):
    def __init__(
            self,
            *,
            scheduling_keys: typing.Collection[str],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._scheduling_keys = list(scheduling_keys)

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[kclient.V1Node](
                '', 'v1', 'nodes',
                self._handle_node_event,
                broadcast=True, component=self.component,
            ),
        ]

    def _get_node_selectors(
                self,
                ctx: context.Context,
                ) -> typing.Iterable[api_utils.LabelSelector]:
        selectors = typing.cast(typing.List,
                                _scheduling_keys_to_selectors(
                                    self._scheduling_keys)
                                )
        return inject_namespace_affinity(ctx, selectors)

    def _handle_node_event(
            self,
            ctx: context.Context,
            event: watcher.StatefulWatchEvent[
                kclient.V1Node,
            ],
            ) -> bool:
        if (event.type_ == watcher.EventType.ADDED or
                event.type_ == watcher.EventType.DELETED or
                event.old_object is None):
            return True
        return (
            event.old_object.metadata.labels != event.object_.metadata.labels
        )

    async def _get_target_nodes(
            self,
            ctx: context.Context,
            ) -> typing.List[kclient.V1Node]:
        selectors = self._get_node_selectors(ctx)
        return list(await interfaces.get_selected_nodes_union(
            ctx.api_client,
            selectors,
        ))

    @start_as_current_span_async(tracer, "get_target_instances")
    async def get_target_instances(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, typing.Any]:
        return {
            node.metadata.name: node.metadata.labels
            for node in await self._get_target_nodes(ctx)
        }


class StatelessInstancedResource(InstancedResource[T]):
    """
    Manage multiple instances of a resource which does not require state
    management.

    :param wrapped_state: The resource to manage
    """
    async def _get_current_metadata(
            self,
            ctx: context.Context) -> typing.List[typing.Mapping]:
        resources = self._wrapped_state.get_resource_interface(ctx)
        return [
            api_utils.extract_metadata(obj)
            for obj in await resources.list_(
                ctx.namespace,
                label_selector=api_utils.LabelSelector(
                    match_labels=self._wrapped_state.labels(ctx),
                    match_expressions=[api_utils.LabelExpression(
                        key=context.LABEL_ORPHANED,
                        operator=api_utils.SelectorOperator.NOT_EXISTS,
                        values=None,
                    )],
                ).as_api_selector(),
            )
        ]

    def _find_stale_instances(
            self,
            instances_metadata: typing.List[typing.Mapping],
            instances: typing.Mapping[str, typing.Any],
            ) -> typing.List[str]:
        result = []
        active_instances = set()
        for metadata in instances_metadata:
            instance_label = metadata.get(
                "labels", {}
            ).get(context.LABEL_INSTANCE)
            if instance_label not in instances.keys():
                result.append(metadata["name"])
                continue

            if instance_label not in active_instances:
                active_instances.add(instance_label)
                continue

            result.append(metadata["name"])

        return result

    async def _reconcile_nodes(
            self,
            instances: typing.Mapping[str, typing.Any],
            ctx: context.Context,
            *,
            dependencies: DependencyMap) -> None:
        all_continueable = True
        errors: typing.List[typing.Tuple[str, Exception]] = []
        for instance, instance_data in instances.items():
            try:
                await self._wrapped_state.reconcile(
                    ctx.with_instance(instance, instance_data),
                    dependencies=dependencies,
                )
            except exceptions.ContinueableError as exc:
                ctx.logger.warn("failed to converge %r for node %s"
                                " (continueable)",
                                self._wrapped_state,
                                instance)
                errors.append((instance, exc))
                continue
            except Exception as exc:
                ctx.logger.error("failed to converge %r for node %s",
                                 self._wrapped_state,
                                 instance,
                                 exc_info=True)
                errors.append((instance, exc))
                all_continueable = False
                continue

        if errors:
            if all_continueable:
                # If there are only ContinueableError, we make sure that we
                # also raise a ContinueableError as that is treated specially
                # by the statemachine.
                ctx.logger.warn(
                    "%s Instance(s) failed to converge (continueable)",
                    len(errors)
                )
                for instance, exc_value in errors:
                    exception = typing.cast(
                        exceptions.ContinueableError,
                        exc_value,
                    )
                    ctx.logger.warn("InstanceState %s failed with: %s",
                                    instance, exception.msg)
                raise exceptions.ContinueableError(
                    "Continuable error in {}".format(self._wrapped_state),
                    True,
                )
            else:
                raise RuntimeError("Instance(s) {} failed to converge".format(
                    ", ".join(
                        "{} ({}: {})".format(instance, type(exc).__name__,
                                             exc)
                        for instance, exc in errors)
                    )
                )

    async def reconcile(
            self,
            ctx: context.Context,
            *,
            dependencies: DependencyMap,
    ) -> None:
        if ctx.instance is not None:
            raise RuntimeError(
                "nesting replicating states is not supported yet"
            )

        target_instances = await self.get_target_instances(ctx)
        current_instances = await self._get_current_metadata(ctx)

        # delete all instances which do not belong to a node or are duplicates
        to_delete = self._find_stale_instances(
            current_instances,
            target_instances,
        )

        # clean up those which need to be gone early, so that we auto-resolve
        # any AmbiguousRequest which may happen later on
        await asyncio.gather(
            *(
                self._wrapped_state._orphan(ctx, ctx.namespace, name)
                for name in to_delete
            )
        )

        await self._reconcile_nodes(
            target_instances,
            ctx,
            dependencies=dependencies,
        )

    async def is_ready(self, ctx: context.Context) -> bool:
        instances = await self.get_target_instances(ctx)
        ready = True
        for instance, instance_data in instances.items():
            ready = ready and await self._wrapped_state.is_ready(
                ctx.with_instance(instance, instance_data),
            )
        return ready


class PerNode(PerNodeMixin, StatelessInstancedResource[T]):
    # NOTE: This constructor exists only for type hinting purposes. Otherwise,
    # mypy is not able to infer the specific type of PerNode from the passed
    # wrapped_state argument.
    def __init__(
            self,
            *,
            scheduling_keys: typing.Collection[str],
            wrapped_state: SingleObject[T],
            **kwargs: typing.Any):
        super().__init__(
            scheduling_keys=scheduling_keys,
            wrapped_state=wrapped_state,
            **kwargs,
        )


class PerSetEntry(StatelessInstancedResource[T]):
    def __init__(
            self,
            *,
            accessor: typing.Callable[
                [context.Context],
                typing.Awaitable[typing.Set[str]]],
            wrapped_state: SingleObject[T],
            **kwargs: typing.Any):
        super().__init__(
            wrapped_state=wrapped_state,
            **kwargs,
        )
        self._accessor = accessor

    @start_as_current_span_async(tracer, "get_target_instances")
    async def get_target_instances(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, typing.Any]:
        return {x: None for x in await self._accessor(ctx)}


class PerStatefulSetPod(StatelessInstancedResource[T]):
    def __init__(
            self,
            *,
            statefulset: StatefulSet,
            wrapped_state: SingleObject[T],
            **kwargs: typing.Any):
        super().__init__(
            wrapped_state=wrapped_state,
            **kwargs,
        )
        self._statefulset = statefulset
        self._declare_dependencies(statefulset)

    @start_as_current_span_async(tracer, "get_target_instances")
    async def get_target_instances(
            self,
            ctx: context.Context,
            ) -> typing.Mapping[str, typing.Any]:
        ss = await self._statefulset._get_current(ctx)
        replicas = ss.spec.replicas
        name = ss.metadata.name
        return {f"{name}-{i}": None for i in range(replicas)}


def _sort_instances(instances: typing.Mapping[str, InstanceState]) -> \
        typing.List[typing.Tuple[str, InstanceState]]:
    """
    :param instances: Dict based of instance name and instance_state
    :return sorted_instances: Sorted list of Tuples containing the name
        and the instance_state

    Sorts the instances of the computeNodes in order to decide wich of them
    schould be recreated first. This is based on this criteria:
    1. A Node with ResourceRunState.UNKNOWN will be put first
    2. Based on the number of reasons, the node has to be recreated
        (more is better)
    3. Based on the creation Timestamp (older is better)

    The criteria above are evaluated in order; the first one to be not
    equal for two instances will be the tiebreaker.
    """

    scored_instances = []
    no_winner_found = []
    for instance, instance_state in instances.items():
        if not instance_state.winner:
            no_winner_found.append((instance, instance_state))
            continue
        winner = instance_state.winner._asdict()
        scored_instances.append(
            (
                (
                    winner.get("run_state") != ResourceRunState.UNKNOWN,
                    0-winner.get("recreation_reason_count", 0),
                    winner.get("creation_timestamp", "")
                ),
                instance, instance_state
            )
        )

    scored_instances.sort(key=lambda x: x[0])

    sorted_instances = [(instance, instance_state)
                        for _, instance, instance_state
                        in scored_instances]
    sorted_instances.extend(no_winner_found)
    return sorted_instances


class StatefulInstancedResource(InstancedResource[T]):
    """
    Carefully Manage multiple instances of a stateful resource.

    :param wrapped_state: The resource to manage

    See :meth:`reconcile` for the logic applied for the resource management.

    **Interface for subclasses**:

    .. automethod:: _group_instances

    .. automethod:: _get_run_state

    .. automethod:: _get_spec_state
    """

    def __init__(self, allow_prevent_deletion: bool = False, *args: typing.Any,
                 **kwargs: typing.Any) -> None:
        super().__init__(*args, **kwargs)
        self.allow_prevent_deletion = allow_prevent_deletion

    @start_as_current_span_async(tracer, "_group_instances")
    async def _group_instances(
            self,
            ctx: context.Context,
            instances: typing.Mapping[str, InstanceState]
            ) -> typing.List[typing.Tuple[
                InstanceGroup, typing.Mapping[str, InstanceState]]]:
        """
        Group instances by an implementation defined rule.

        :param ctx: The context of the operation.
        :param instances: The map of all instances managed by this resource.

        Return a list of tuples, where each entry contains an
        :class:InstanceGroup and all instances associated with that group.
        """
        return [
            (InstanceGroup(None, "1", None), instances)
        ]

    async def _make_complete_body(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> ResourceBody:
        body = await self._wrapped_state._make_body(ctx, dependencies)
        await self._wrapped_state.adopt_object(ctx, body)
        return body

    @abc.abstractmethod
    def _get_spec_state(
            self,
            ctx: context.Context,
            intent: typing.Mapping,
            instance: T,
            ) -> ResourceSpecState:
        """
        Get the state of the instance’s spec.

        :param ctx: The context of the operation.
        :param intent: The intended state of the resource as returned by the
            `wrapped_state`.
        :param instance: The current state of the resource as returned by the
            Kubernetes API.

        Return:

        - :attr:`~.ResourceSpecState.UP_TO_DATE` if the spec of the resource
          does not need updating.
        - :attr:`~.ResourceSpecState.STALE` if the spec of the resource
          needs updating.
        """
        raise NotImplementedError

    def number_of_recreation_reasons(
            self,
            ctx: context.Context,
            intent: typing.Mapping,
            instance: typing.Mapping,
            ) -> int:
        return 0

    @abc.abstractmethod
    def _get_run_state(
            self,
            ctx: context.Context,
            instance: T,
            ) -> ResourceRunState:
        """
        For the definitions of all possible states, please look at the
        defintion of enumerations of states.
        :class:ResourceRunState
        """
        raise NotImplementedError

    def _get_node_name(self, resource_info: ResourceInfo) -> str:
        return resource_info.metadata["name"]

    @start_as_current_span_async(tracer, "_get_current_instances")
    async def _get_current_instances(
            self,
            ctx: context.Context,
            ) -> typing.Tuple[
                typing.Mapping[
                    str,
                    typing.Collection[ResourceInfo]
                ],
                typing.Collection[ResourceInfo],
            ]:
        intf = self._wrapped_state.get_resource_interface(ctx)
        resources = await intf.list_(
            ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels=self.labels(ctx),
            ).as_api_selector(),
        )
        result: typing.Dict[str, typing.List[ResourceInfo]] = {}
        leftovers = []
        for resource in resources:
            metadata = api_utils.extract_metadata(resource)
            try:
                instance = metadata["labels"][context.LABEL_INSTANCE]
            except KeyError:
                leftovers.append(ResourceInfo(metadata, resource))
                continue
            result.setdefault(instance, []).append(
                ResourceInfo(metadata, resource),
            )
        return result, leftovers

    def _select_winner(
            self,
            ctx: context.Context,
            intended_body: ResourceBody,
            instances: typing.Collection[ResourceInfo],
            ) -> typing.Tuple[
                typing.Optional[WinnerInfo],
                typing.Collection[kclient.V1ObjectReference]
            ]:
        """
        Select the resource to keep out of a collection of resources, if any.

        :param ctx: The context of the operation.
        :param intended_body: The intended body of the resource
        :param instances: The instance information as collection of tuples.
            The tuples consist of the current metadata (as dict) and the
            current instance.
        :return: The optional winner plus its state and a collection of all
            resources to delete.

        The winning resource is the one with the best internal score. The
        scoring system works like this: The criteria below are evaluated in
        order; the first one to be not equal for two instances will be the
        tiebreaker.

        - Up-to-date is better than stale
        - Ready or starting is better
        - Ready is better
        - Unknown is better (NB: as ready and starting have been selected
          beforehand, when we have the choice between unknown and shutting
          down, we pick unknown because that might heal, while shutting down
          will not.)
        - Older is better

        In addition, any resource which is marked with
        :attr:`~.ResourceRunState.IMMEDIATE_DELETION_REQUIRED` or which has a
        not-:data:`None` deletion timestamp is not considered for the winner at
        all; Resources which have
        :attr:`~.ResourceRunState.IMMEDIATE_DELETION_REQUIRED` are marked for
        deletion in any case, while resources which have a not-:data:`None`
        deletion timestamp are not returned in either return value.
        """
        to_delete = []
        scored_refs = []
        for instance_info in instances:
            metadata = instance_info.metadata
            instance = instance_info.body
            spec_state = self._get_spec_state(ctx, intended_body, instance)
            run_state = self._get_run_state(ctx, instance)
            recreation_reason_count = self.number_of_recreation_reasons(
                ctx, intended_body, instance)
            if metadata.get("deletionTimestamp") is not None:
                continue
            if metadata.get("labels", {}) \
                    .get(context.LABEL_PREVENT_DELETION) == "True":  # noqa: E501
                continue
            ref = kclient.V1ObjectReference(
                name=metadata["name"],
                namespace=metadata["namespace"],
            )
            if run_state == ResourceRunState.DELETION_REQUIRED:
                to_delete.append(ref)
                continue

            spec_state_not_up_to_date = (
                spec_state != ResourceSpecState.UP_TO_DATE
            )
            run_state_not_ok = (
                run_state != ResourceRunState.READY and
                run_state != ResourceRunState.STARTING
            )
            run_state_not_ready = run_state != ResourceRunState.READY
            run_state_not_unknown = run_state != ResourceRunState.UNKNOWN
            scored_refs.append(
                (
                    (
                        spec_state_not_up_to_date,
                        run_state_not_ok,
                        run_state_not_ready,
                        run_state_not_unknown,
                        metadata["creationTimestamp"],
                    ),
                    WinnerInfo(ref, run_state, spec_state,
                               metadata.get("creationTimestamp"),
                               recreation_reason_count),
                ),
            )

        scored_refs.sort(key=lambda x: x[0])
        if not scored_refs:
            return None, to_delete

        to_delete.extend(ref for _, (ref, _, _, _, _) in scored_refs[1:])
        return scored_refs[0][1], to_delete

    @start_as_current_span_async(tracer, "flag_instead_of_delete")
    async def flag_instead_of_delete(self, ctx: context.Context,
                                     name: str, namespace: str) -> None:
        v1 = kclient.CoreV1Api(ctx.api_client)
        body = [{"op": "add",
                 "path": jsonpatch.JsonPointer.from_parts([
                     "metadata",
                     "labels",
                     context.LABEL_PREVENT_DELETION]).path,
                 "value": "True"}]
        await v1.patch_node(name, body)

        intf = self.get_resource_interface(ctx)
        await intf.patch(namespace, name, body)
        await customresource.update_status(
            intf,
            namespace,
            name,
            conditions=[
                customresource.ConditionUpdate(
                    type_="RequiresRecreation",
                    status="True",
                    reason="",
                    message=f"Resource {self.component} may "
                            "not be deleted and "
                            "requires manual recreation",
                ),
            ]
        )

    @start_as_current_span_async(tracer, "_compile_full_state")
    async def _compile_full_state(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            target_instances: typing.Mapping[str, typing.Any],
            assignment: typing.Mapping[str, typing.Collection[ResourceInfo]],
            ) -> typing.Tuple[
                typing.Mapping[str, InstanceState],
                typing.Collection[kclient.V1ObjectReference],
            ]:
        to_delete: typing.List[kclient.V1ObjectReference] = []

        instance_keys = target_instances.keys()
        assignment_keys = set(assignment)

        v1 = kclient.CoreV1Api(ctx.api_client)
        ydb_interface = interfaces.yaookdisruptionbudget_interface(
            ctx.api_client
        )

        for wrong_instance in assignment_keys - instance_keys:
            prevent_deletion = False
            resource_info = list(assignment[wrong_instance])[0]
            node_name = self._get_node_name(resource_info)
            node = await v1.read_node(node_name)
            node_labels = node.metadata.labels
            namespace = list(
                assignment[wrong_instance]
            )[0].metadata["namespace"]

            disruption_budgets = await ydb_interface.list_(namespace)
            for disruption_budget in disruption_budgets:
                node_is_in_disruption_budget = (
                    api_utils.matches_node_selector(
                        disruption_budget["spec"]["nodeSelectors"],
                        node_labels)
                )

                if (node_is_in_disruption_budget and
                        disruption_budget["spec"].get(
                            "preventDeletion", False)):
                    prevent_deletion = True

            if prevent_deletion and self.allow_prevent_deletion:
                await self.flag_instead_of_delete(ctx, node_name, namespace)
            else:
                to_delete.extend(
                    kclient.V1ObjectReference(
                        namespace=instance_info.metadata["namespace"],
                        name=instance_info.metadata["name"],
                    )
                    for instance_info in assignment[wrong_instance]
                )

        full_state = {}
        for instance, instance_data in target_instances.items():
            instance_ctx = ctx.with_instance(
                instance,
                instance_data
            )
            instance_assignment = assignment.get(instance, [])
            intended_body = await self._make_complete_body(
                instance_ctx,
                dependencies,
            )
            winner, new_to_delete = self._select_winner(
                instance_ctx,
                intended_body,
                instance_assignment,
            )
            state = InstanceState(
                winner=winner,
                intended_body=intended_body,
                instance_data=instance_data,
                has_any_resources=bool(instance_assignment),
                may_create_resources=True,
            )
            to_delete.extend(new_to_delete)

            full_state[instance] = state

        return full_state, to_delete

    async def _update_stats(
            self,
            ctx: context.Context,
            groups: typing.List[InstanceGroup],
            ) -> None:
        for group in groups:
            if not group.stats:  # We did not have any instance in this group
                continue
            ctx.logger.info(
                "For group %s: "
                "%d target instances, %d current instances with %d resources. "
                "%d/%d unavailable, %d/%d ready, "
                "%d/%d up to date, %d newly created, %d deleted, "
                "offline migration %s, spare_nodes %s",
                group.name,
                group.stats.nconfigured,
                group.stats.nexisting_instances,
                group.stats.nexisting_resources,
                group.stats.nconfigured - group.stats.navailable,
                group.get_max_unavailable(group.stats.nconfigured),
                group.stats.nready,
                group.stats.nconfigured,
                group.stats.nupdated,
                group.stats.nconfigured,
                group.stats.ncreated,
                group.stats.ndeleted,
                group.get_disruptive_maintenance(),
                ",".join(group.stats.spare_nodes),
            )

    @start_as_current_span_async(tracer, "reconcile")
    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        """
        Reconcile the state of the managed resources carefully.

        :param ctx: The context of the operation
        :param dependencies: The dependencies of the operation

        .. note::

            This method does not use :meth:`~.SingleObject.reconcile` of
            the wrapped state!
        """
        assignment, leftovers = await self._get_current_instances(ctx)
        target_instances = await self.get_target_instances(ctx)
        intf = self._wrapped_state.get_resource_interface(ctx)

        to_delete = [
            kclient.V1ObjectReference(
                namespace=resource_info.metadata["namespace"],
                name=resource_info.metadata["name"],
            )
            for resource_info in leftovers
        ]
        to_flag = []
        full_state, new_to_delete = await self._compile_full_state(
            ctx,
            dependencies,
            target_instances,
            assignment,
        )
        to_delete.extend(new_to_delete)
        del new_to_delete

        grouped_instances = await self._group_instances(ctx, full_state)
        for group, instances in grouped_instances:
            currently_unavailable = sum(
                0 if instance_state.is_available else 1
                for instance_state in instances.values()
            )
            nexisting_resources = sum(
                len(resources)
                for instance, resources in assignment.items()
                if instance in instances
            )
            nexisting_instances = sum(
                1 if instance in instances else 0
                for instance in assignment
            )
            current_spare_nodes = [
                instance
                for instance, values in assignment.items()
                if values[0].body.get("spec", {}).get("isSpareNode", False) is True
            ]
            nconfigured = len(instances)
            navailable = 0
            nready = 0
            nupdated = 0
            new_to_delete = []
            new_to_flag = []

            max_unavailable = group.get_max_unavailable(nconfigured)
            created_counter = 0

            for instance, instance_state in _sort_instances(instances):
                instance_tuple = (instance, instance_state)
                ctx.logger.debug(
                    "instance %s: has_any_resources: %r; winner: %r",
                    instance_tuple,
                    instance_state.has_any_resources,
                    instance_state.winner,
                )

                if instance_state.winner is not None:
                    # update the counters
                    run_state = instance_state.winner.run_state
                    if run_state == ResourceRunState.STARTING:
                        navailable += 1
                    elif run_state == ResourceRunState.READY:
                        navailable += 1
                        nready += 1

                    if instance_state.is_up_to_date:
                        nupdated += 1

                if (instance_state.winner is not None and
                        not instance_state.is_up_to_date):
                    if currently_unavailable < max_unavailable:
                        ctx.logger.debug(
                            "instance %s: is not up-to-date and we have "
                            "budget left (%d/%d unavailable), allowing "
                            "recreation and deleting %s/%s to trigger update",
                            instance,
                            currently_unavailable,
                            max_unavailable,
                            instance_state.winner.ref.namespace,
                            instance_state.winner.ref.name,
                        )
                        if group.get_prevent_deletion() \
                                and self.allow_prevent_deletion:
                            new_to_flag.append(instance_state.winner.ref)
                        else:
                            new_to_delete.append(
                                instance_state.winner.ref,
                            )
                        run_state = instance_state.winner.run_state
                        if instance_state.is_available:
                            currently_unavailable += 1
                        if run_state == ResourceRunState.STARTING:
                            navailable -= 1
                        elif run_state == ResourceRunState.READY:
                            navailable -= 1
                            nready -= 1
                    else:
                        ctx.logger.debug(
                            "instance %s: is not up-to-date, but recreation "
                            "is currently not allowed (%d/%d unavailable)",
                            instance,
                            currently_unavailable,
                            max_unavailable,
                        )

                    continue

                if not instance_state.can_create_new_resource:
                    continue

                ctx.logger.debug(
                    "instance %s: has no resources, creating a new one",
                    instance,
                )

                if group.spare_nodes > len(current_spare_nodes):
                    instance_state.intended_body["spec"]["isSpareNode"] = True
                    current_spare_nodes.append(instance)

                if group.name != "DEFAULT" and \
                        instance_state.intended_body.get("kind") == \
                        "NovaComputeNode":
                    instance_state.intended_body["spec"]["appliedDisruptionBudget"] = \
                        group.name

                await intf.create(
                    ctx.namespace,
                    instance_state.intended_body,
                    field_manager=ctx.field_manager,
                )

                created_counter += 1

            group.stats = InstanceStatistics(
                nconfigured=nconfigured,
                nexisting_instances=nexisting_instances,
                nexisting_resources=nexisting_resources,
                nupdated=nupdated,
                nready=nready,
                navailable=navailable,
                ncreated=created_counter,
                ndeleted=len(new_to_delete)+len(new_to_flag),
                spare_nodes=current_spare_nodes,
            )
            to_delete.extend(new_to_delete)
            del new_to_delete
            to_flag.extend(new_to_flag)
            del new_to_flag

        await asyncio.gather(*(intf.delete(ref.namespace, ref.name)
                               for ref in to_delete),
                             return_exceptions=True,)

        await asyncio.gather(
            *(self.flag_instead_of_delete(ctx, ref.name, ref.namespace)
                for ref in to_flag),
            return_exceptions=True,
        )

        await self._update_stats(
            ctx,
            [group for group, _ in grouped_instances],
        )


class StatefulPerNode(PerNodeMixin, StatefulInstancedResource[T]):

    def __init__(
            self,
            ignore_grouping_labels: typing.List = [],
            *args: typing.Any,
            **kwargs: typing.Any,
            ) -> None:
        super().__init__(*args, **kwargs)
        self.ignore_grouping_labels = ignore_grouping_labels

    @start_as_current_span_async(tracer, "_group_instances")
    async def _group_instances(
            self,
            ctx: context.Context,
            instances: typing.Mapping[str, InstanceState]
            ) -> typing.List[typing.Tuple[
                InstanceGroup, typing.Mapping[str, InstanceState]]]:
        ydb_interface = interfaces.yaookdisruptionbudget_interface(
            ctx.api_client)
        disruption_budgets = await ydb_interface.list_(ctx.namespace)
        groups: typing.List[typing.Tuple[
            InstanceGroup, typing.Mapping[str, InstanceState]]] = []
        observed_instances: typing.Dict[str, str] = {}
        ignored_instances: typing.Dict[str, str] = {}
        field_manager = "%s-%s" % (ctx.parent_kind, self.component)

        # check for ignored instances
        if len(self.ignore_grouping_labels) > 0:
            for instance, instance_state in instances.items():
                node_labels = typing.cast(typing.Mapping[str, str],
                                          instance_state.instance_data)
                for ignore_label in self.ignore_grouping_labels:
                    if ignore_label in node_labels.keys():
                        ignored_instances[instance] = "ignored-ydb"

        for disruption_budget in disruption_budgets:
            name = disruption_budget["metadata"]["name"]
            namespace = disruption_budget["metadata"]["namespace"]
            max_unavailable = disruption_budget["spec"]["maxUnavailable"]
            prevent_deletion = disruption_budget["spec"].get(
                "preventDeletion", False)
            spare_nodes = disruption_budget["spec"].get("spareNodes", {}).get(
                "amount", 0)
            group = InstanceGroup(
                kclient.V1ObjectReference(name=name, namespace=namespace),
                max_unavailable,
                None, field_manager,
                prevent_deletion=prevent_deletion,
                spare_nodes=spare_nodes,
            )
            group_instances = {}

            for instance, instance_state in instances.items():
                node_labels = typing.cast(typing.Mapping[str, str],
                                          instance_state.instance_data)
                if instance in ignored_instances:
                    continue
                if api_utils.matches_node_selector(
                        disruption_budget["spec"]["nodeSelectors"],
                        node_labels):
                    if instance in observed_instances:
                        raise exceptions.ConfigurationInvalid(
                            "The node %s is already present in "
                            "YaookDisruptionBudget %s, but would now also be "
                            "part of %s. This is not allowed" %
                            (instance, observed_instances[instance], name)
                        )
                    group_instances[instance] = instance_state
                    observed_instances[instance] = name

            groups.append((group, group_instances))

        # ignored ydb - 100% unavailable
        groups.append(
            (
                InstanceGroup(None, "100%", None, field_manager),
                {
                    k: v
                    for k, v in instances.items()
                    if k in ignored_instances.keys()
                },
            )
        )

        leftover_instances = (
            instances.keys()
            - observed_instances.keys()
            - ignored_instances.keys()
        )
        # default group
        groups.append(
            (
                InstanceGroup(None, "1", None, field_manager),
                {
                    k: v
                    for k, v in instances.items()
                    if k in leftover_instances
                },
            )
        )

        return groups

    async def _update_stats(
            self,
            ctx: context.Context,
            groups: typing.List[InstanceGroup],
            ) -> None:
        await super()._update_stats(ctx, groups)

        ydb_interface = interfaces.yaookdisruptionbudget_interface(
            ctx.api_client)
        patch: typing.Union[bytes, typing.Mapping, typing.List]

        for group in groups:
            if group.ref is None:
                continue
            if group.stats is None or group.stats.no_resource_exists():
                curr = await ydb_interface.read_status(
                    group.ref.namespace, group.ref.name)
                if any([
                        x["type"] == group.field_manager
                        for x in curr.get("status", {}).get("nodes", [])]):
                    if len(curr.get("status", {}).get("nodes", [])) > 1:
                        patch = _encode_body({
                            "apiVersion": ydb_interface.api_version,
                            "kind": "YaookDisruptionBudget",
                            "status": {}
                        })
                    # purge the whole status as this is the last element
                    else:
                        patch = [{
                            "op": "remove",
                            "path": "/status"
                        }]
                    await ydb_interface.patch_status(
                        group.ref.namespace,
                        group.ref.name,
                        patch,
                        field_manager=group.field_manager
                    )
            else:
                node = {
                    "type": group.field_manager,
                    "lastUpdateTime":
                        api_utils.format_timestamp(
                            datetime.utcnow()),
                    "configuredInstances":
                        group.stats.nconfigured,
                    "existingInstances":
                        group.stats.nexisting_instances,
                    "updatedInstances": group.stats.nupdated,
                    "readyInstances": group.stats.nready,
                    "availableInstances": group.stats.navailable,
                }
                if len(group.stats.spare_nodes) > 0:
                    node["spareNodes"] = group.stats.spare_nodes
                body = {
                        "apiVersion": ydb_interface.api_version,
                        "kind": "YaookDisruptionBudget",
                        "status": {
                            "nodes": [node]
                        }
                    }
                await ydb_interface.patch_status(
                    group.ref.namespace,
                    group.ref.name,
                    _encode_body(body),
                    field_manager=group.field_manager,
                )
