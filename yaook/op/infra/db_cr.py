#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import secrets
import typing
import semver

import kubernetes_asyncio.client as kclient

import yaook.common.config
import yaook.op.common
import yaook.op.scheduling_keys
import yaook.statemachine as sm
from yaook.statemachine import version_utils
from yaook.statemachine import interfaces
import yaook.statemachine.resources as resources
import yaook.statemachine.registry

from . import resources as infra_resources

JOB_SCHEDULING_KEYS = [
    yaook.op.scheduling_keys.SchedulingKey.INFRA_DATABASE.value,
    yaook.op.scheduling_keys.SchedulingKey.ANY_INFRA.value,
]


def generate_mariadb_compliant_password():
    # mariadb will choke at >32 characters
    return secrets.token_urlsafe(24)


class VersionDependentWsrepDefaults(sm.CueLayer):
    async def get_layer(self, ctx: sm.context.Context) -> sm.cue.CueInput:
        next_release = version_utils.get_next_release(ctx)
        next_ver = semver.VersionInfo.parse(f"{next_release}.0")
        config: typing.List[typing.Mapping[str, typing.Any]] = []
        # 'wsrep_replicate_myisam' is deprecated since MariaDB version 10.6
        # and got replaced by 'wsrep_mode=REPLICATE_MYISAM'
        if next_ver.compare("10.6.0") >= 0:
            config.append(
                {
                    "galera": {
                        "wsrep_mode": "REPLICATE_MYISAM",
                    },
                },
            )
        else:
            config.append(
                {
                    "galera": {
                        "wsrep_replicate_myisam": True,
                    },
                },
            )
        return {"mariadb": yaook.common.config.MYSQL_INI.declare(config)}


class IpFamilyConfig(sm.CueLayer):
    def __init__(
            self,
            *,
            service: resources.KubernetesReference[kclient.V1Service],
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._declare_dependencies(service)
        self._service = service

    async def get_layer(self, ctx: sm.context.Context) -> sm.cue.CueInput:
        svc_interface = interfaces.service_interface(api_client=ctx.api_client)
        svc_ref = await self._service.get(ctx)
        svc = await svc_interface.read(ctx.namespace, svc_ref.name)
        has_ipv6 = False
        if svc.spec.ip_families is not None:
            has_ipv6 = "IPv6" in svc.spec.ip_families

        config: typing.List[typing.Mapping[str, typing.Any]] = []
        if has_ipv6:
            # if we do support IPv6, we make ourselves dual-stack capable.
            config.append(
                {
                    "sst": {
                        "sockopt": ",pf=ip6",
                    }
                }
            )

        next_release = version_utils.get_next_release(ctx)
        next_ver = semver.VersionInfo.parse(f"{next_release}.0")
        if next_ver.compare("10.11.0") >= 0:
            # here, we can have a comma-separated list of bind-addresses
            config.append(
                {
                    "mysqld": {
                        "bind_address": "::,0.0.0.0",
                    }
                }
            )
        elif next_ver.compare("10.6.0") >= 0:
            # with >= 10.6.0 and < 10.11.0, we are screwed: there is no way
            # to actually have a dual-stack service
            # we thus prefer IPv6 if both are set, otherwise we listen on
            # IPv4.
            if has_ipv6:
                config.append(
                    {
                        "mysqld": {
                            "bind_address": "::",
                        }
                    }
                )
            else:
                config.append(
                    {
                        "mysqld": {
                            "bind_address": "0.0.0.0",
                        }
                    }
                )
        else:
            # before 10.6.0, :: implied both address families
            config.append(
                {
                    "mysqld": {
                        "bind_address": "::",
                    }
                }
            )

        return {"mariadb": yaook.common.config.MYSQL_INI.declare(config)}


class StatefulSet(sm.TemplatedStatefulSet):
    def __init__(
            self,
            *,
            replication_certificate: sm.Certificate,
            frontend_certificate: sm.Certificate,
            exporter_certificate: sm.Certificate,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.replication_certificate = replication_certificate
        self.frontend_certificate = frontend_certificate
        self.exporter_certificate = exporter_certificate

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: resources.DependencyMap,
            ) -> resources.TemplateParameters:
        # We must never ever change the cluster name as then the nodes can no
        # longer connect to each other
        cluster_name = None
        try:
            current = await self._get_current(ctx)
            for container in current.spec.template.spec.containers:
                if container.env is None:
                    continue
                for env in container.env:
                    if env.name == "MARIADB_GALERA_CLUSTER_NAME":
                        cluster_name = env.value
            if cluster_name is None:
                raise ValueError("Could not find MARIADB_GALERA_CLUSTER_NAME "
                                 "env var in container, this is basically "
                                 "impossible")
        except sm.ResourceNotPresent:
            cluster_name = ctx.parent_name

        param = await super()._get_template_parameters(ctx, dependencies)
        param["vars"]["replication_cert_not_before"] = (
            await self.replication_certificate._get_current(ctx)
            )["status"]["notBefore"]
        param["vars"]["frontend_cert_not_before"] = (
            await self.frontend_certificate._get_current(ctx)
            )["status"]["notBefore"]
        param["vars"]["exporter_cert_not_before"] = (
            await self.exporter_certificate._get_current(ctx)
            )["status"]["notBefore"]
        param["vars"]["cluster_name"] = cluster_name
        return param


class AutogeneratedDBPasswords(sm.Secret):
    KEYS = [
        "mariadb-root-password",
        "mariadb-galera-mariabackup-password",
        "mariadb-password",
    ]

    def __init__(self,
                 *,
                 metadata: sm.MetadataProvider,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._metadata = metadata

    async def _make_body(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> sm.ResourceBody:
        try:
            current = await self._get_current(ctx)
            data = {
                key: base64.b64decode(
                    current.data[key].encode("ascii")
                ).decode("ascii")
                for key in self.KEYS
            }
        except sm.ResourceNotPresent:
            data = {
                key: generate_mariadb_compliant_password()
                for key in self.KEYS
            }

        data["databaseName"] = ctx.parent_spec["database"]
        data = {
            key: base64.b64encode(
                value.encode("ascii")
            ).decode("ascii")
            for key, value in data.items()
        }
        return {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": sm.evaluate_metadata(ctx, self._metadata),
            "data": data,
        }

    def _needs_update(self, current, new):
        return current.data != new["data"]


class ExporterSecret(infra_resources.RawTemplateSecret):
    def __init__(self,
                 *,
                 db_passwords: AutogeneratedDBPasswords,
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._db_passwords = db_passwords
        self._declare_dependencies(db_passwords)

    async def _get_template_parameters(
            self,
            ctx: sm.Context,
            dependencies: sm.DependencyMap) -> resources.TemplateParameters:
        params = await super()._get_template_parameters(ctx, dependencies)
        passwords = await self._db_passwords._get_current(ctx)
        params["vars"]["username"] = infra_resources.MAINTENANCE_USER
        params["vars"]["root_password"] = base64.b64decode(
                    passwords.data["mariadb-root-password"].encode("ascii")
                ).decode("ascii")
        return params


class MySQLService(sm.ReleaseAwareCustomResource):
    API_GROUP = "infra.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "mysqlservices"
    KIND = "MySQLService"
    RELEASES = [
        "10.2",
        "10.3",
        "10.4",
        "10.5",
        "10.6",
        "10.11",
        "11.0",
        "11.4",
    ]
    VALID_UPGRADE_TARGETS = [
        "10.3",
        "10.4",
        "10.5",
        "10.6",
        "10.11",
        "11.0",
        "11.4",
    ]
    UPGRADE_STRATEGY = sm.UpgradeStrategy.MULTIPLE_RELEASE

    mariadb_image = sm.ReleaseAwareVersionedDependency(
        {
            release: sm.BitnamiVersionedDockerImage(
                "bitnami/mariadb-galera",
                release=release,
            )
            for release in RELEASES
        },
        targetfn=lambda ctx: version_utils.get_next_release(ctx),
    )

    mariadb_upgrade = sm.MappedVersionedDependency(
        {
            **{release: "mysql_upgrade" for release in ["10.2", "10.3"]},
            **{
                release: "mariadb-upgrade"
                for release in ["10.4", "10.5", "10.6", "10.11", "11.0", "11.4"]
            },
        },
        targetfn=lambda ctx: version_utils.get_next_release(ctx),
    )

    backup_creator_image = sm.ConfigurableVersionedDockerImage(
        'backup-creator',
        sm.YaookSemVerSelector(),
    )
    backup_shifter_image = sm.ConfigurableVersionedDockerImage(
        'backup-shifter',
        sm.YaookSemVerSelector(),
    )
    ssl_terminator_image = sm.ConfigurableVersionedDockerImage(
        'ssl-terminator',
        sm.YaookSemVerSelector(),
    )

    mysqld_exporter_image = sm.VersionedDockerImage(
        'prom/mysqld-exporter',
        sm.SemVerSelector(prefix="v"),
    )

    unready_service = sm.TemplatedService(
        template="db-headless-service.yaml",
        params={
            "suffix": "bare",
            "database_component": "database",
            "publish_unready": True,
        },
    )

    ready_service = sm.TemplatedService(
        template="db-headless-service.yaml",
        params={
            "suffix": "rdy",
            "database_component": "database",
            "publish_unready": False,
        },
    )

    passwords = AutogeneratedDBPasswords(
        metadata=lambda ctx: f"{ctx.parent_name}-db-creds",
        component=infra_resources.MYSQL_DATABASE_COMPONENT_PASSWORDS,
    )

    mysql_exporter_config_secret = ExporterSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-exporter-cfg-", True),
        copy_on_write=True,
        add_dependencies=[ready_service],
        db_passwords=passwords,
        template_map={
            "mysql-exporter.yaml": "db-exporter.yaml",
            "my.cnf": "db-exporter-my.cnf",
        },
    )

    frontend_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-frontend-", True),
    )

    frontend_certificate = sm.TemplatedCertificate(
        template="db-frontend-certificate.yaml",
        add_dependencies=[frontend_certificate_secret],
    )

    ready_frontend_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=frontend_certificate,
    )

    exporter_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-exporter-", True),
    )

    exporter_certificate = sm.TemplatedCertificate(
        template="db-exporter-certificate.yaml",
        add_dependencies=[exporter_certificate_secret, unready_service],
    )

    ready_exporter_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=exporter_certificate,
    )

    replication_ca_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-galeraca-", True),
    )

    replication_ca_certificate = sm.TemplatedCertificate(
        template="db-replication-ca-certificate.yaml",
        add_dependencies=[replication_ca_certificate_secret],
    )

    ready_replication_ca_certificate_secret =\
        sm.ReadyCertificateSecretReference(
            certificate_reference=replication_ca_certificate,
        )

    replication_ca = sm.TemplatedIssuer(
        template="db-ca.yaml",
        add_dependencies=[ready_replication_ca_certificate_secret],
    )

    replication_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-galera-", True),
    )

    replication_certificate = sm.TemplatedCertificate(
        template="db-replication-certificate.yaml",
        add_dependencies=[replication_certificate_secret, replication_ca,
                          unready_service],
    )

    ready_replication_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=replication_certificate,
    )

    ca_certs = sm.CAConfigMap(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-ca-certificates-", True),
        usercerts_spec_key="caCertificates",
        certificate_secrets_states=[
            ready_frontend_certificate_secret,
            ready_replication_certificate_secret,
        ],
    )

    mysql_service_monitor = sm.GeneratedStatefulsetServiceMonitor(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-service-monitor-", True),
        service=unready_service,
        certificate=ready_exporter_certificate_secret,
        endpoints=["prometheus"],
    )

    mysql_backup_service_monitor = sm.GeneratedServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-db-backup-service-monitor-",
            True),
        service=unready_service,
        certificate=ready_exporter_certificate_secret,
        endpoints=["backup-metrics"],
    )

    db_config = sm.CueConfigMap(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-cfg-", True),
        add_cue_layers=[
            sm.SpecLayer(
                target="mariadb",
                accessor="mysqlConfig",
                flavor=yaook.common.config.MYSQL_INI,
            ),
            VersionDependentWsrepDefaults(),
            IpFamilyConfig(
                # which service we use doesn't really matter---what is
                # important is that it is a service which has been created by
                # the time the config is rendered, so that we can learn the
                # cluster's IP address family preferences.
                service=unready_service,
            ),
        ],
        copy_on_write=True,
    )

    database = StatefulSet(
        template="db-statefulset.yaml",
        replication_certificate=replication_certificate,
        frontend_certificate=frontend_certificate,
        exporter_certificate=exporter_certificate,
        add_dependencies=[
            db_config,
            unready_service,
            ready_replication_certificate_secret,
            ready_frontend_certificate_secret,
            ready_exporter_certificate_secret,
            passwords,
            ca_certs,
            mysql_exporter_config_secret,
        ],
        scheduling_keys=JOB_SCHEDULING_KEYS,
        tolerate_node_down=lambda ctx: ctx.parent_spec.get(
            "tolerateNodeDown", False),
        params={
            "mariadb_root_username": infra_resources.MAINTENANCE_USER,
        },
        versioned_dependencies=[
            ssl_terminator_image,
            mariadb_image,
            mariadb_upgrade,
            backup_creator_image,
            backup_shifter_image,
            mysqld_exporter_image,
        ],
    )

    database_pdb = sm.QuorumPodDisruptionBudget(
         metadata=lambda ctx: f"{ctx.parent_name}-db-pdb",
         replicated=database,
    )

    database_pod_services = sm.PerStatefulSetPod(
        statefulset=database,
        wrapped_state=sm.TemplatedService(
            template="db-headless-per-pod-service.yaml",
            params={
                "database_component": "database",
            },
        ),
    )

    haproxy_config = infra_resources.HaproxyTemplateConfigMap(
        metadata=lambda ctx: (f"{ctx.parent_name}-prx-cfg-", True),
        copy_on_write=True,
        database_pod_services=database_pod_services,
        template_map={
            "haproxy.cfg": "db-haproxy.cfg",
        },
    )

    haproxy_metrics_certificate_secret = sm.EmptyTlsSecret(
        metadata=lambda ctx: (f"{ctx.parent_name}-db-prx-metrics-", True),
    )

    haproxy_metrics_certificate = sm.TemplatedCertificate(
        template="db-haproxy-metrics-certificate.yaml",
        add_dependencies=[haproxy_metrics_certificate_secret],
    )

    ready_haproxy_metrics_certificate_secret = \
        sm.ReadyCertificateSecretReference(
            certificate_reference=haproxy_metrics_certificate,
        )

    service_reload_image = sm.ConfigurableVersionedDockerImage(
        'service-reload',
        sm.YaookSemVerSelector(),
    )

    haproxy_image = sm.VersionedDockerImage(
        'library/haproxy',
        sm.SemVerSelector(suffix="-bookworm"),
    )

    haproxy = sm.TemplatedDeployment(
        scheduling_keys=[
            yaook.op.scheduling_keys.SchedulingKey.ANY_INFRA.value,
            yaook.op.scheduling_keys.SchedulingKey.INFRA_DATABASE.value,
        ],
        template="db-haproxy.yaml",
        add_dependencies=[
            haproxy_config,
            ready_haproxy_metrics_certificate_secret,
            ],
        versioned_dependencies=[
            haproxy_image,
            service_reload_image,
        ],
    )

    haproxy_pdb = sm.QuorumPodDisruptionBudget(
        metadata=lambda ctx: f"{ctx.parent_name}-prx-pdb",
        replicated=haproxy,
    )

    # can be orphaned after 2024-11-28
    # in combination clean up the frontend certificate template
    old_public_service = sm.TemplatedService(
        component=yaook.op.common.OLD_MYSQL_DATABASE_SERVICE_COMPONENT,
        template="db-service.yaml",
        add_dependencies=[haproxy],
        params={"suffix": ""}
    )

    public_service = sm.TemplatedService(
        component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
        template="db-service.yaml",
        add_dependencies=[haproxy],
        params={"suffix": "-db-frontend"}
    )

    haproxy_service_monitor = sm.GeneratedStatefulsetServiceMonitor(
        metadata=lambda ctx: (
            f"{ctx.parent_name}-db-haproxy-service-monitor-",
            True),
        service=public_service,
        certificate=ready_haproxy_metrics_certificate_secret,
        endpoints=["prometheus"],
    )

    def __init__(self, **kwargs):
        super().__init__(
            assemble_sm=True,
            **kwargs
        )

    async def get_unversioned_installed_release(
        self, ctx: sm.context.Context
    ) -> str:
        statefulsets = interfaces.stateful_set_interface(ctx.api_client)
        try:
            first_sts, *_ = await statefulsets.list_(
                ctx.namespace,
                label_selector={
                    sm.context.LABEL_PARENT_NAME: ctx.parent_name
                },
            )
        except ValueError:
            # If we do not have a StatefulSet it is a fresh deployment
            # in which case we can deploy the targetRelease directly.
            return version_utils.get_target_release(ctx)
        else:
            # Get a reference to the StatefulSet and check the currently
            # installed version
            container, *_ = filter(
                lambda container: container.name == "mariadb-galera",
                first_sts.spec.template.spec.containers
            )
            mariadb_version, _ = sm.BitnamiVersion().parse(
                container.image.split(":")[1]
            )
            return f"{mariadb_version.major}.{mariadb_version.minor}"


yaook.statemachine.registry.register(MySQLService)


class MySQLUserServiceReference(sm.ReadyForeignReference[typing.Mapping]):
    def __init__(self, **kwargs: typing.Any):
        super().__init__(
            resource_interface_factory=sm.mysqlservice_interface,
            **kwargs,
        )

    def get_resource_references(
            self,
            ctx: sm.Context,
            ) -> typing.Collection[kclient.V1ObjectReference]:
        return [
            kclient.V1ObjectReference(
                namespace=ctx.namespace,
                name=ctx.parent_spec["serviceRef"]["name"],
            )
        ]


class MySQLUser(sm.CustomResource):
    API_GROUP = "infra.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "mysqlusers"
    KIND = "MySQLUser"

    database_ref = MySQLUserServiceReference()

    database_service = sm.ForeignResourceDependency(
        resource_interface_factory=sm.service_interface,
        foreign_resource=database_ref,
        foreign_component=yaook.op.common.MYSQL_DATABASE_SERVICE_COMPONENT,
    )

    database_credentials = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=database_ref,
        foreign_component=infra_resources.MYSQL_DATABASE_COMPONENT_PASSWORDS,
    )

    database_certificate = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=database_ref,
        foreign_component="frontend_certificate_secret",
    )

    optional_secret_ref = infra_resources.DynamicSecretHandler()

    user = infra_resources.MySQLUser(
        finalizer="infra.yaook.cloud/cleanup-mysql-user",
        database_service=database_service,
        database_credentials=database_credentials,
        tls_secret=database_certificate,
        password_secret=optional_secret_ref,
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)


yaook.statemachine.registry.register(MySQLUser)
