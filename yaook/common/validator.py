#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import typing
import os
import requests
import logging
import yaml
import json
from oslo_config import cfg
from oslo_policy import policy, _checks


KUBE_CERT = '/var/run/secrets/kubernetes.io/serviceaccount/ca.crt'
LOG_DATEFMT = "%Y-%m-%d %H:%M:%S"
LOG_FORMAT = "%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s"
logging.basicConfig(format=LOG_FORMAT, datefmt=LOG_DATEFMT)
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


class PolicyInvalid(Exception):
    """General exception raised for policy invalid."""
    def __init__(self, msg):
        super().__init__(msg)


def build_policy_configmap(
    policies: str,
    additional_policy: typing.Optional[str] = None,
    defaults_filename: str = "/default_policy/policy.yaml",
) -> typing.Optional[typing.Mapping[str, str]]:

    try:
        valid_policies = validate_policies(policies, defaults_filename,
                                           additional_policy)
    # Who needs common base classes :)
    except (policy.PolicyNotAuthorized, policy.InvalidScope,
            policy.DuplicatePolicyError, policy.PolicyNotRegistered,
            policy.InvalidDefinitionError, policy.InvalidRuleDefault,
            policy.InvalidContextObject) as e:
        raise PolicyInvalid(e)

    # Use the result from `validate_policies` which also includes the defaults.
    return valid_policies


def validate_policies(
    unvalidated: str,
    defaults_filename: str,
    additional_unvalidated: typing.Optional[str] = None,
) -> typing.Optional[typing.Mapping[str, str]]:
    """Validate the unvalidated policies together with the default policies.

    :param unvalidated: policies to validate
    :param default_policy_filename: name of file containing default policies

    :return: The validated policies without the defaults or None
    """
    defaults = get_default_policies(defaults_filename)
    # if we dont specify any policies, we just return None
    policies = json.loads(unvalidated)
    additional_policy = json.loads(additional_unvalidated) if \
        additional_unvalidated else {}

    if len(policies) == 0 and len(additional_policy) == 0:
        return None

    enforcer = policy.Enforcer(
        cfg.CONF,
        policy_file=defaults_filename,
        use_conf=False,
    )

    policies = extend_policies(policies, additional_policy, dict(defaults))

    # all_unvalidated_dict is to contain all rules in defaults (with possibly
    # modified values) together with the rules only defined in policies.
    all_unvalidated_dict = dict(defaults)
    all_unvalidated_dict.update(policies)

    # Check that no invalid rules are defined.
    all_unvalidated_rules = policy.Rules.from_dict(all_unvalidated_dict)
    _validate_policy_keys(all_unvalidated_rules, defaults)

    # Check that the rules are non-cyclic and that all referenced
    # rules are defined.
    enforcer.set_rules(all_unvalidated_rules)
    enforcer.check_rules(raise_on_violation=True)

    # The enforcer.check_rules and the parsing of the rules that
    # Rules.from_dict() performs are not enough to validate the provided
    # policy.
    # For example, the rule value "dummy" cannot be parsed in Rules.from_dict()
    # which results in the rule having the value _checks.FalseCheck(). This is
    # however not considered invalid. We want to be more conservative and
    # disallow such values. Therefore we validate the policy values again.
    _validate_policy_values(all_unvalidated_dict, all_unvalidated_rules)
    # If you specify policies in the policy.yaml the service will enforce these
    # policies, even if they match the defaults. But because they get enforced
    # the old deprecated (no scope) policies will not be logically OR'ed with
    # the new defaults. So we just add the specified policies that do not
    # match the default, to enable a hopefully interrupt free upgrade of the
    # operator
    result = {}
    for key, val in policies.items():
        result.update({str(key): str(all_unvalidated_dict.get(key))})
    return result


def extend_policies(policy: typing.Dict[str, str],
                    additional_rules: typing.Mapping[str, str],
                    defaults: typing.Mapping[str, str] = {}
                    ) -> typing.Mapping[str, str]:
    """
    Extend the given policy with additional rules take into account the default
    policies.

    This function takes a policy, additional rules, and optional defaults as
    input. It iterates over the additional rules and updates the policy
    accordingly. If a key in the additional rules already exists in the policy,
    the value is appended to the existing value using the logical OR operator.
    If a key is not present in the policy but exists in the defaults, the value
    is appended to the existing value from the defaults using the logical OR
    operator. If a key is not present in both the policy and defaults, the
    value is added directly to the policy.

    Parameters:
    - policy (typing.Mapping[str, str]): The original policy to be extended.
    - additional_rules (typing.Mapping[str, str]): The additional rules to be
      added to the policy.
    - defaults (typing.Mapping[str, str], optional): The default values to be
      used if a key is not present in the policy. Defaults to an empty
      dictionary.

    Returns:
    - typing.Mapping[str, str]: The extended policy with additional rules and
      defaults.
    """
    for key, value in additional_rules.items():
        if key in policy.keys():
            policy[key] = f"({policy[key]}) or {value}"
        elif key in defaults.keys():
            policy[key] = f"({defaults[key]}) or {value}"
        else:
            policy[key] = value
    return policy


def get_default_policies(file_path: str) -> typing.Mapping[str, str]:
    """Returns the policies in the provided file.

    Raises ValueError if the file_path is invalid or the file
    does not contain any policies, since the default policy file
    must not be empty.

    :param file_path: path to file containing the default policies
    :return: the default policies
    :raises: FileNotFoundError
    """
    with open(file_path) as f:
        default_policies_str = f.read()
        default_policies = policy.parse_file_contents(default_policies_str)
        if not default_policies:
            msg = (
                f"The default policy file '{file_path}' must not be empty."
                "If you use locally pinned versions please add the default "
                f"policies for the service under '{file_path}'"
            )
            raise ValueError(msg)
        return default_policies


def _validate_policy_values(
    unvalidated_dict: typing.Mapping[str, str],
    unvalidated_rules: policy.Rules
) -> None:
    for rule in unvalidated_rules:
        check = unvalidated_rules.get(rule, _checks.FalseCheck)
        if isinstance(check, _checks.FalseCheck):
            # The value of the rule 'rule' is either malformed or
            # correctly specified as a rule which 'never allows', i.e., as '!'
            # (the string representation of oslo_policy._checks.FalseCheck()).
            # Therefore we raise unless the rule was explicitly given as '!'.
            if unvalidated_dict[rule] != str(_checks.FalseCheck()):
                raise policy.InvalidDefinitionError([rule])


def _validate_policy_keys(
    all_unvalidated_rules: policy.Rules,
    defaults: typing.Mapping[str, str]
) -> None:
    """Makes sure that no rule key name in all_unvalidated_rules, but not in
    defaults, is unused, i.e., never mentioned in any of the rule values."""
    non_defaults = {
        rule_name
        for rule_name in all_unvalidated_rules
        if rule_name not in defaults
    }

    all_rule_checks = {
        rule_name: _get_rule_checks(check)
        for rule_name, check in all_unvalidated_rules.items()
    }
    used_policy_rules = {
        check.match
        for using_rule_name, rule_checks in all_rule_checks.items()
        for check in rule_checks
    }
    unused_policy_rules = []
    for rule_name in non_defaults:
        if rule_name not in used_policy_rules:
            unused_policy_rules.append(rule_name)

    if unused_policy_rules:
        raise policy.PolicyNotRegistered(unused_policy_rules)


def _get_rule_checks(check: policy.Check) -> typing.List[policy.RuleCheck]:
    if isinstance(check, policy.RuleCheck):
        return [check]
    rule_checks = []
    rule = getattr(check, 'rule', None)
    if rule:
        rule_checks += _get_rule_checks(rule)
    rules = getattr(check, 'rules', None)
    if rules:
        for c in rules:
            rule_checks += _get_rule_checks(c)
    return rule_checks


def read_kube_config():
    global KUBE_HOST, KUBE_TOKEN
    KUBE_HOST = "https://{}:{}".format(
        'kubernetes.default',
        os.environ['KUBERNETES_SERVICE_PORT'],
    )
    with open('/var/run/secrets/kubernetes.io/serviceaccount/token', 'r') as f:
        KUBE_TOKEN = f.read()


def get_configmap_definition(name, namespace):
    url = f'{KUBE_HOST}/api/v1/namespaces/{namespace}/configmaps/{name}'
    resp = requests.get(url,
                        headers={'Authorization': f'Bearer {KUBE_TOKEN}'},
                        verify=KUBE_CERT)
    if resp.status_code != 200:
        LOG.error('Cannot get configmap %s.', name)
        LOG.error(resp.text)
        return None
    return resp.json()


def update_configmap(name, secret, namespace):
    url = f'{KUBE_HOST}/api/v1/namespaces/{namespace}/configmaps/{name}'
    resp = requests.put(url,
                        json=secret,
                        headers={'Authorization': f'Bearer {KUBE_TOKEN}'},
                        verify=KUBE_CERT)
    if resp.status_code != 200:
        LOG.error('Cannot update configmap %s.', name)
        LOG.error(resp.text)
        return False
    return True


def main():
    NAMESPACE = os.environ['KUBERNETES_NAMESPACE']
    CONFIGMAP_NAME = os.environ['CONFIGMAP']
    POLICIES = os.environ['POLICY']
    ADDITIONAL_POLICIES = os.environ.get('ADDITIONAL_POLICY', None)

    LOG.info(f"Got following inputs from deployment:\n{POLICIES}")
    LOG.info("Got following additional inputs from deployment:\n"
             f"{ADDITIONAL_POLICIES}")

    # merge and validate policies
    LOG.info("Building policies")
    policy = build_policy_configmap(POLICIES, ADDITIONAL_POLICIES)

    # update configmap
    LOG.info("Getting kube config")
    read_kube_config()

    LOG.info(f"Getting configmap with name: {CONFIGMAP_NAME}")
    cm = get_configmap_definition(CONFIGMAP_NAME, NAMESPACE)

    if policy is not None:
        # width=999999 to avoid line wrapping in the generated yaml
        cm['data'] = {"policy.yaml": yaml.dump(policy, width=999999)}
    else:
        cm['data'] = {"policy.yaml": ""}

    LOG.info("Updating configmap with policies")
    if not update_configmap(CONFIGMAP_NAME, cm, NAMESPACE):
        sys.exit(1)

    sys.exit(0)


if __name__ == "__main__":
    main()
