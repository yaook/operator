#
# Copyright (c) 2024 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from typing import Callable

from tests import testutils
from tests.testutils import CustomResourceTestCase
from yaook.op.scheduling_keys import SchedulingKey
import yaook.op.common as common
from yaook.statemachine import context, interfaces
import yaook.statemachine as sm


# region Database
async def test_creates_database_and_user_async(
    self: CustomResourceTestCase, namespace: str
):
    self._make_all_databases_ready_immediately()

    await self.cr.sm.ensure(self.ctx)

    dbs = interfaces.mysqlservice_interface(self.api_client)
    dbusers = interfaces.mysqluser_interface(self.api_client)
    secrets = interfaces.secret_interface(self.api_client)

    db, = await dbs.list_(namespace)
    api_user, = await dbusers.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "db_api_user",
        }
    )
    api_user_password, = await secrets.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "db_api_user_password",
        }
    )

    self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                     db["metadata"]["name"])
    self.assertEqual(
        api_user["spec"]["passwordSecretKeyRef"]["name"],
        api_user_password.metadata.name,
    )

    self.assertEqual(db["spec"]["replicas"], 1)
    self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
    self.assertEqual(db["spec"]["proxy"]["timeoutClient"], 300)
    self.assertEqual(db["spec"]["storageSize"], "8Gi")


def get_default_database_config():
    return {
        "replicas": 1,
        "timeoutClient": 300,
        "proxy": {
            "replicas": 1,
            "resources":
                testutils.generate_db_proxy_resources(),
        },
        "backup": {
            "schedule": "0 * * * *"
        },
        "storageSize": "8Gi",
        "resources": testutils.generate_db_resources(),
    }
# endregion Database


# region Message Queue
async def test_creates_message_queue_and_user(
    self: CustomResourceTestCase, namespace: str,
    user_label: str, pw_label: str
):
    self._make_all_mqs_succeed_immediately()

    await self.cr.sm.ensure(self.ctx)

    mqs = interfaces.amqpserver_interface(self.api_client)
    mqusers = interfaces.amqpuser_interface(self.api_client)
    secrets = interfaces.secret_interface(self.api_client)

    mq, = await mqs.list_(namespace)
    api_user, = await mqusers.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: user_label,
        }
    )
    api_user_password, = await secrets.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: pw_label,
        }
    )

    self.assertEqual(api_user["spec"]["serverRef"]["name"],
                     mq["metadata"]["name"])
    self.assertEqual(
        api_user["spec"]["passwordSecretKeyRef"]["name"],
        api_user_password.metadata.name,
    )
    self.assertEqual(mq["spec"]["replicas"], 1)


async def test_creates_config_with_transport_url(
    self: CustomResourceTestCase, namespace: str,
    config_file_name: str
):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    secrets = interfaces.secret_interface(self.api_client)
    services = interfaces.service_interface(self.api_client)
    config, = await secrets.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "config",
        },
    )
    mq_user_password_secret, = await secrets.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "mq_api_user_password",
        },
    )
    mq_service, = await services.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: common.AMQP_SERVER_SERVICE_COMPONENT,
            context.LABEL_PARENT_PLURAL: "amqpservers",
        },
    )
    mq_user_password = await sm.extract_password(
        self.ctx,
        mq_user_password_secret.metadata.name,
    )
    barbican_conf = config.data[config_file_name]
    cfg = testutils._parse_config(barbican_conf, decode=True)

    self.assertEqual(
        cfg.get("DEFAULT", "transport_url"),
        f"rabbit://api:{mq_user_password}@"
        f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
    )


async def test_amqp_server_frontendIssuer_name(
    self: CustomResourceTestCase, namespace: str
):
    self._make_all_databases_ready_immediately()
    self._make_all_mqs_succeed_immediately()
    await self.cr.sm.ensure(self.ctx)

    amqpserver = interfaces.amqpserver_interface(self.api_client)

    amqp, = await amqpserver.list_(
        namespace,
    )

    self.assertEqual(
        "issuername",
        amqp["spec"]["frontendIssuerRef"]["name"],
    )


async def test_creates_memcached(self: CustomResourceTestCase, namespace: str):
    await self.cr.sm.ensure(self.ctx)

    memcacheds = interfaces.memcachedservice_interface(self.api_client)
    memcached, = await memcacheds.list_(namespace)

    self.assertEqual(
        memcached["spec"]["memory"],
        "512"
    )
    self.assertEqual(
        memcached["spec"]["connections"],
        "2000"
    )


def get_default_message_queue_config():
    return {
        "replicas": 1,
        "storageSize": "2Gi",
        "storageClassName": "bar-class",
        "resources": testutils.generate_amqp_resources(),
    }
# endregion Message Queue


# region Certificates
async def test_creates_certificate_and_halts_async(
        self: CustomResourceTestCase, namespace: str):
    await self.cr.sm.ensure(self.ctx)
    self.api_client.client_side_validation = False
    cert_interface = interfaces.certificates_interface(self.api_client)

    all_certs = await cert_interface.list_(namespace)
    self.assertEqual(len(all_certs), 1)
    cert, = all_certs

    self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                     "certificate")

    await self.cr.sm.ensure(self.ctx)

    all_certs = await cert_interface.list_(namespace)
    self.assertEqual(len(all_certs), 1)
    cert, = all_certs

    self.assertEqual(cert["metadata"]["labels"][context.LABEL_COMPONENT],
                     "certificate")


async def test_certificate_contains_service_name_async(
          self: CustomResourceTestCase,
          label_component_value: str, namespace: str):
    self._make_all_dependencies_complete_immediately()

    await self.cr.sm.ensure(self.ctx)

    certificates = interfaces.certificates_interface(self.api_client)
    services = interfaces.service_interface(self.api_client)

    service, = await services.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: label_component_value,
        },
    )

    certificate, = await certificates.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "certificate",
        },
    )

    self.assertIn(
        f"{service.metadata.name}.{namespace}.svc",
        certificate["spec"]["dnsNames"],
    )


async def test_certificate_contains_issuer_name_async(
          self: CustomResourceTestCase, namespace: str):
    self._make_all_dependencies_complete_immediately()

    await self.cr.sm.ensure(self.ctx)

    certificates = interfaces.certificates_interface(self.api_client)

    certificate, = await certificates.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "certificate",
        },
    )

    self.assertEqual(
        "issuername",
        certificate["spec"]["issuerRef"]["name"],
    )
# endregion Certificates


# region Deployments
async def test_creates_deployment_with_replicas_when_all_jobs_succeed(
        self: CustomResourceTestCase, label_component_value: str,
        namespace: str, replica_count: int
):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)
    deployments = interfaces.deployment_interface(self.api_client)
    deployment, = await deployments.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: label_component_value,
        },
    )
    self.assertEqual(deployment.spec.replicas, replica_count)
# endregion Deployments


# region Services
async def test_creates_service_for_deployment_async(
    self: CustomResourceTestCase,
    namespace: str,
    deployment_label_component_value: str,
    service_label_component_value: str
):
    self._make_all_dependencies_complete_immediately()

    await self.cr.sm.ensure(self.ctx)

    deployments = interfaces.deployment_interface(self.api_client)
    deployment, = await deployments.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: deployment_label_component_value,
        },
    )
    services = interfaces.service_interface(self.api_client)

    service, = await services.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: service_label_component_value,
        },
    )

    self.assertFalse(
        service.spec.publish_not_ready_addresses,
    )
    self.assertDictEqual(

        service.spec.selector,
        deployment.spec.template.metadata.labels,
    )
# endregion Services


# region Ingress
async def test_creates_endpoint_with_ingress(
    self: CustomResourceTestCase,
    namespace: str,
    api_endpoint_name: str,
    url: str
):
    await self.cr.sm.ensure(self.ctx)

    endpoints = interfaces.keystoneendpoint_interface(self.api_client)
    endpoint = await endpoints.read(namespace, api_endpoint_name)  # ?

    self.assertEqual(url, endpoint["spec"]["endpoints"]["public"])


async def test_creates_ingress(
    self: CustomResourceTestCase, namespace: str,
    ingress_name: str, host: str
):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    ingresses = interfaces.ingress_interface(self.api_client)
    ingress = await ingresses.read(namespace, ingress_name)

    self.assertEqual(
        ingress.spec.rules[0].host,
        host
    )


async def test_disable_ingress_creation(
    self: CustomResourceTestCase,
    namespace: str,
    cls: type,
    get_yaml: Callable
):
    deployment_yaml = get_yaml()
    deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = False
    self._configure_cr(cls, deployment_yaml)
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    ingress_int = interfaces.ingress_interface(self.api_client)
    ingresses = await ingress_int.list_(
        namespace,
        label_selector={context.LABEL_COMPONENT: "api_ingress"},
    )
    self.assertEqual(len(ingresses), 0)


async def test_enable_ingress_creation(
    self: CustomResourceTestCase,
    namespace: str,
    cls: type,
    get_yaml: Callable
):
    deployment_yaml = get_yaml()
    deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = True
    self._configure_cr(cls, deployment_yaml)
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    ingress_int = interfaces.ingress_interface(self.api_client)
    ingresses = await ingress_int.list_(
        namespace,
        label_selector={context.LABEL_COMPONENT: "api_ingress"},
    )
    self.assertEqual(len(ingresses), 1)


async def test_ingress_force_ssl_annotation(
    self: CustomResourceTestCase,
    namepace: str, ingress_name: str
):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    ingresses = interfaces.ingress_interface(self.api_client)
    ingress = await ingresses.read(namepace, ingress_name)

    self.assertEqual(
        ingress
        .metadata
        .annotations["nginx.ingress.kubernetes.io/force-ssl-redirect"],
        "true",
    )


async def test_ingress_matches_service(
        self: CustomResourceTestCase,
        namespace: str,
        ingress_name: str,
        svc_name: str
):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    ingresses = interfaces.ingress_interface(self.api_client)
    ingress = await ingresses.read(namespace, ingress_name)

    services = interfaces.service_interface(self.api_client)
    service = await services.read(namespace, svc_name)

    self.assertEqual(
        ingress.spec.rules[0].http.paths[0].backend.service.name,
        service.metadata.name,
    )
    self.assertEqual(
        ingress.spec.rules[0].http.paths[0].backend.service.port.number,
        ([x.port for x in service.spec.ports if x.name == "external"][0]),
    )
# endregion Ingress


# region Openstack Config
async def test_creates_config_with_database_uri(
        self: CustomResourceTestCase,
        namespace: str,
        config_file_name: str,
        section: str = "DEFAULT",
        option: str = "sql_connection"
        ):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    secrets = interfaces.secret_interface(self.api_client)
    services = interfaces.service_interface(self.api_client)
    mysqlusers = interfaces.mysqluser_interface(self.api_client)
    mysqlservices = interfaces.mysqlservice_interface(self.api_client)
    config, = await secrets.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "config",
        },
    )
    db_user_password_secret, = await secrets.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "db_api_user_password",
        },
    )
    db_service, = await services.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: common.MYSQL_DATABASE_SERVICE_COMPONENT,
            context.LABEL_PARENT_PLURAL: "mysqlservices",
        },
    )
    db_user_password = await sm.extract_password(
        self.ctx,
        db_user_password_secret.metadata.name,
    )
    db_user, = await mysqlusers.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "db_api_user",
        },
    )
    db, = await mysqlservices.list_(
        namespace,
        label_selector={
            context.LABEL_COMPONENT: "db",
        },
    )
    db_name = db["spec"]["database"]
    conf = config.data[config_file_name]
    cfg = testutils._parse_config(conf, decode=True)

    self.assertEqual(
        cfg.get(section, option),
        f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
        f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
        f"{db_name}?charset=utf8&ssl_ca=/etc/pki/tls/certs/ca-bundle.crt",
    )
# endregion Openstack Config


# region Scheduling Keys
async def test_applies_scheduling_key_to_deployment(
        self: CustomResourceTestCase,
        namespace: str,
        component: str,
        scheduling_keys: list[SchedulingKey]):

    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    deployments = sm.deployment_interface(self.api_client)

    deployment, = await deployments.list_(
        namespace,
        label_selector={
            sm.context.LABEL_COMPONENT: component,
        },
    )

    self.assertEqual(
        deployment.spec.template.spec.affinity.node_affinity.
        required_during_scheduling_ignored_during_execution.to_dict(),
        {
            "node_selector_terms": [
                {
                    "match_expressions": [
                        {
                            "key": key,
                            "operator": "Exists",
                            "values": None,
                        },
                    ],
                    "match_fields": None,
                } for key in scheduling_keys
            ],
        },
    )


async def test_applies_scheduling_key_to_job(
        self: CustomResourceTestCase,
        namespace: str,
        component: str,
        scheduling_keys: list[SchedulingKey]
        ):
    self._make_all_dependencies_complete_immediately()
    await self.cr.sm.ensure(self.ctx)

    job_interface = interfaces.job_interface(self.api_client)
    job, = await job_interface.list_(
        namespace,
        label_selector={
            sm.context.LABEL_COMPONENT: component,
        }
    )

    self.assertEqual(
        job.spec.template.spec.affinity.node_affinity.
        required_during_scheduling_ignored_during_execution.to_dict(),
        {
            "node_selector_terms": [
                {
                    "match_expressions": [
                        {
                            "key": key,
                            "operator": "Exists",
                            "values": None,
                        },
                    ],
                    "match_fields": None,
                } for key in scheduling_keys
            ],
        },
    )

    self.assertCountEqual(
        job.spec.template.spec.to_dict()["tolerations"],
        [
            {
                "key": key,
                "operator": "Exists",
                "effect": None,
                "toleration_seconds": None,
                "value": None,
            } for key in scheduling_keys
        ],
    )

# endregion Scheduling Keys
