#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
from unittest.mock import sentinel
import uuid

import yaook.statemachine.context as context
import yaook.statemachine.version_utils as version_utils


def _get_context(parent):
    intf = unittest.mock.Mock([])
    intf.plural = "testobjects"
    ctx = context.Context(
        namespace="test-namespace",
        parent=parent,
        parent_intf=intf,
        instance=None,
        instance_data=None,
        api_client=sentinel.api_client,
        logger=sentinel.logger,
        field_manager=sentinel.field_manager,
    )
    return ctx


class TestGetInstalledRelease(unittest.TestCase):
    def test_extracts_installed_release(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": sentinel.parent_spec,
                "status": {
                    "installedRelease": sentinel.installed_release
                }
            })
        self.assertEqual(
            sentinel.installed_release,
            version_utils.get_installed_release(ctx))

    def test_none_on_no_release(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": sentinel.parent_spec,
                "status": {
                }
            })
        self.assertEqual(None, version_utils.get_installed_release(ctx))

    def test_none_on_no_status(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": sentinel.parent_spec,
            })
        self.assertEqual(None, version_utils.get_installed_release(ctx))


class TestGetTargetRelease(unittest.TestCase):
    def test_extracts_target_release(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": {
                    "targetRelease": sentinel.target_release
                }
            })
        self.assertEqual(
            sentinel.target_release,
            version_utils.get_target_release(ctx))

    def test_raises_for_no_release(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": {}
            })
        self.assertRaises(
            KeyError,
            version_utils.get_target_release,
            ctx)


class TestisUpgrade(unittest.TestCase):
    def testt_detects_update_new(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": {
                    "targetRelease": sentinel.target_release
                }
            })
        self.assertFalse(version_utils.is_upgrading(ctx))

    def test_detects_update_current(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": {
                    "targetRelease": sentinel.target_release,
                },
                "status": {
                    "installedRelease": sentinel.target_release,
                }
            })
        self.assertFalse(version_utils.is_upgrading(ctx))

    def test_detects_update_different(self):
        ctx = _get_context({
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "test-object",
                    "uid": str(uuid.uuid4()),
                },
                "spec": {
                    "targetRelease": sentinel.target_release,
                },
                "status": {
                    "installedRelease": sentinel.installed_release,
                }
            })
        self.assertTrue(version_utils.is_upgrading(ctx))
