##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ dependencies['bgp_service'].resource_name() }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  serviceName: {{ dependencies['bgp_service'].resource_name() }}
  selector:
    matchLabels: {{ labels }}
  template:
    metadata:
      labels: {{ labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      tolerations: {{ params["tolerations"] }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchFields:
              - key: metadata.name
                operator: In
                values:
                # The name of the NeutronOVNBGPAgent resource must always be the
                # name of the Kubernetes node it runs on. So we use that here
                # to schedule the pod.
                # A bit of a hack, admittedly, but covered by tests, sooo...
                - {{ crd_spec.hostname }}
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                "network.yaook.cloud/provides-l2": "true"
            topologyKey: kubernetes.io/hostname
      dnsPolicy: ClusterFirstWithHostNet
      initContainers:
        - name: ovn-bgp-init
          image:  {{ versioned_dependencies['ovn_bgp_agent_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["/add-bgp-interface.sh"]
          securityContext:
            privileged: true
          volumeMounts:
            - name: bridge-interface-mapping
              mountPath: /etc/ovn-bgp-agent/bridge_interface_mapping
              subPath: bridge-interface-mapping
            - name: host-proc
              mountPath: /host/proc
              mountPropagation: Bidirectional
            - name: run-openvswitch
              mountPath: /run/openvswitch
          env:
            - name: DEBUG
              value: "1"
      containers:
        - name: "ovn-bgp-agent"
          image:  {{ versioned_dependencies['ovn_bgp_agent_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["ovn-bgp-agent"]
          args:
            - --config-file
            - /etc/ovn-bgp-agent/ovn-bgp-agent.conf
          securityContext:
            privileged: true
          volumeMounts:
            - name: ovn-bgp-config
              mountPath: /etc/ovn-bgp-agent/ovn-bgp-agent.conf
              subPath: ovn-bgp-agent.conf
            - name: frr-config
              mountPath: /etc/frr/frr.conf
              subPath: frr.conf
            - name: tls-secret
              mountPath: /etc/ssl/private
            - name: run-openvswitch
              mountPath: /run/openvswitch
            - name: frr-run
              mountPath: /var/run/frr
            - name: bridge-interface-mapping
              mountPath: /etc/ovn-bgp-agent/bridge_interface_mapping
              subPath: bridge-interface-mapping
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          resources: {{ crd_spec | resources('ovn-bgp-agent') }}
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - ps aux | grep privsep-helper | grep /etc/ovn-bgp-agent/ovn-bgp-agent.conf
            timeoutSeconds: 5
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - ps aux | grep privsep-helper | grep /etc/ovn-bgp-agent/ovn-bgp-agent.conf
            timeoutSeconds: 5
          lifecycle:
            preStop:
              exec:
                command:
                  - /bin/bash
                  - -ec
                  - /remove-bgp-interface.sh
        - name: "frr-bgpd"
          image:  {{ versioned_dependencies['ovn_bgp_agent_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["/start_bgpd.sh"]
          securityContext:
            privileged: true
          volumeMounts:
            - name: frr-config
              mountPath: /etc/frr/frr.conf
              subPath: frr.conf
            - name: frr-run
              mountPath: /var/run/frr
            - name: bridge-interface-mapping
              mountPath: /etc/ovn-bgp-agent/bridge_interface_mapping
              subPath: bridge-interface-mapping
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
          resources: {{ crd_spec | resources('frr-bgpd') }}
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - kill -0 $(pgrep bgpd) 2>/dev/null
            timeoutSeconds: 5
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - vtysh -c "show bgp neighbors" | grep "BGP state = Established"
            timeoutSeconds: 5
        - name: "frr-zebra"
          image:  {{ versioned_dependencies['ovn_bgp_agent_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["/start_zebra.sh"]
          securityContext:
            privileged: true
          volumeMounts:
            - name: frr-config
              mountPath: /etc/frr/frr.conf
              subPath: frr.conf
            - name: frr-run
              mountPath: /var/run/frr
          env:
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
          resources: {{ crd_spec | resources('frr-zebra') }}
          livenessProbe:
            exec:
              command:
              - sh
              - -c
              - kill -0 $(pgrep zebra) 2>/dev/null
            timeoutSeconds: 5
          readinessProbe:
            exec:
              command:
              - sh
              - -c
              - kill -0 $(pgrep zebra) 2>/dev/null
            timeoutSeconds: 5
        - name: "frr-exporter"
          image:  {{ versioned_dependencies['ovn_bgp_agent_image'] }}
          imagePullPolicy: IfNotPresent
          command: ["prometheus-frr-exporter"]
          args:
            - --no-collector.bfd
            - --no-collector.ospf
            - --web.listen-address=:9342
          volumeMounts:
            - name: frr-run
              mountPath: /var/run/frr
          resources: {{ crd_spec | resources('frr-exporter') }}
        - name: ssl-terminator-metrics
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: IfNotPresent
          env:
            - name: SERVICE_PORT
              value: "9337"
            - name: METRICS_PORT
              value: "8002"
            - name: LOCAL_PORT
              value: "9342"
          ports:
            - name: metrics
              containerPort: 8002
              protocol: TCP
            - name: bgp-metrics
              containerPort: 9337
              protocol: TCP
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8002
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8002
              scheme: HTTPS
          resources: {{ crd_spec | resources('ssl-terminator-metrics') }}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('service-reload') }}
      volumes:
        - name: bridge-interface-mapping
          projected:
            sources:
            - configMap:
                name: {{ dependencies['bgp_config'].resource_name() }}
                items:
                - key: bridge-interface-mapping
                  path: bridge-interface-mapping
        - name: frr-config
          configMap:
            name: {{ dependencies['bgp_config'].resource_name() }}
            items:
            - key: frr.conf
              path: frr.conf
        - name: ssl-terminator-config
          emptyDir: {}
        - name: frr-run
          emptyDir: {}
        - name: host-proc
          hostPath:
            path: /proc
        - name: ovn-bgp-config
          projected:
            sources:
            - configMap:
                name: {{ dependencies['bgp_config'].resource_name() }}
                items:
                - key: ovn-bgp-agent.conf
                  path: ovn-bgp-agent.conf
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
        - name: run-openvswitch
          hostPath:
            path: /run/openvswitch
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
