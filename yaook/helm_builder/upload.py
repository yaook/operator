import pathlib

import yaook.helm_builder.helmutils
import yaook.helm_builder.cli


if __name__ == "__main__":
    import argparse
    import sys

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--dry-run", action="store_true", default=False)
    parser.add_argument("chart", type=pathlib.Path)

    args = parser.parse_args()

    repo_path = yaook.helm_builder.cli.get_ci_repo_path()
    auth = yaook.helm_builder.cli.get_ci_auth()

    if not args.chart.name.endswith(".tgz"):
        raise ValueError("chart names must end in .tgz")

    if args.dry_run:
        print(
            f"dry-run: would upload {args.chart} to {repo_path} as "
            f"{auth.split(':')[0]}",
            file=sys.stderr,
        )
        sys.exit(0)

    yaook.helm_builder.helmutils.upload_helm_chart(
        args.chart,
        args.chart.name,
        repo_path,
        auth,
    )
