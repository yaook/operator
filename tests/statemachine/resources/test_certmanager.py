#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import contextlib
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.watcher as watcher

import yaook.statemachine.resources.certmanager as certmanager
import yaook.statemachine.resources.k8s as k8s


class TestEmptyTLSSecret(unittest.IsolatedAsyncioTestCase):
    async def test__make_body_contains_requried_fields(self):
        cm = certmanager.EmptyTlsSecret(
            metadata="emtpy-secret"
        )
        result = await cm._make_body(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.deps,
        )
        self.assertEqual(result["type"], "kubernetes.io/tls")
        self.assertIn("tls.crt", result["data"])
        self.assertIn("tls.key", result["data"])

    async def test__needs_update_never_true(self):
        cm = certmanager.EmptyTlsSecret(
            metadata="emtpy-secret"
        )
        result = cm._needs_update(
            unittest.mock.sentinel.current,
            unittest.mock.sentinel.new
        )
        self.assertFalse(result)


@ddt.ddt
class TestReadyCertificateSecretReference(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.certificate_ref = unittest.mock.Mock(
            k8s.KubernetesReference
        )
        self.rcsr = certmanager.ReadyCertificateSecretReference(
            certificate_reference=self.certificate_ref,
        )

    async def test_get_extracts_secret_name_from_certificate(self):
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            intf = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.certificates_interface",
            ))
            intf.return_value.read = unittest.mock.AsyncMock()
            intf.return_value.read.return_value = {
                "spec": {
                    "secretName": sentinel.secret_name,
                }
            }

            self.certificate_ref.get.return_value = kclient.V1ObjectReference(
                namespace=sentinel.namespace,
                name=sentinel.cert_name,
            )

            ref = await self.rcsr.get(ctx)

        self.certificate_ref.get.assert_awaited_once_with(ctx)

        intf.assert_called_once_with(ctx.api_client)
        intf.return_value.read.assert_awaited_once_with(
            sentinel.namespace,
            sentinel.cert_name,
        )

        self.assertEqual(
            ref.namespace,
            sentinel.namespace,
        )

        self.assertEqual(
            ref.name,
            sentinel.secret_name,
        )

    async def test_get_all_returns_single_reference(self):
        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.rcsr, "get",
            ))
            get.return_value = sentinel.ref

            refs = await self.rcsr.get_all(sentinel.ctx)

        get.assert_awaited_once_with(sentinel.ctx)

        self.assertDictEqual(
            refs,
            {
                None: sentinel.ref,
            },
        )

    async def test_reconcile_returns_false(self):
        self.assertFalse(
            await self.rcsr.reconcile(sentinel.ctx, sentinel.deps),
        )

    @ddt.data(
        {},
        {"tls.key": "foo", "tls.crt": "bar"},
        {"ca.crt": "foo", "tls.crt": "bar"},
        {"ca.crt": "foo", "tls.key": "bar"},
    )
    async def test_is_ready_returns_false_if_keys_are_missing(
            self,
            secret_data):
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.rcsr, "get",
            ))
            get.return_value = kclient.V1ObjectReference(
                namespace=sentinel.namespace,
                name=sentinel.name,
            )

            intf = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.secret_interface",
            ))
            intf.return_value.read = unittest.mock.AsyncMock()
            intf.return_value.read.return_value = kclient.V1Secret(
                data=secret_data,
            )

            result = await self.rcsr.is_ready(ctx)

        self.assertFalse(result)

    @ddt.data(
        {"tls.key": "foo", "tls.crt": "bar", "ca.crt": ""},
        {"ca.crt": "foo", "tls.crt": "bar", "tls.key": ""},
        {"ca.crt": "foo", "tls.key": "bar", "tls.crt": ""},
    )
    async def test_is_ready_returns_false_if_keys_are_empty(
            self,
            secret_data):
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.rcsr, "get",
            ))
            get.return_value = kclient.V1ObjectReference(
                namespace=sentinel.namespace,
                name=sentinel.name,
            )

            intf = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.secret_interface",
            ))
            intf.return_value.read = unittest.mock.AsyncMock()
            intf.return_value.read.return_value = kclient.V1Secret(
                data=secret_data,
            )

            result = await self.rcsr.is_ready(ctx)

        self.assertFalse(result)

    async def test_is_ready_returns_true_if_all_keys_are_populated(self):
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.rcsr, "get",
            ))
            get.return_value = kclient.V1ObjectReference(
                namespace=sentinel.namespace,
                name=sentinel.name,
            )

            intf = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.secret_interface",
            ))
            intf.return_value.read = unittest.mock.AsyncMock()
            intf.return_value.read.return_value = kclient.V1Secret(
                data={
                    "tls.crt": "foo",
                    "tls.key": "bar",
                    "ca.crt": "baz",
                },
            )

            result = await self.rcsr.is_ready(ctx)

        self.assertTrue(result)

    def test_declares_certificate_reference_as_dependency(self):
        self.assertIn(
            self.certificate_ref,
            self.rcsr._dependencies,
        )

    async def test_delete_is_most_likely_noop(self):
        await self.rcsr.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.rcsr.cleanup_orphans(sentinel.ctx, sentinel.protect)


class TestCertificate(unittest.IsolatedAsyncioTestCase):
    class CertificateStateTest(certmanager.Certificate):
        async def _make_body(self, ctx, deps):
            raise NotImplementedError

    def setUp(self):
        self.cs = self.CertificateStateTest()
        self.ctx = unittest.mock.Mock()

    def test__handle_certificate_event_triggers_on_delete(self):
        event = watcher.StatefulWatchEvent(
            watcher.EventType.DELETED,
            None,
            None,
            None,
            None,
            None,
            )
        self.assertTrue(self.cs._handle_certificate_event(self.ctx, event))

    def test__handle_certificate_event_triggers_on_ready_added(self):
        event = watcher.StatefulWatchEvent(
            watcher.EventType.ADDED,
            None,
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ]
                }
            },
            None,
            None,
        )
        self.assertTrue(self.cs._handle_certificate_event(self.ctx, event))

    def test__handle_certificate_event_triggers_on_ready_changed(self):
        event = watcher.StatefulWatchEvent(
            watcher.EventType.MODIFIED,
            None,
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ]
                }
            },
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "False",
                        }
                    ]
                }
            },
        )
        self.assertTrue(self.cs._handle_certificate_event(self.ctx, event))

    def test__handle_certificate_event_triggers_on_date_changed(self):
        event = watcher.StatefulWatchEvent(
            watcher.EventType.MODIFIED,
            None,
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ],
                    "notBefore": "new",
                }
            },
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ],
                    "notBefore": "old",
                }
            },
        )
        self.assertTrue(self.cs._handle_certificate_event(self.ctx, event))

    def test__handle_certificate_event_no_trigger_nothing_changed(self):
        event = watcher.StatefulWatchEvent(
            watcher.EventType.MODIFIED,
            None,
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ],
                    "notBefore": "new",
                }
            },
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ],
                    "notBefore": "new",
                }
            },
        )
        self.assertFalse(self.cs._handle_certificate_event(self.ctx, event))

    def test__handle_certificate_event_no_trigger_if_not_ready(self):
        event = watcher.StatefulWatchEvent(
            watcher.EventType.MODIFIED,
            None,
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "False",
                        }
                    ],
                    "notBefore": "new",
                }
            },
            None,
            {
                "status": {
                    "conditions": [
                        {
                            "type": "Ready",
                            "status": "True",
                        }
                    ],
                    "notBefore": "old",
                }
            },
        )
        self.assertFalse(self.cs._handle_certificate_event(self.ctx, event))
