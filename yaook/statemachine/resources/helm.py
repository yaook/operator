#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.helm` – Objects helm releases
#######################################################

.. autosummary::

    HelmRelease

.. autoclass:: HelmRelease

"""  # noqa
import abc
import asyncio
import contextlib
import dataclasses
import json
import re
import subprocess  # nosemgrep there are no useful helm bindings unfortunately
import tempfile
import typing

from .. import context
from .base import (
    DependencyMap,
    ResourceBody,
)
from .external import ExternalResource


@dataclasses.dataclass(frozen=True)
class HelmReleaseInfo:
    chart: str
    namespace: str
    name: str
    version: typing.Optional[str]
    values: ResourceBody


@contextlib.contextmanager
def temporary_json_file(
        data: typing.Mapping,
        ) -> typing.Iterator[str]:
    with tempfile.NamedTemporaryFile(mode="wt") as f:
        json.dump(data, f)
        f.flush()
        yield f.name


_TO_ESCAPE_S_RX = re.compile(
    "|".join(map(re.escape, map(chr, range(0x00, 0x20)))) + "|\"|\\\\"
)
_SIMPLE_ESCAPES = {
    "\n": "n",
    "\t": "t",
    "'": "'",
    '"': '"',
    "\\": "\\",
}


def escape_helm_string(value: str) -> str:
    """
    Encode the given string as a helm template string.
    """
    parts = ['"']
    previous_end = 0
    for match in _TO_ESCAPE_S_RX.finditer(value):
        start, end = match.span()
        parts.append(value[previous_end:start])
        matched_substr = value[start:end]
        sequence = _SIMPLE_ESCAPES.get(
            matched_substr,
            "x{:02x}".format(ord(matched_substr)),
        )
        parts.append("\\")
        parts.append(sequence)
        previous_end = end
    parts.append(value[previous_end:])
    parts.append('"')
    return "".join(parts)


def as_helm_template(value: str) -> str:
    """
    Return a helm template which evaluates to the value given.
    """
    escaped = escape_helm_string(value)
    return "{{ print " f"{escaped}" " }}"


@contextlib.asynccontextmanager
async def managed_subprocess(
        program: str,
        *args: str,
        **kwargs: typing.Any,
        ) -> typing.AsyncGenerator[asyncio.subprocess.Process, typing.Any]:
    proc = await asyncio.create_subprocess_exec(program, *args, **kwargs)
    try:
        yield proc
    finally:
        if proc.returncode is None:
            proc.kill()
            await proc.wait()


async def check_communicate(
        proc: asyncio.subprocess.Process,
        argv: typing.Sequence[str]) -> typing.Tuple[bytes, bytes]:
    stdout, stderr = await proc.communicate()
    assert proc.returncode is not None  # nosemgrep this is just a type hint
    if proc.returncode != 0:
        raise subprocess.CalledProcessError(
            proc.returncode,
            argv,
            output=stdout,
            stderr=stderr,
        )
    return stdout, stderr


async def helm_upgrade(info: HelmReleaseInfo) -> None:
    async with contextlib.AsyncExitStack() as stack:
        fname = stack.enter_context(temporary_json_file(info.values))
        argv = [
            "helm", "upgrade", "--install",
            "--values", fname,
            "--namespace", info.namespace,
        ]
        if info.version is not None:
            argv.extend([
                "--version", info.version,
            ])
        argv.extend([
            info.name, info.chart,
        ])

        proc = await stack.enter_async_context(managed_subprocess(*argv))
        await check_communicate(proc, argv)


async def helm_delete(namespace: str, name: str) -> None:
    argv = [
        "helm", "delete",
        "--namespace", namespace,
        name,
    ]

    async with managed_subprocess(*argv) as proc:
        await check_communicate(proc, argv)


class HelmRelease(ExternalResource):
    """
    Manage a `helm <https://helm.sh>`_ release.

    This is an abstract external resource base class to manage a helm release
    inside the kubernetes cluster. For methods which need to be implemented by
    subclasses, see below.

    While Helm also manages Kubernetes resources, we do not have direct
    access to those. Hence, this is managed as an external resource with a
    finalizer.

    .. warning::

        Use of helm releases in an operator is discouraged. It is preferable to
        port helm charts to operators.

    .. note::

        On every iteration of the operator, `helm upgrade` will be called. This
        may or may not be what you want, and it may even be dangerous depending
        on the helm chart used.

    .. note::

        This depends on having the ``helm`` binary in the search path.

    **Interface for subclasses**:

    .. automethod:: _get_helm_release
    """

    @abc.abstractmethod
    async def _get_helm_release(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> HelmReleaseInfo:
        """
        Create the necessary information to manage the helm release.

        :rtype: HelmReleaseInfo
        :return: The information fully describing the desired helm release.

        .. warning::

            The name and namespace of the returned object must always be the
            same for an instance of a helm release. Otherwise, the previous
            release will be orphaned.`
        """

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> None:
        info = await self._get_helm_release(ctx, dependencies)
        await helm_delete(info.namespace, info.name)

    async def update(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> None:
        info = await self._get_helm_release(ctx, dependencies)
        await helm_upgrade(info)
