#!/bin/bash

# WARNING: We highly recommend setting a maintenance window during the execution of this script since it can cause messages
# of OpenStack components to get lost.
#
# This script deletes the old exchange of fanout queues and recreates them with the updated durable=true property
# as it would normally be created with the oslo_messaging_rabbit setting:
# rabbit_transient_quorum_queue: true
#
# This is necessary because RabbitMQ does not allow property changes for existing exchanges and the only option
# is to recreate them.

set -euo pipefail

if [ "$#" -ne 1 ]; then
  echo "Usage: ./recreate-exchanges.sh <AMQPSERVER_NAME>"
  exit 1
fi

AMQPSERVER_NAME=$1

set -x

function k {
  kubectl -n yaook "$@"
}

rabbitmq_pod=$(k get pod -o name -l "state.yaook.cloud/component=amqpserver,state.yaook.cloud/parent-name=${AMQPSERVER_NAME}" | head -n 1)
fanout_exchanges=$(k exec ${rabbitmq_pod} -- rabbitmqctl list_exchanges --formatter json | jq -r '.[] | select( .type == "fanout" and .name != "amq.fanout" ) | .name')
bindings=$(k exec ${rabbitmq_pod} -- rabbitmqctl list_bindings source_name destination_name routing_key --formatter json)

for exchange in ${fanout_exchanges}; do
  echo "delete exchange ${exchange} dry run"
  k exec ${rabbitmq_pod} -- bash -c 'rabbitmqadmin delete exchange name='${exchange}' -u $RABBITMQ_DEFAULT_USER -p$RABBITMQ_DEFAULT_PASS'
  k exec ${rabbitmq_pod} -- bash -c 'rabbitmqadmin declare exchange name='${exchange}' type=fanout durable=true auto_delete=true -u $RABBITMQ_DEFAULT_USER -p$RABBITMQ_DEFAULT_PASS'
  
  echo $bindings | jq -r ".[] | select(.source_name == \"${exchange}\") | .source_name + \" \" + .destination_name + \" \" + .routing_key" | \
  while read -r source destination routing_key; do k exec ${rabbitmq_pod} -- bash -c 'rabbitmqadmin declare binding source='${source}' destination='${destination}' routing_key='${routing_key}' --vhost=/ -u $RABBITMQ_DEFAULT_USER -p${RABBITMQ_DEFAULT_PASS}'; done
done

exit 0
