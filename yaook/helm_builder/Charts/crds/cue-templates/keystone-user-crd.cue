// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#operatorcrd
{
	#kind:     "KeystoneUser"
	#plural:   "keystoneusers"
	#singular: "keystoneuser"
	#schema: properties: spec: {
		required: [
			"keystoneRef",
		]
		properties: {
			keystoneRef: crd.#keystoneref
			role: {
				type:        "string"
				description: "Uses this role to bind the User to the target scope(s). If not provided the admin role will be used. If the role does not exist, it will be created. Target scope(s) for the role assignment are either defined by the scopes property or default to the service project if not specified."
			}
			scopes: {
				type:        "array"
				description: "Optional list of scopes to assign the role in for the User. List entries are key-value pairs where the key is one of the scope types 'project', 'domain' or 'system' and the value is the individual name of the respective scope. If not provided, will default to the project scope for the service project."
				minItems:    1
				items: {
					type: "object"
					oneOf: [
						{required: ["project"]},
						{required: ["domain"]},
						{required: ["system"]},
					]
					properties: {
						project: type: "string"
						domain: type:  "string"
						system: type:  "string"
					}
				}
			}
		}
	}
}
